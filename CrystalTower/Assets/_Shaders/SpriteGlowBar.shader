﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprites/Additive"
{
	Properties
	{
			[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
			_Color("Tint", Color) = (1,1,1,1)
			_Intensity("Intensity", Float) = 2
			_Value("Value", Float) = 1
	}

		SubShader
			{
					Tags
					{
							"Queue" = "Transparent"
							"IgnoreProjector" = "True"
							"RenderType" = "Transparent"
							"PreviewType" = "Plane"
							"CanUseSpriteAtlas" = "True"
					}

					Cull Off
					Lighting Off
					ZWrite Off
					Fog { Mode Off }
					Blend SrcAlpha One
				//AlphaTest Greater .01
				ColorMask RGB

				Pass
				{
				CGPROGRAM
						#pragma vertex vert
						#pragma fragment frag
						#include "UnityCG.cginc"

						struct appdata_t
						{
								float4 vertex   : POSITION;
								float4 color    : COLOR;
								float2 texcoord : TEXCOORD0;
						};

						struct v2f
						{
								float4 vertex   : SV_POSITION;
								fixed4 color : COLOR;
								half2 texcoord  : TEXCOORD0;
						};

						fixed4 _Color;
						float _Intensity;
						float _Value;

						v2f vert(appdata_t IN)
						{
								v2f OUT;
								OUT.vertex = UnityObjectToClipPos(IN.vertex);
								OUT.texcoord = IN.texcoord;
								OUT.color = IN.color * _Color;
								return OUT;
						}

						sampler2D _MainTex;

						fixed4 frag(v2f IN) : COLOR
						{
							fixed4 colorcito = IN.color * _Color;
							if (IN.texcoord.y < _Value)
							{
								colorcito *= _Intensity;
							}
								
							return tex2D(_MainTex, IN.texcoord) * colorcito;
						}
				ENDCG
				}
			}
}
