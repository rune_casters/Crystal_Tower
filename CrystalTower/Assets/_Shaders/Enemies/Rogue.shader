// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "CultOfRunes/Enemies/Rogue"
{
	Properties
	{
		_Distorsion("Distorsion", 2D) = "bump" {}
		_Panner2D("Panner 2D", Vector) = (0,1,0,0)
		_Blending("Blending", Range( 0 , 1)) = 0
		_DistorsionScale("DistorsionScale", Range( 0 , 1)) = 1
		_Metallic("Metallic", Range( 0 , 1)) = 1
		_Smoothness("Smoothness", Range( 0 , 1)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		GrabPass{ }
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows exclude_path:deferred 
		struct Input
		{
			float2 uv_texcoord;
			float4 screenPos;
		};

		uniform sampler2D _GrabTexture;
		uniform sampler2D _Distorsion;
		uniform float2 _Panner2D;
		uniform float _DistorsionScale;
		uniform float _Blending;
		uniform float _Metallic;
		uniform float _Smoothness;


		inline float4 ASE_ComputeGrabScreenPos( float4 pos )
		{
			#if UNITY_UV_STARTS_AT_TOP
			float scale = -1.0;
			#else
			float scale = 1.0;
			#endif
			float4 o = pos;
			o.y = pos.w * 0.5f;
			o.y = ( pos.y - o.y ) * _ProjectionParams.x * scale + o.y;
			return o;
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 panner13 = ( _Time.x * _Panner2D + i.uv_texcoord);
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_grabScreenPos = ASE_ComputeGrabScreenPos( ase_screenPos );
			float4 ase_grabScreenPosNorm = ase_grabScreenPos / ase_grabScreenPos.w;
			float4 screenColor31 = tex2D( _GrabTexture, ( float4( ( UnpackNormal( tex2D( _Distorsion, panner13 ) ) * _DistorsionScale ) , 0.0 ) + ase_grabScreenPosNorm ).xy );
			float4 temp_cast_2 = (1.0).xxxx;
			float4 lerpResult32 = lerp( screenColor31 , temp_cast_2 , _Blending);
			o.Emission = lerpResult32.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16100
1;1;1918;1016;726.0686;457.3718;1;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;18;-1189.69,-385.1403;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;29;-1125.712,-263.3082;Float;False;Property;_Panner2D;Panner 2D;1;0;Create;True;0;0;False;0;0,1;0,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TimeNode;15;-1162.098,-136.4584;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;13;-855.2311,-287.4348;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;42;-580.8781,-328.5126;Float;True;Property;_Distorsion;Distorsion;0;0;Create;True;0;0;False;0;e586e94ee94d88944a1f69019d287103;e586e94ee94d88944a1f69019d287103;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;41;-543.421,-99.57973;Float;False;Property;_DistorsionScale;DistorsionScale;3;0;Create;True;0;0;False;0;1;0.052;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;-203.7417,-193.131;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GrabScreenPosition;38;-511.1252,8.449965;Float;False;0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;39;12.31764,-150.8103;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;34;81.55366,116.6089;Float;False;Property;_Blending;Blending;2;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;31;169.5366,-157.3624;Float;False;Global;_GrabScreen0;Grab Screen 0;6;0;Create;True;0;0;False;0;Object;-1;False;False;1;0;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;33;195.1519,20.83025;Float;False;Constant;_White;White;2;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;406.5485,177.4832;Float;False;Property;_Smoothness;Smoothness;5;0;Create;True;0;0;False;0;1;0.574;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;410.5485,101.4832;Float;False;Property;_Metallic;Metallic;4;0;Create;True;0;0;False;0;1;0.913;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;32;476.9196,-107.2457;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;788,-113;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;CultOfRunes/Enemies/Rogue;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Translucent;0.5;True;True;0;False;Opaque;;Transparent;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;13;0;18;0
WireConnection;13;2;29;0
WireConnection;13;1;15;1
WireConnection;42;1;13;0
WireConnection;40;0;42;0
WireConnection;40;1;41;0
WireConnection;39;0;40;0
WireConnection;39;1;38;0
WireConnection;31;0;39;0
WireConnection;32;0;31;0
WireConnection;32;1;33;0
WireConnection;32;2;34;0
WireConnection;0;2;32;0
WireConnection;0;3;43;0
WireConnection;0;4;44;0
ASEEND*/
//CHKSM=997D8A43E195BC6A9C8E0DFC861FB4BC96B8A69A