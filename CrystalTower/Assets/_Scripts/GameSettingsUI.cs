﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Handles interface and user communication for Game settings
/// </summary>
public class GameSettingsUI : MonoBehaviour
{
    /// <summary>
    /// Reference to settings data
    /// </summary>
    public GameSettings gameSettings;

    /// <summary>
    /// Resolution dropdown reference
    /// </summary>
    [SerializeField] private TMP_Dropdown resolutionDropdown;

    /// <summary>
    /// Screen mode dropdown reference
    /// </summary>
    [SerializeField] private TMP_Dropdown screenModeDropdown;

    /// <summary>
    /// Quality dropdown reference
    /// </summary>
    [SerializeField] private TMP_Dropdown qualityDropdown;

    /// <summary>
    /// Anti aliasing dropdown reference
    /// </summary>
    [SerializeField] private TMP_Dropdown antiAliasingDropdown;

    /// <summary>
    /// Sets initial settings data
    /// </summary>
    public void Awake()
    {
        // Set all posible resolutions
        gameSettings.resolutions = Screen.resolutions;
        // Resolutions dropdown Setup
        resolutionDropdown.ClearOptions();
        List<string> resolutionOptionList = new List<string>();
        int currentResolutionIndex = 0;
      
        // Loop every resolution
        for (int i = 0; i < gameSettings.resolutions.Length; i++)
        {
            // Cache it
            Resolution resolution = gameSettings.resolutions[i];
            // Set display string
            string resolutionString = resolution.width + " x " + resolution.height + " _ " + resolution.refreshRate;
            // Add it to the list
            resolutionOptionList.Add(resolutionString);
            // Set initial resolution
            if (resolution.width == Screen.currentResolution.width && resolution.height == Screen.currentResolution.height && resolution.refreshRate == Screen.currentResolution.refreshRate)
                currentResolutionIndex = i;
        }
 
        // Add list to the dropdown
        resolutionDropdown.AddOptions(resolutionOptionList);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        // Screen Mode Setup
        screenModeDropdown.ClearOptions();
        screenModeDropdown.AddOptions(new List<string>(Enum.GetNames(typeof(FullScreenMode))));
        screenModeDropdown.value = (int)Screen.fullScreenMode;
        screenModeDropdown.RefreshShownValue();

        // Quality Settings Setup
        qualityDropdown.ClearOptions();
        qualityDropdown.AddOptions(new List<string>(QualitySettings.names));
        qualityDropdown.value = QualitySettings.GetQualityLevel();
        qualityDropdown.RefreshShownValue();

        // AntiAliasing Setup
        // Not using it
    }

    /// <summary>
    /// Bridge that communicates from settings menu to settings data
    /// </summary>
    public void SetResolution(int resolutionIndex)
    {
        gameSettings.SetResolution(gameSettings.resolutions[resolutionIndex]);
    }
}
