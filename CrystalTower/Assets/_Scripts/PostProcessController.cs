﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

/// <summary>
/// Handles communication with the Unity Post Process layer
/// </summary>
public class PostProcessController : MonoBehaviour
{
    /// <summary>
    /// Reference to the Post process volume
    /// </summary>
    public PostProcessVolume volume;

    /// <summary>
    /// Reference to the post process profile
    /// </summary>
    private PostProcessProfile currentProfile;

    /// <summary>
    /// Vignette effect
    /// </summary>
    Vignette vignette = null;

    /// <summary>
    /// Time calculated
    /// </summary>
    float currentTime = 2;

    /// <summary>
    /// Speed in which it lerps back to the original vignette
    /// </summary>
    float lerpSpeed = 0.3f;

    private void Awake()
    {
        // Get setting from post processing profile
        volume.profile.TryGetSettings(out vignette);
    }

    private void Update()
    {
        // Hit effect lerp color
        if (currentTime <= 1)
        {
            currentTime += Time.deltaTime * lerpSpeed;
            // Go from red to black
            vignette.color.Interp(Color.red, Color.black, currentTime);
            vignette.smoothness.Interp(0.4f, 0.2f, currentTime);
        }
    }

    /// <summary>
    /// Reset time so that on Update handles the vignette transition
    /// </summary>
    public void PlayerHit()
    {
        currentTime = 0;
    }
}
