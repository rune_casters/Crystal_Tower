﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Handles enemy detection and displays them on a compass on the player's UI
/// </summary>
public class CompassTrigger : MonoBehaviour
{
    /// <summary>
    /// Pointer that represents an enemy on the compass
    /// </summary>
    struct EnemyPointer
    {
        /// <summary>
        /// Reference to the pointer's transform
        /// </summary>
        public RectTransform pointerRect;

        /// <summary>
        /// Reference to the enemy targeted
        /// </summary>
        public Enemy enemy;
    }

    /// <summary>
    /// Stores every enemy that has been detected to display on the compass
    /// </summary>
    private List<EnemyPointer> enemyList = new List<EnemyPointer>();

    /// <summary>
    /// Reference to the player (needed for the calculations)
    /// </summary>
    public Transform playerTransform;

    /// <summary>
    /// Reference to the compass transform
    /// </summary>
    public RectTransform compassRect;

    /// <summary>
    /// Reference to the compass pointer prefab
    /// </summary>
    public RectTransform pointerPrefabRect;

    /// <summary>
    /// Scale in which they are displayed
    /// </summary>
    float pointerScale;

    /// <summary>
    /// Gets every corner of the compass which is used to calculate scale
    /// </summary>
    private void Start()
    {
        // Create array in which we store the four corners of the rectangle and then calculate width of rectangle
        Vector3[] v = new Vector3[4];
        compassRect.GetLocalCorners(v);
        pointerScale = Vector3.Distance(v[1], v[2]);
    }

    /// <summary>
    /// Displays every enemy from the list
    /// </summary>
    public void Update()
    {
        // Loop every enemy from the list
        for (int i = enemyList.Count - 1; i >= 0; i--)
        {
            // Cache enemy pointer
            EnemyPointer enemyPointer = enemyList[i];
            // If the enemy does not exist (it just died)
            if (!enemyPointer.enemy)
            {
                // Destroy pointer and remove it from list
                Destroy(enemyPointer.pointerRect.gameObject);
                enemyList.Remove(enemyPointer);
            }
            // Otherwise calculate position
            else
            {
                // Calculate angle from player to enemy target 
                // Used this and changed it up https://answers.unity.com/questions/983219/making-a-compass-point-towards-the-closest-enemy.html
                float angle = Mathf.Atan2(Vector3.Dot(Vector3.up, Vector3.Cross(playerTransform.forward, enemyPointer.enemy.transform.position - playerTransform.position)), Vector3.Dot(playerTransform.forward, enemyPointer.enemy.transform.position - playerTransform.position)) * Mathf.Rad2Deg;
                // Clamp it so it doesn't leave the compass from both sides
                angle = Mathf.Clamp(angle, -90, 90) / 180.0f * pointerScale;
                // Set pointer position considering angle
                enemyPointer.pointerRect.localPosition = new Vector3(angle, enemyPointer.pointerRect.localPosition.y, enemyPointer.pointerRect.localPosition.z);
                // Added value to calculate distance between player and enemy target
                float value = 0.05f * Vector3.Distance(enemyPointer.enemy.transform.position, transform.position);
                // Use this value as scale to represent if the target is far away or close
                enemyPointer.pointerRect.localScale = Vector3.one / Mathf.Clamp(value, 0.5f, 1.1f);
            }
        }
    }

    /// <summary>
    /// Add an enemy if it doesn't exist already in the enemy list
    /// </summary>
    public void OnTriggerEnter(Collider other)
    {
        // Get collided enemy
        Enemy enemy = other.GetComponent<Enemy>();
        // Check if it exists already
        EnemyPointer enemyPointer = enemyList.FirstOrDefault(enemyStruct => enemyStruct.enemy == enemy);
        // If it doesn't exist inside list
        if (enemyPointer.enemy == null)
        {
            // Create enemy pointer
            enemyPointer.enemy = enemy;
            // Set UI prefab
            enemyPointer.pointerRect = Instantiate(pointerPrefabRect, compassRect);
            // Add to list
            enemyList.Add(enemyPointer);
        }
    }

    /// <summary>
    /// Clears every enemy from the compass (doesnt kill the enemies, only UI-wise)
    /// </summary>
    public void ClearCompass()
    {
        foreach (EnemyPointer enemyPointer in enemyList)
        {
            Destroy(enemyPointer.pointerRect.gameObject);
        }
        enemyList.Clear();
    }

}
