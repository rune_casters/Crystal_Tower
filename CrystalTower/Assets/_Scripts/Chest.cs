﻿using UnityEngine;

/// <summary>
/// Handles loot spawning when players is in contact
/// </summary>
public class Chest : MonoBehaviour
{
    /// <summary>
    /// If the chest has already been opened
    /// </summary>
    private bool opened;

    /// <summary>
    /// Reference to the loot spawn component that creates loot
    /// </summary>
    public LootSpawn lootspawn;

    /// <summary>
    /// Reference to the loot table that stores the probabilities and data of every possible item  
    /// </summary>
    public LootTable chestTable;

    /// <summary>
    /// Reference to chest animator
    /// </summary>
    public Animator anim;

    /// <summary>
    /// Spawns loot when player enters the trigger
    /// </summary>
    public void OnTriggerEnter(Collider other)
    {
        // Check if the chest has not been opened
        if (!opened)
        {
            // Trigger open animation
            opened = true;
            anim.SetTrigger("Open");
            // Spawns loot
            lootspawn.SpawnLoot(chestTable, Vector3.up * 0.5f -  2 * transform.forward );
        }
    }
}
