﻿using UnityEngine;

/// <summary>
/// Handles light flickering effect
/// </summary>
public class LightFlicker : MonoBehaviour
{
    /// <summary>
    /// Cached starting light position
    /// </summary>
    private Vector3 initialPosition;

    /// <summary>
    /// Flicker cooldown
    /// </summary>
    private float cooldown = 0.1f;

    /// <summary>
    /// Current timer
    /// </summary>
    private float currentTime;

    /// <summary>
    /// Cached light reference
    /// </summary>
    private Light lightRef;

    private void Start()
    {
        // Cache data
        lightRef = GetComponent<Light>();
        initialPosition = transform.position;
    }

    /// <summary>
    /// Flicker effect
    /// </summary>
    public void Update()
    {
        // Changes light's intensity based on ping pong function which uses time
        lightRef.intensity = Mathf.PingPong(Time.time, 3) + 3;
        currentTime += Time.deltaTime;

        // Move randomly after some time
        if (currentTime >= cooldown)
        {
            transform.position = Random.insideUnitSphere * 0.01f + initialPosition;
            currentTime = 0;
        }
    }
}
