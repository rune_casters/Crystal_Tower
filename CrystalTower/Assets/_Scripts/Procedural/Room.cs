﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Physical Room that handles room events, communication between portals and stores a reference to room data
/// </summary>
public class Room : MonoBehaviour
{
    /// <summary>
    /// Portals parent transform
    /// </summary>
    [Header("Room Data")]
    public Transform portalsContainer;

    /// <summary>
    /// Current portals on this room
    /// </summary>
    [HideInInspector] public int currentPortals;

    /// <summary>
    /// Max portals that can be available
    /// </summary>
    [HideInInspector] public int maxPortals;

    /// <summary>
    /// Event raised when player enter room
    /// </summary>
    public GameEvent enterRoomEvent;

    /// <summary>
    /// Event raised when room is cleared from enemies
    /// </summary>
    public GameEvent roomClearedEvent;

    /// <summary>
    /// If the room closes it's portals when player enters (only enemy rooms)
    /// </summary>
    public bool closePortalsOnEnter;

    /// <summary>
    /// If the room has been cleared previously
    /// </summary>
    [HideInInspector] public bool clearedRoom;

    /// <summary>
    /// Stores every portal on the current room
    /// </summary>
    private List<Portal> portals;

    /// <summary>
    /// Reference to the spawn manager
    /// </summary>
    public SpawnManager spawnManagerRef;

    /// <summary>
    /// Current room type
    /// </summary>
    public RoomType roomType;

    /// <summary>
    /// Types of room
    /// </summary>
    public enum RoomType
    {
        START,
        ENEMY,
        SHOP,
        END
    }
    
    /// <summary>
    /// Checker prefab used for the 3D navigational algorithm
    /// </summary>
    [Header("3D NavMesh Data")]
    public Transform checker;

    /// <summary>
    /// Starting position in which the algorithm will start
    /// </summary>
    [SerializeField] private Transform checkerStartPos;

    /// <summary>
    /// Room data in which we save all the info regarding 3D navigational data
    /// </summary>
    [SerializeField] private RoomData  data;

    private void Awake()
    {
        // Get possible portal count
        maxPortals = portalsContainer.childCount;
        portals = new List<Portal>();
    }

    /// <summary>
    /// Returns a randomly created portal with given prefab
    /// </summary>
    public Portal CreateRandomPortal(GameObject portalPrefab)
    {
        int iterations = 0;
        // Look for a random portal point that has not been activated yet
        int randomPortalPoint = Random.Range(0, portalsContainer.childCount);
        // Safe check iterations 
        while (portalsContainer.GetChild(randomPortalPoint).gameObject.activeSelf && iterations <= 30)
        {
            // Get random portal
            randomPortalPoint = Random.Range(0, portalsContainer.childCount);
            iterations++;
        }

        // Create Portal
        Transform selectedPoint = portalsContainer.GetChild(randomPortalPoint);

        // Debug check
        if (selectedPoint.gameObject.activeSelf)
            Debug.Log("<color=red> ERROR: ALL PORTALS MUST BE DISABLED BEFORE CREATING THE LEVEL! </color>");

        // Create new portal on given point
        GameObject currentPortalGameObj = Instantiate(portalPrefab, selectedPoint.position, selectedPoint.rotation);
        // Adjust position
        currentPortalGameObj.transform.position += currentPortalGameObj.transform.forward * 0.1f + Vector3.up;
        currentPortalGameObj.transform.parent = selectedPoint;
        // Enable point
        selectedPoint.gameObject.SetActive(true);
        currentPortals++;
        // Get portal component and add it to room portals
        Portal currentPortal = currentPortalGameObj.GetComponent<Portal>();
        portals.Add(currentPortal);
        // Return created portal
        return currentPortal;
    }

    /// <summary>
    /// Apply given state to all the portals
    /// </summary>
    public void TogglePortals(bool value)
    {
        // Loop portals and apply given state
        foreach (Portal portal in portals)
            portal.TogglePortal(value);
    }

    /// <summary>
    /// Called when player enters room
    /// </summary>
    public void NotifyRoomEntrance()
    {
        // If the room has no been cleared
        if (!clearedRoom)
        {
            // Raise event
            if (enterRoomEvent)
                enterRoomEvent.Raise();

            // Close portals if it's an enemy room
            if (closePortalsOnEnter)
                TogglePortals(false);

            // Start spawning if it's an enemy room
            if (spawnManagerRef)
                spawnManagerRef.StartSpawning();
        }
    }

    /// <summary>
    /// Called when every enemy has been dealth with
    /// </summary>
    public void RoomCleared()
    {
        clearedRoom = true;
        // Open open if they have been closed
        if (closePortalsOnEnter)
            TogglePortals(true);
        // Raise cleared room event
        roomClearedEvent.Raise();

    }

    #region Pathfinding search volume

    /// <summary>
    /// Return calculated walkable areas
    /// </summary>
    public int[,,] GetWalkableArea()
    {
        // Create array to store result it
        int[,,] result = new int[data.walkableArea.Length, data.walkableArea[0].walkableLine.Length, data.walkableArea[0].walkableLine[0].walkableColumn.Length];
        // Loop walkable rows of area
        for (int i = 0; i < data.walkableArea.Length; i++)
        {
            // Loop walkable lines of row
            for (int j = 0; j < data.walkableArea[i].walkableLine.Length; j++)
            {
                // Loop walkable cubes of line
                for (int k = 0; k < data.walkableArea[i].walkableLine[j].walkableColumn.Length; k++)
                {
                    // Check value to set if it's walkable or not
                    if (data.walkableArea[i].walkableLine[j].walkableColumn[k].value)
                    {
                        result[i, j, k] = 0;
                    }
                    else
                    {
                        result[i, j, k] = 1;
                    }

                }
            }
        }
        return result;
    }

    /// <summary>
    /// Return position of the starting point
    /// </summary>
    public Vector3 GetStartPos()
    {
        return checkerStartPos.position;
    }

    /// <summary>
    /// Return size of the used node
    /// </summary>
    public float GetNodeSize()
    {
        return checker.transform.localScale.x;
    }
    #endregion
}


/// <summary>
/// Stores a row of lines
/// </summary>
[System.Serializable]
public class WalkableRow
{
    public WalkableLine[] walkableLine;
}

/// <summary>
/// Stores a line of cubes
/// </summary>
[System.Serializable]
public class WalkableLine
{
    public WalkableColumn[] walkableColumn;
}

/// <summary>
/// Stores if the cube is walkable
/// </summary>
[System.Serializable]
public class WalkableColumn
{
    public bool value;
}
