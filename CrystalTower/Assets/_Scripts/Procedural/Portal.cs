﻿using UnityEngine;

/// <summary>
/// Handles player transportation between rooms
/// </summary>
public class Portal : MonoBehaviour
{
    /// <summary>
    /// Destination point transform
    /// </summary>
    private Transform destination;

    /// <summary>
    /// Reference to the room in which the destination is from
    /// </summary>
    private Room destinationRoom;

    /// <summary>
    /// Reference to renderer
    /// </summary>
    private Renderer    render;

    /// <summary>
    /// Reference to material used when the portal is open
    /// </summary>
    public Material    openMaterial;

    /// <summary>
    /// Reference to material used when the portal has already been used
    /// </summary>
    public Material    usedMaterial;

    /// <summary>
    /// Reference to material used when the portal is closed
    /// </summary>
    public Material    closedMaterial;

    /// <summary>
    /// If the portal can be used
    /// </summary>
    public bool Open { get; set; }

    /// <summary>
    /// If the portal has already been used
    /// </summary>
    public bool Used { get; set; }

    private void Awake()
    {
        // Cache renderer
        render = GetComponent<Renderer>();
        Open = true;
    }

    /// <summary>
    /// Checks if the portal is open on trigger enter, if it is transports player to destination room notifying it of the arrival
    /// </summary>
    /// <param name="other"></param>
    public void OnTriggerEnter(Collider other)
    {
        // Check if it's open and the player is inside trigger
        if (other.CompareTag("Player") && Open)
        {
            // Enable destination room (was disabled for optimizing purposes)
            destinationRoom.gameObject.SetActive(true);

            // If it has not been used mark it
            if (!Used)
            {
                Use();
                destination.GetComponentInParent<Portal>().Use();
            }

            // Move player to destination
            other.transform.position = destination.position + Vector3.up;
            other.transform.rotation = destination.rotation;
            // Notify destination room for event purposes (spawn enemies on arrival and close portals)
            destinationRoom.NotifyRoomEntrance();
            // Reinitialize camera since we rotated the First person controller 
            other.GetComponent<RigidbodyFirstPersonController>().ReInitMouseLook();
            // Disable room in which the player was (again for optimizing purposes)
            destination.GetComponentInParent<Portal>().destinationRoom.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Mark portal as used
    /// </summary>
    public void Use()
    {
        Used = true;
        // Change material
        render.material = usedMaterial;
    }

    /// <summary>
    /// Set portal's destination room and position  
    /// </summary>
    public void SetDestination(Transform _destination, Room _destinationRoom)
    {
        destination = _destination;
        destinationRoom = _destinationRoom;
    }

    /// <summary>
    /// Set portal on the given state
    /// </summary>
    /// <param name="value"></param>
    public void TogglePortal(bool value)
    {
        Open = value;
        // Set material depending on given value
        if (!value)
            render.material = closedMaterial;
        else
        {
            if (Used)
                render.material = usedMaterial;
            else
                render.material = openMaterial;
        }
            
    }
}
