﻿using UnityEngine;

/// <summary>
/// Stores data about room such as name and prefab along with 3D Navigation data
/// </summary>
[CreateAssetMenu(menuName = "Room/Data")]
public class RoomData : ScriptableObject
{
    /// <summary>
    /// Room name
    /// </summary>
    [Header("Room Prefab Data")]
    public string roomName;

    /// <summary>
    /// Room prefab 
    /// </summary>
    public GameObject roomPrefab;

    /// <summary>
    /// Data used on the 3D navigation algorithm
    /// </summary>
    public WalkableRow[] walkableArea;

    /// <summary>
    /// Size of used nodes
    /// </summary>
    public float nodeSize;

    /// <summary>
    /// Position where the 3D algorithm has to start
    /// </summary>
    public Vector3 startPos;

    /// <summary>
    /// Room probability of appearing
    /// </summary>
    [Range(1, 100)]
    public int probability = 50;

    /// <summary>
    /// Sets up 3D navigation data to the room through given parameters
    /// </summary>
    public void SetUpWalkable(int[] walkable, int width, int height, int lenght, float _nodeSize, Vector3 _startPos)
    {
        nodeSize = _nodeSize;
        startPos = _startPos;
        // Creates 3D data (rows)
        walkableArea = new WalkableRow[width];
        for (int i = 0; i < width; i++)
        {
            walkableArea[i] = new WalkableRow();
            // Creates line
            walkableArea[i].walkableLine = new WalkableLine[lenght];
            for (int k = 0; k < lenght; k++)
            {
                walkableArea[i].walkableLine[k] = new WalkableLine();
                // Creates column
                walkableArea[i].walkableLine[k].walkableColumn = new WalkableColumn[height];
                for (int j = 0; j < height; j++)
                {
                    walkableArea[i].walkableLine[k].walkableColumn[j] = new WalkableColumn();
                    // Calculate value (if it's walkable or not)
                    walkableArea[i].walkableLine[k].walkableColumn[j].value = walkable[i * lenght * height + k * height + j] == 1;
                }
            }
        }
    }
}