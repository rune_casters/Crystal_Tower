﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Handles creation and conexion of procedural dungeon
/// </summary>
public class DungeonGenerator : MonoBehaviour
{
    /// <summary>
    /// Starting position of first room
    /// </summary>
    [Header("Generator Data")]
    [SerializeField] private Vector3 startingPosition;

    /// <summary>
    /// Distance between rooms on creation
    /// </summary>
    [SerializeField] [Range(50, 500)] private int distanceBetweenRooms;

    /// <summary>
    /// Range of portal count
    /// </summary>
    [SerializeField] [Range(2,8)] private int maxPortalsPerRoom;

    /// <summary>
    /// If it generates a dungeon on start (testing purposes)
    /// </summary>
    [SerializeField] private bool generateOnStart = false;

    /// <summary>
    /// Reference to the sanctuary room prefab (first room)
    /// </summary>
    [Space]
    [Header("Room Prefabs")]
    [SerializeField] private GameObject sanctuaryRoomPrefab;
    /// <summary>
    /// Reference to the rooms that will be used to create dungeon (set through editor)
    /// </summary>
    [SerializeField] private RoomType[] rooms;

    /// <summary>
    /// Reference to the final room prefab (last room)
    /// </summary>
    [SerializeField] private GameObject finalRoomPrefab;

    /// <summary>
    /// Portal prefab that is used to connect rooms between them
    /// </summary>
    [Header("Other Prefabs")]
    [SerializeField] private GameObject portalPrefab;

    /// <summary>
    /// Rooms are stored on creation on this list
    /// </summary>
    [Space(5)]
    private List<Room> dungeon;

    /// <summary>
    /// Empty object that is parent to all rooms (used to clear)
    /// </summary>
    private Transform dungeonContainer;

    /// <summary>
    /// Number of minimum rooms that could be created
    /// </summary>
    [HideInInspector] public float numberOfRoomsMin;

    /// <summary>
    /// Number of maximum rooms that could be created
    /// </summary>
    [HideInInspector] public float numberOfRoomsMax;

    /// <summary>
    /// Counts total rooms created
    /// </summary>
    private int numberOfRooms;

    /// <summary>
    /// Stores total frequence calculated by the sum of every room frequence (used to generate randomly)
    /// </summary>
    private int totalFrequence;

    private void Start()
    {
        // Generates dungeon if set to true (normally the player just sets foot on the first portal)
        if (generateOnStart)
            GenerateDungeon();
    }

    /// <summary>
    /// Generates dungeon and connects it with portals
    /// </summary>
    public void GenerateDungeon()
    {
        InstantiateDungeon();
        ConnectDungeon();
    }

    /// <summary>
    /// Creates dungeon structure randomly using frequence and probabilities
    /// </summary>
    private void InstantiateDungeon()
    {
        // Initialise data
        dungeon = new List<Room>();
        dungeonContainer = new GameObject().transform;
        dungeonContainer.parent = transform;
        dungeonContainer.name = "Dungeon Container";
        numberOfRooms = Random.Range(Mathf.RoundToInt(numberOfRoomsMin), Mathf.RoundToInt(numberOfRoomsMax));
        totalFrequence = 0;

        // Calculate total frequence
        foreach (RoomType roomType in rooms)
        {
            totalFrequence += roomType.frequence;
            roomType.CurrentCount = 0;
            roomType.TotalProbability = 0;

            // Calculate each room probability
            foreach (RoomData roomData in roomType.roomDataArr)
                roomType.TotalProbability += roomData.probability;
        }


        // Create initial room (only one)
        GameObject sanctuaryRoom = Instantiate(sanctuaryRoomPrefab, startingPosition, sanctuaryRoomPrefab.transform.rotation);
        sanctuaryRoom.name = sanctuaryRoomPrefab.name + "_1";
        sanctuaryRoom.transform.parent = dungeonContainer;
        dungeon.Add(sanctuaryRoom.GetComponent<Room>());

        // Create every other room but the last
        for (int i = 1; i < numberOfRooms - 1; i++)
            CreateRoom(totalFrequence, startingPosition + new Vector3(distanceBetweenRooms * i, 0, 0), i);

        // Create final room
        GameObject finalRoom = Instantiate(finalRoomPrefab, startingPosition + new Vector3(distanceBetweenRooms * numberOfRooms, 0, 0), sanctuaryRoom.transform.rotation);
        finalRoom.name = finalRoomPrefab.name + "_" + numberOfRooms;
        finalRoom.transform.parent = dungeonContainer;
        dungeon.Add(finalRoom.GetComponent<Room>());

        // Debug result
        if (dungeon.Count < numberOfRooms)
            Debug.Log("DNG_Generated Dungeon with " + dungeon.Count + " / " + numberOfRooms + " rooms ( Max count is too low compared to the number of Rooms ).");
        else
            Debug.Log("DNG_Generated Dungeon with " + dungeon.Count + " rooms.");
    }

    /// <summary>
    /// Connects dungeon with portals through set conditions
    /// </summary>
    private void ConnectDungeon()
    {
        // Loop every room
        foreach (Room room in dungeon)
        {
            // Check that the room has enough space for portals
            if (room.currentPortals < room.maxPortals && room.currentPortals < maxPortalsPerRoom)
            {
                // Get a random number of portals 
                int roomPortalCount = Random.Range(1, room.maxPortals - room.currentPortals);

                // Create them
                for (int i = 0; i < roomPortalCount && i < maxPortalsPerRoom; i++)
                {
                    // Look for Destination portal on random other room
                    Room randomRoom;
                    // Has to not be this room and have enough space for portal
                    int iterations = 0;
                    do
                    {
                        randomRoom = dungeon[Random.Range(0, dungeon.Count)];
                        // Check that the first room and the last room are not the same ones (design condition)
                    } while ((room == randomRoom ||
                              room.roomType == Room.RoomType.START && randomRoom.roomType == Room.RoomType.END ||
                              randomRoom.roomType == Room.RoomType.START && room.roomType == Room.RoomType.END ||
                              randomRoom.currentPortals >= randomRoom.maxPortals) && ++iterations < 30);

                    // Safe check if it's unlucky so it doesn't crash
                    if (iterations < 30)
                    {
                        // Create Portal on current room
                        Portal roomPortal = room.CreateRandomPortal(portalPrefab);
                        // Create Portal on random room
                        Portal randomRoomPortal = randomRoom.CreateRandomPortal(portalPrefab);
                        // Connect
                        roomPortal.SetDestination(randomRoomPortal.transform.GetChild(0), randomRoom);
                        randomRoomPortal.SetDestination(roomPortal.transform.GetChild(0), room);
                    }
                }
            }
        }

        // Disable every dungeon room but the first one (so it saves quite some FPS)
        for (int i = 1; i < dungeon.Count; i++)
        {
            dungeon[i].gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Clears entire dungeon destroying every room
    /// </summary>
    public void ClearDungeon()
    {
        if (dungeonContainer)
            Destroy(dungeonContainer.gameObject);
    }

    /// <summary>
    /// Clears entire dungeon but through the Editor
    /// </summary>
    public void ClearDungeonEditor()
    {
        if (dungeonContainer)
            DestroyImmediate(dungeonContainer.gameObject);

        if (transform.childCount != 0)
            foreach (Transform dngToClear in transform)
                DestroyImmediate(dngToClear.gameObject);
    }

    /// <summary>
    /// Creates a room with the given total frequence, position and room count
    /// </summary>
    private void CreateRoom(int totalFrequence, Vector3 position, int count)
    {
        // If the room has been created
        bool roomCreated = false;
        // Iteration safe check
        int iterations = 0;

        do
        {
            // Get random number (random room type) 
            // --> EX: 35 is the random number __ Ranges: 1-40 Enemies / 40-60 Parkour __ Room Selected: Enemies
            int randomFrequence = Random.Range(0, totalFrequence);
            int accumulatedFrequence = 0;
            
            // Loop room types and get a random type
            foreach (RoomType roomType in rooms)
            {
                // Accumulate given room frequence and check if the random number is below that interval
                accumulatedFrequence += roomType.frequence;
                if (randomFrequence <= accumulatedFrequence && roomType.CurrentCount < roomType.maximumCount)
                {
                    // We have a type now, we need a random room between the given ones
                    int randomProbability = Random.Range(0, roomType.TotalProbability);
                    int accumulatedProbability = 0;

                    // Loop rooms of that type
                    foreach (RoomData roomData in roomType.roomDataArr)
                    {
                        // Accumulate given room probability and check if the random number is below that interval
                        accumulatedProbability += roomData.probability;
                        if (randomProbability <= accumulatedProbability)
                        {
                            // Create the room
                            GameObject roomPrefabRef = roomData.roomPrefab;
                            GameObject currentRoomInstance = Instantiate(roomPrefabRef, position, roomPrefabRef.transform.rotation);
                            currentRoomInstance.name = "R" + (count + 1) + "_" + roomData.roomName;
                            currentRoomInstance.transform.parent = dungeonContainer;
                            dungeon.Add(currentRoomInstance.GetComponent<Room>());
                            roomType.CurrentCount++;
                            roomCreated = true;
                            goto End;
                        }
                    }
                }
            }
            End: { }
            // Only stop if the room has been created of the safe check has been cracked
        } while (!roomCreated && ++iterations < 10);
    }
}

// Only compile on editor
#if UNITY_EDITOR

/// <summary>
/// Custom Editor for the dungeon generator
/// </summary>
[CustomEditor(typeof(DungeonGenerator))]
public class DungeonGeneratorEditor : Editor
{
    /// <summary>
    /// Drawns Inspector data
    /// </summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DungeonGenerator generator = (DungeonGenerator)target;

        GUILayout.Space(5);
        // Darw range checks
        GUILayout.Box("Room count interval");
        EditorGUILayout.LabelField("Min:", Mathf.RoundToInt(generator.numberOfRoomsMin).ToString());
        EditorGUILayout.LabelField("Max:", Mathf.RoundToInt(generator.numberOfRoomsMax).ToString());
        // Draw slider for the number of rooms
        EditorGUILayout.MinMaxSlider(ref generator.numberOfRoomsMin, ref generator.numberOfRoomsMax, 4, 26);
        GUILayout.Box("Disable GenerateOnStart and Generate a Dungeon to play on your own one");
        // Create dungeon on editor
        if (GUILayout.Button("Generate Dungeon [Clear on Default]"))
        {
            generator.ClearDungeonEditor();
            generator.GenerateDungeon();
        }
        // Clear dungeon on editor
        if (GUILayout.Button("Clear Dungeon"))
        {
            generator.ClearDungeonEditor();
        }

    }
}

#endif

/// <summary>
/// Stores room data
/// </summary>
[System.Serializable]
public class RoomType
{
    /// <summary>
    /// Room type name
    /// </summary>
    [Header("Room Type Data")]
    public string       name;
    /// <summary>
    /// Room type frequence used to calculate randomly
    /// </summary>
    [Range(1, 100)]
    public int          frequence = 50;
    /// <summary>
    /// Maximum possible rooms of that type
    /// </summary>
    [Range(1, 20)]
    public int          maximumCount = 5;
    /// <summary>
    /// Current rooms created of that type
    /// </summary>
    public int          CurrentCount { get; set; }
    /// <summary>
    /// Stores every room of that type
    /// </summary>
    public RoomData[]   roomDataArr;
    /// <summary>
    /// Calculated total probability
    /// </summary>
    public int          TotalProbability { get; set; }
}

