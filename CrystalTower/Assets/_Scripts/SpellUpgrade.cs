﻿using UnityEngine;

/// <summary>
/// Stores data of an upgrade spell and if it's being sold (physical version of a spell that can be taken by the player)
/// </summary>
public class SpellUpgrade : MonoBehaviour
{
    /// <summary>
    /// If it's being sold
    /// </summary>
    [HideInInspector] public bool sold;

    /// <summary>
    /// Slot that is occupied by the spell
    /// </summary>
    [HideInInspector] public SpellData.Slot spellSlot;

    /// <summary>
    /// Spell data that can be taken by the player
    /// </summary>
    [HideInInspector] public SpellData upgradeSpell;
}
