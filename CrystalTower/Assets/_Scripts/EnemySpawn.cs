﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Handles enemy spawning on restricted zone
/// </summary>
[DisallowMultipleComponent]
public class EnemySpawn : MonoBehaviour
{
    /// <summary>
    /// Spawn zone (edited through scene)
    /// </summary>
    [Header("Data")]
    public Vector3 dimensions = new Vector3(5, 0, 5);
    /// <summary>
    /// Possible enemies that could be spawned
    /// </summary>
    public EnemySpawnData[] waveTypes = new EnemySpawnData[0];

    /// <summary>
    /// Reference to the current room spawn manager
    /// </summary>
    [HideInInspector] public SpawnManager spawnManagerRef;

    /// <summary>
    /// Spawns (there was a multiple check here)
    /// </summary>
    public void Spawn()
    {
        SpawnRandom();
    }

    /// <summary>
    /// Spawns given enemy in the given position
    /// </summary>
    private void SpawnEnemy(EnemyStats enemyStats, Vector3 position)
    {
        // Create enemy using prefab and given position
        Enemy newEnemy = Instantiate(enemyStats.prefab, position, enemyStats.prefab.transform.rotation).GetComponent<Enemy>();
        // Set 3D nav mesh data
        if (spawnManagerRef.data)
            newEnemy.SetWalkableArea(spawnManagerRef.GetWalkableArea(), spawnManagerRef.data.startPos + transform.parent.parent.position, spawnManagerRef.data.nodeSize);
        // Add new enemy to spawn manager
        spawnManagerRef.AddEnemy(enemyStats.dificultyValue);
    }

    /// <summary>
    /// Spawns a random enemy from enemy list
    /// </summary>
    private void SpawnRandom()
    {
        // Get random type of enemy
        int waveNumber = Random.Range(0, waveTypes.Length);
        // Check if it's a squad 
        if (waveTypes[waveNumber].enemyData.Length > 1)
        {
            // Check if its a group of enemies that have to spawn randomly
            if (waveTypes[waveNumber].randomGroup)
            {
                // Spawn enemies in squad randomly
                for (int i = 0; i < waveTypes[waveNumber].enemiesInSquad; i++)
                {
                    SpawnEnemy(waveTypes[waveNumber].enemyData[Random.Range(0, waveTypes[waveNumber].enemyData.Length)].enemyStats, GetRandomPoint());
                }
            }
            // Otherwise spawn them on their given position
            else
            {
                // Spawn enemies on their respectives positions
                foreach (EnemySpawnData.EnemyData enemy in waveTypes[waveNumber].enemyData)
                {
                    SpawnEnemy(enemy.enemyStats, transform.position + enemy.spawnPosition);
                }
            }
        }
        // If it's a solo enemy
        else
        {
            // Spawn in alone on a random point
            SpawnEnemy(waveTypes[waveNumber].enemyData[0].enemyStats, GetRandomPoint());
        }
    }

    /// <summary>
    /// Returns a random point on spawning zone
    /// </summary>
    private Vector3 GetRandomPoint()
    {
        // Calculates the random point
        return new Vector3(Random.Range(transform.position.x - dimensions.x / 2, transform.position.x + dimensions.x / 2),
                            Random.Range(transform.position.y - dimensions.y / 2, transform.position.y + dimensions.y / 2),
                            Random.Range(transform.position.z - dimensions.z / 2, transform.position.z + dimensions.z / 2));
    }

    // Custom editor
#if UNITY_EDITOR

    /// <summary>
    /// Creates a Spawn (design helper) 
    /// </summary>
    [MenuItem("| LevelDesign |/Spawn", false, 1)]
    public static void CreateSpawn()
    {
        // Creates spawn
        EnemySpawn newSpawn = new GameObject("EnemySpawn").AddComponent<EnemySpawn>();
        // If the editor user is selecting an object then put it there
        if (Selection.activeGameObject)
        {
            newSpawn.transform.SetParent(Selection.activeGameObject.transform);
            newSpawn.transform.position = Selection.activeGameObject.transform.position;
        }
        // Otherwise just create it in front of camera
        else
            newSpawn.transform.position = Camera.current.transform.position + Camera.current.transform.forward * 5;

        // Add it to the undo operation
        Undo.RegisterCreatedObjectUndo(newSpawn.gameObject, "Create " + newSpawn.name);
        // Set new spawn as selection
        Selection.activeObject = newSpawn;
    }

    /// <summary>
    /// Toggle drawing spawns information
    /// </summary>
    [MenuItem("| LevelDesign |/ToggleSpawnInfo", false, 51)]
    public static void ToggleSpawnInfo()
    {
        drawInfo = !drawInfo;
    }

    /// <summary>
    /// Color in which the zone is currently drawn
    /// </summary>
    [Header("Display")]
    public Color gizmoColor = new Color(1.0f, 0, 0, 0.5f);
    public static bool drawInfo = true;

    void OnDrawGizmos()
    {
        // Set color
        Gizmos.color = gizmoColor;
        // Draw spawn zone
        Gizmos.DrawCube(transform.position, dimensions);
    }

#endif

}