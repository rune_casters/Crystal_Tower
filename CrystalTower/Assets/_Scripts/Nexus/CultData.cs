﻿using UnityEngine;

/// <summary>
/// Stores data about cult (Hela, Fenrir etc...)
/// </summary>
[CreateAssetMenu]
public class CultData : ScriptableObject
{
    /// <summary>
    /// Cult god name
    /// </summary>
    public new string   name;
    /// <summary>
    /// God sprite
    /// </summary>
    public Sprite       sprite;

    /// <summary>
    /// Cult description
    /// </summary>
    [TextArea]
    public string       description;

    /// <summary>
    /// If the cult is unlocked
    /// </summary>
    public bool         unlocked;

    // Cult Spell references
    public SpellData basicSpellData;
    public SpellData movSpellData;
    public SpellData passiveSpellData;
}
