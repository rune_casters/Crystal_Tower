﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles talents disposition as a tree  (removed from the game so I removed comments)
/// </summary>
public class BuildingTree : MonoBehaviour
{
    public GameObject linePrefab;

    //private int talentPoints = 5;

    public Color unlockedBranch;

    public GameObject talentText;

    [HideInInspector] public bool onHover;

    // TO DO: RAYCAST 
    void Start ()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform branch = transform.GetChild(i);
            for (int j = 0; j < branch.childCount; j++)
            {
                Transform talent = branch.GetChild(j);
                BuildingTalent currentTalent = talent.GetComponent<BuildingTalent>();

                if (talent.GetComponent<BuildingTalent>().posteriorTalents != null)
                {
                    BuildingTalent[] posteriorTalents = talent.GetComponent<BuildingTalent>().posteriorTalents;
                    List<GameObject> lines = new List<GameObject>();


                    for (int k = 0; k < posteriorTalents.Length; k++)
                    {
                        Transform posteriorTalent = posteriorTalents[k].transform;
                        posteriorTalent.GetComponent<BuildingTalent>().addPreviousTalent(currentTalent);

                        GameObject line = CreateLine(talent, posteriorTalent);

                        posteriorTalent.GetComponent<BuildingTalent>().previousLines.Add(line);


                        // Create line from this talent to the next and add it to lines
                        lines.Add(line);
                    }

                    currentTalent.setLines(lines.ToArray());
                }
            }
        }
        
	}

    private GameObject CreateLine(Transform talent, Transform posteriorTalent)
    {
        GameObject line = Instantiate(linePrefab);
        Transform lineTransform = line.transform;

        lineTransform.SetParent(transform.parent.GetChild(0));
        lineTransform.SetAsFirstSibling();
        lineTransform.position = talent.position;

        line.GetComponent<RectTransform>().sizeDelta = new Vector2(Vector2.Distance(talent.position, posteriorTalent.position), 3);

        Vector3 dir = posteriorTalent.position - talent.position;
        lineTransform.rotation = Quaternion.FromToRotation(Vector3.right, dir);

        return line;
    }

    public void BranchUnlocked(BuildingTalent talent)
    {
        Transform branch = talent.transform.parent;

        bool toUnlock = true;

        for (int i = 0; i < branch.childCount; i++)
        {
            if(!branch.GetChild(i).GetComponent<BuildingTalent>().getUnlocked())
            {
                toUnlock = false;
                break;
            }
        }

        if (toUnlock)
        {
            branch.GetComponent<Image>().color = unlockedBranch;
        }
    }

    private void Update()
    {
        if (onHover)
        {
            talentText.transform.position = new Vector3( Camera.main.ScreenToWorldPoint(Input.mousePosition).x + 5, Camera.main.ScreenToWorldPoint(Input.mousePosition).y - 5, 40);
        }
    }

}
