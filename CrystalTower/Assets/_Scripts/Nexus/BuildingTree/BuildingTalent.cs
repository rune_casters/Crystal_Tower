﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Talent from a talent line (removed from the game so I removed comments)
/// </summary>
public class BuildingTalent : MonoBehaviour
{

    private int pointsIn;
    public int pointsToUnlock;

    private bool available;
    private bool unlocked;
    private bool onHover;

    private int talentToUnlock;
    private int talentsUnlocked;
     
    private List<BuildingTalent> previousTalents;
    public BuildingTalent[] posteriorTalents;

    [HideInInspector]
    public List<GameObject> previousLines;
    private GameObject[] posteriorLines;

    private BuildingTree buildingTreeReference;
    private Text pointsText;



    private void Awake()
    {
        AddPointerEvents();
        pointsText = transform.GetChild(0).GetComponent<Text>();
        buildingTreeReference = transform.parent.parent.GetComponent<BuildingTree>();

        pointsText.text = pointsIn + "/" + pointsToUnlock;
    }

    private void AddPointerEvents()
    {
        EventTrigger trigger = GetComponent<EventTrigger>();

        EventTrigger.Entry pointerEnter = new EventTrigger.Entry();
        EventTrigger.Entry pointerExit = new EventTrigger.Entry();
        EventTrigger.Entry pointerDown = new EventTrigger.Entry();

        pointerEnter.eventID = EventTriggerType.PointerEnter;
        pointerExit.eventID = EventTriggerType.PointerExit;
        pointerDown.eventID = EventTriggerType.PointerDown;

        pointerEnter.callback.AddListener((data) => { OnPointerEnterDelegate((PointerEventData)data); });
        pointerExit.callback.AddListener((data) => { OnPointerExitDelegate((PointerEventData)data); });
        pointerDown.callback.AddListener((data) => { OnPointerDownDelegate((PointerEventData)data); });

        trigger.triggers.Add(pointerEnter);
        trigger.triggers.Add(pointerExit);
        trigger.triggers.Add(pointerDown);
    }

    private void OnPointerEnterDelegate(PointerEventData data)
    {   
        if(!unlocked)
        {
            for (int i = 0; i < posteriorLines.Length; i++)
            {
                posteriorLines[i].GetComponent<Line>().HoverLine();
            }

            if (!available)
            {
                foreach (var previousLine in previousLines)
                {
                    if (previousLine.GetComponent<Image>().color != Color.green)
                    {
                        previousLine.GetComponent<Image>().color = Color.red;
                    }
                    
                }
            }
        }
      

        buildingTreeReference.onHover = true;

        buildingTreeReference.talentText.SetActive(true);
        buildingTreeReference.talentText.transform.position = data.position - new Vector2(- 10,10);
        buildingTreeReference.talentText.transform.GetChild(0).GetComponent<Text>().text = name;

        GetComponent<Outline>().enabled = true;

    }

    private void OnPointerExitDelegate(PointerEventData data)
    {
        buildingTreeReference.onHover = false;

        if (!unlocked)
        {
            for (int i = 0; i < posteriorLines.Length; i++)
            {
                posteriorLines[i].GetComponent<Line>().UnhoverLine();
            }

            if (!available)
            {
                foreach (var previousLine in previousLines)
                {
                    if (previousLine.GetComponent<Image>().color != Color.green)
                    {
                        previousLine.GetComponent<Image>().color = Color.white;
                    }
                    
                }
            }

             
        }
        

        buildingTreeReference.talentText.SetActive(false);
        GetComponent<Outline>().enabled = false;
    }

    private void OnPointerDownDelegate(PointerEventData data)
    {
        if (!unlocked && talentsUnlocked == talentToUnlock) Unlock();
    }

    public void setLines(GameObject[] _lines)
    {
        posteriorLines = _lines;
    }

    public void addPreviousTalent(BuildingTalent buildingTalent)
    {   
        if(previousTalents == null) previousTalents = new List<BuildingTalent>();

        previousTalents.Add(buildingTalent);
        talentToUnlock++;
    }

    public void Unlock()
    {

        if (pointsIn < pointsToUnlock - 1 )
        {
            pointsIn++;
            

        }
        else
        {
            pointsIn++;
            unlocked = true;

            transform.parent.parent.GetComponent<BuildingTree>().BranchUnlocked(this);
            UnlockPosteriorTalents();
            for (int i = 0; i < posteriorLines.Length; i++)
            {
                posteriorLines[i].GetComponent<Line>().UnlockLine();
            }

        }

        pointsText.text = pointsIn + "/" + pointsToUnlock;
    }

    private void UnlockPosteriorTalents()
    {
        for (int i = 0; i < posteriorTalents.Length; i++)
        {
            posteriorTalents[i].AddUnlockedTalent();
        }
    }

    private void AddUnlockedTalent()
    {
        talentsUnlocked++;

        if (talentsUnlocked == talentToUnlock) available = true;
    }

    public bool getUnlocked()
    {
        return unlocked;
    }
}
