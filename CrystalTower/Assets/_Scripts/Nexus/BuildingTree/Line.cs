﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Interface line drawer (removed from the game so I removed comments)
/// </summary>
public class Line : MonoBehaviour
{
    private Image lineImage;
    private Color normalLine;
    public Color hoverLine;
    public Color unlockedLine;

    private void Start()
    {
        lineImage = GetComponent<Image>();
        normalLine = lineImage.color;
    }

    public void HoverLine()
    {
        lineImage.color = hoverLine;
    }

    public void UnhoverLine()
    {
        lineImage.color = normalLine;
    }

    public void UnlockLine()
    {
        lineImage.color = unlockedLine;
    }


}
