﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles displaying item information based on type
/// </summary>
public class ItemDisplay : MonoBehaviour
{
    /// <summary>
    /// Reference to rarity colors data
    /// </summary>
    [SerializeField] private RarityColors rarityColors; 

    /// <summary>
    /// Reference to the name text
    /// </summary>
    [SerializeField] private TextMeshProUGUI nameText; 

    /// <summary>
    /// Reference to the description text
    /// </summary>
    [SerializeField] private TextMeshProUGUI descriptionText;

    /// <summary>
    /// Reference to the image 
    /// </summary>
    [SerializeField] private Image imageRef; 

    /// <summary>
    /// Reference to the outline effect
    /// </summary>
    [SerializeField] private Outline displayOutline;

    /// <summary>
    /// Reference to the camera used by the player in order to calculate World Coordinates to UI
    /// </summary>
    [SerializeField] private Camera cam;

    /// <summary>
    /// Displays given trinket's information
    /// </summary>
    public void UpdateDisplay(TrinketData trinketData)
    {
        // Get rarity color depending on trinket's rarity
        Color trinketColor = rarityColors.rarityColors[(int)trinketData.rarity];

        // Set text, color, description and sprite
        nameText.text = trinketData.name;
        nameText.color = trinketColor;
        descriptionText.text = trinketData.description;
        descriptionText.color = trinketColor * 0.9f;
        imageRef.sprite = trinketData.sprite;
        displayOutline.effectColor = trinketColor;
    }

    /// <summary>
    /// Displays given spell's information
    /// </summary>
    public void UpdateDisplay(SpellData spellData)
    {
        // Set text, color, description and sprite
        nameText.text = spellData.name;
        nameText.color = Color.white;
        descriptionText.text = spellData.description;
        descriptionText.color = Color.white * 0.9f;
        imageRef.sprite = spellData.sprite;
        displayOutline.effectColor = Color.white;
    }

    /// <summary>
    /// Update position calculating it from World to UI
    /// </summary>
    /// <param name="position"></param>
    public void UpdatePosition(Vector3 position)
    {
        // Calculate real UI position from object (follows item)
        Vector3 worldPos = cam.WorldToScreenPoint(position);
        transform.position = worldPos;
    }
}
