﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stores inventory data such as trinkets
/// </summary>
[CreateAssetMenu]
public class InventoryData : ScriptableObject
{
    /// <summary>
    /// List of every trinket
    /// </summary>
    private List<Trinket> trinketList = new List<Trinket>();

    /// <summary>
    /// Event raised when trinket is equiped
    /// </summary>
    [SerializeField] private GameEvent onTrinketEquipedEvent;

    /// <summary>
    /// Add trinket to list raising event
    /// </summary>
    public void AddTrinket(Trinket trinket)
    {
        trinketList.Add(trinket);
        onTrinketEquipedEvent.Raise();
    }
}
