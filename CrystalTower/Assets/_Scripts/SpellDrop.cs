﻿using UnityEngine;

/// <summary>
/// Stores data of a spell and if it's being sold (physical version of a spell that can be taken by the player)
/// </summary>
public class SpellDrop : MonoBehaviour
{
    /// <summary>
    /// If it's being sold
    /// </summary>
    public bool sold;

    /// <summary>
    /// Spell data that the player can take
    /// </summary>
    public SpellData spellData;
}
