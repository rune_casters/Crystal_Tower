﻿using UnityEngine;

/// <summary>
/// Handles and calculates enemies position and displays it on a compass (old way)
/// </summary>
public class Compass : MonoBehaviour
{
    /// <summary>
    /// Reference to the player (needed for the calculations)
    /// </summary>
    public Transform playerTransform;

    /// <summary>
    /// Target used to calculate pointer position
    /// </summary>
    public Transform target;

    /// <summary>
    /// Reference to the compass transform
    /// </summary>
    public RectTransform compassRect;

    /// <summary>
    /// Reference to the compass pointer transform
    /// </summary>
    public RectTransform pointerRect;

    /// <summary>
    /// Scale in which they are displayed
    /// </summary>
    float pointerScale;

    /// <summary>
    /// Sets position of element through compass
    /// </summary>
    private void Start()
    {
        Vector3[] v = new Vector3[4];
        compassRect.GetLocalCorners(v);
        pointerScale = Vector3.Distance(v[1], v[2]);
    }

    /// <summary>
    /// Updates it's position based on the angle that the player has with that target
    /// </summary>
    private void Update()
    {
        float angle = Vector3.SignedAngle(playerTransform.forward, target.position - playerTransform.position, playerTransform.up);
        angle = Mathf.Clamp(angle, -90, 90) / 180.0f * pointerScale;
        pointerRect.localPosition = new Vector3(angle, pointerRect.localPosition.y, pointerRect.localPosition.z);
    }
}
