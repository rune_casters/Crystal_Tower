﻿using UnityEngine;

/// <summary>
/// Stores data about the spawning type, method and count
/// </summary>
[CreateAssetMenu(menuName = "Enemies/Enemy Wave")]
public class EnemySpawnData : ScriptableObject
{
    /// <summary>
    /// Stores enemy type and their spawning position
    /// </summary>
    [System.Serializable]
    public struct EnemyData
    {
        /// <summary>
        /// Enemy type
        /// </summary>
        public EnemyStats enemyStats;
        /// <summary>
        /// Spawing position which can be used to spawn there
        /// </summary>
        public Vector3 spawnPosition;
    }

    /// <summary>
    /// If the group has to be spawned on random positions
    /// </summary>
    public bool randomGroup;

    /// <summary>
    /// How much enemies on the squad
    /// </summary>
    public int enemiesInSquad;

    /// <summary>
    /// Stores enemy datas used to spawn
    /// </summary>
    public EnemyData[] enemyData;

}

