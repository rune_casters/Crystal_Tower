﻿using System;
using UnityEngine;

/// <summary>
/// Stores Game Settings in data
/// </summary>
[CreateAssetMenu]
public class GameSettings : ScriptableObject
{
    /// <summary>
    /// Stores every type of possible resolution
    /// </summary>
    public Resolution[] resolutions;

    /// <summary>
    /// Stores every type of possible anti alising levels
    /// </summary>
    public int[] antiAliasingLevels = { 0, 2, 4 };

    /// <summary>
    /// Sets given resolution into the screen
    /// </summary>
    public void SetResolution(Resolution currentResolution)
    {
        Screen.SetResolution(currentResolution.width, currentResolution.height, Screen.fullScreenMode, currentResolution.refreshRate);
    }

    /// <summary>
    /// Sets given screen mode into the screen
    /// </summary>
    public void SetScreenMode(int screenMode)
    {
        Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, (FullScreenMode)screenMode, Screen.currentResolution.refreshRate);
    }

    /// <summary>
    /// Sets quality level into engine (Bad, Ultra etc..)
    /// </summary>
    /// <param name="qualityIndex"></param>
    public void SetQualitySettings(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    /// <summary>
    /// Sets anti aliasing level into engine
    /// </summary>
    public void SetAntiAliasing(int antiAliasing)
    {
        // Check if it exists
        if (Array.Exists(antiAliasingLevels, element => element == antiAliasing))
        {
            QualitySettings.antiAliasing = antiAliasing;
        }
    }

    /// <summary>
    /// Sets given engine timescale (used to pause)
    /// </summary>
    public void SetTimeScale(float value)
    {
        Time.timeScale = value;
    }
}
