﻿using UnityEngine;

/// <summary>
/// Stores the color of given rarity
/// </summary>
[CreateAssetMenu()]
public class RarityColors : ScriptableObject
{
    /// <summary>
    /// Types of rarity in the game
    /// </summary>
    public enum RARITY
    {
        Common,
        Uncommon,
        Rare,
        Epic,
        Legendary
    };

    // Array of colors used (through editor)
    public Color[] rarityColors;
}
