﻿using UnityEngine;

/// <summary>
/// Handles destructible objects using hittable interface
/// </summary>
public class Destructible : MonoBehaviour, IHittable
{
    /// <summary>
    /// Reference to prefab of post destruction meshes
    /// </summary>
    [SerializeField] private GameObject postDestructiblePrefab;

    /// <summary>
    /// Force used when hit
    /// </summary>
    [Range(1, 1000)]
    [SerializeField] private float explosionForce = 300;

    /// <summary>
    /// Radius used when hit
    /// </summary>
    [Range(1, 20)]
    [SerializeField] private float radius = 10;

    /// <summary>
    /// Hittable interface implementation that changes prefab to multiples broken meshes and explodes
    /// </summary>
    public void Hit(float damage, Vector3 hitPosition)
    {
        // Activate destructible prefab
        postDestructiblePrefab.SetActive(true);
        postDestructiblePrefab.transform.SetParent(transform.parent);

        // Get every collider around it
        Collider[] colliders = Physics.OverlapSphere(hitPosition, radius);

        // Loop through them and add force
        foreach (Collider collider in colliders)
        {
            Rigidbody rb = collider.GetComponent<Rigidbody>();
            if (rb)
                rb.AddExplosionForce(explosionForce, hitPosition, radius);
        }
        // Destroy original gameobject
        Destroy(gameObject);
    }
}
