﻿using UnityEngine;

/// <summary>
/// Handles hovering animation
/// </summary>
public class Hover : MonoBehaviour
{
    /// <summary>
    /// Given hover vector with speed
    /// </summary>
    public Vector3 hoverVector;

    /// <summary>
    /// Starting hovering position
    /// </summary>
    private Vector3 startingPosition;

    private void Start()
    {
        // Cache starting position
        startingPosition = transform.position;
    }

    /// <summary>
    /// Hovers depending on the given hover vector using time
    /// </summary>
    public void Update()
    {
        transform.position = new Vector3(transform.position.x, startingPosition.y, transform.position.z) + hoverVector * Mathf.Cos(Time.time);
    }
}
