﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Handles general game events
/// </summary>
public class GameState : MonoBehaviour
{
    /// <summary>
    /// Static reference accesible from entire game
    /// </summary>
    public static GameState instance;

    /// <summary>
    /// Reference to the pause panel
    /// </summary>
    [SerializeField] private GameObject pausePanel = null;

    /// <summary>
    /// Reference to the death panel
    /// </summary>
    [SerializeField] private GameObject deathPanel = null;

    /// <summary>
    /// Reference to the demo end panel
    /// </summary>
    [SerializeField] private GameObject demoEndPanel;

    /// <summary>
    /// Reference to the cult panel
    /// </summary>
    [SerializeField] private GameObject cultPanel;

    /// <summary>
    /// If the player can die (debug purposes)
    /// </summary>
    [SerializeField] private bool deathEnabled;

    /// <summary>
    /// If the cursor is locked from the start
    /// </summary>
    [SerializeField] private bool cursorLockOnStart = true;

    /// <summary>
    /// If the game is paused
    /// </summary>
    public bool GamePaused { get; set; }

    private void Awake()
    {   
        // Get instance at first
        instance = this;
    }

    private void Start()
    {
        // Lock cursor and disable pause panel in case it's open
        Mage.rigidFps.mouseLook.SetCursorLock(cursorLockOnStart);
        pausePanel.SetActive(false);
    }

    private void Update()
    {
        // Check for pause
        if ((Input.GetButtonDown("Pause") || Input.GetButtonDown("Pause Joystick")) && CheckPauseAble()) TogglePause();
    }

    /// <summary>
    /// Check if the player can pause the game (no panels are active)
    /// </summary>
    /// <returns></returns>
    public bool CheckPauseAble()
    {
        return !deathPanel.activeSelf && !cultPanel.activeSelf;
    }

    /// <summary>
    /// Toggles pause
    /// </summary>
    public void TogglePause()
    {
        // Sets time scale, pause panel and mouselock depending if it's pausing or unpausing game
        GamePaused = !GamePaused;
        if (GamePaused) Time.timeScale = 0;
        else Time.timeScale = 1;
        pausePanel.SetActive(!pausePanel.activeSelf);
        Mage.rigidFps.mouseLook.SetCursorLock(!Mage.rigidFps.mouseLook.lockCursor);
    }

    /// <summary>
    /// Pause game on death and show death panel
    /// </summary>
    public void DeathEvent()
    {
        if (deathEnabled)
        {
            GamePaused = true;
            Time.timeScale = 0;
            deathPanel.SetActive(true);
        }
    }

    /// <summary>
    /// Stops demo (NOT USED)
    /// </summary>
    public void DemoEnd()
    {
        GamePaused = !GamePaused;
        if (GamePaused) Time.timeScale = 0;
        else Time.timeScale = 1;
        demoEndPanel.SetActive(!demoEndPanel.activeSelf);
        Mage.rigidFps.mouseLook.SetCursorLock(!Mage.rigidFps.mouseLook.lockCursor);
    }

    /// <summary>
    /// Revives (only on debug)
    /// </summary>
    public void ResumeDeath()
    {
        Time.timeScale = 1;
        GamePaused = false;
        //  Mage.instance.IsDead = false;
        //  Mage.instance.CurrentHealth = Mage.instance.BaseMaxHealth;
        deathPanel.SetActive(false);
        Mage.rigidFps.mouseLook.SetCursorLock(true);
    }

    /// <summary>
    /// Restarts scene
    /// </summary>
    public void RestartGame()
    {
        // Restarts current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1;
    }
    
    /// <summary>
    /// Loads asyncronously the menu
    /// </summary>
    public void GoToMenu()
    {
        StartCoroutine(Menu.LoadYourAsyncScene("Scene_Menu"));
        Time.timeScale = 1;
    }

    /// <summary>
    /// Exits the game
    /// </summary>
    public void ExitGame()
    {
        // If the user is on the editor then exit editor
        #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
        // Otherwise just exit game application
        #else
                Application.Quit ();
        #endif
    }

}
