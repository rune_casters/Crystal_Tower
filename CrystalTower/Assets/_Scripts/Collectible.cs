﻿using UnityEngine;

/// <summary>
/// Stores collectible data such as quantity or type
/// </summary>
public class Collectible : MonoBehaviour
{
    /// <summary>
    /// Collectible types
    /// </summary>
    public enum Type
    {
        Skull,
        Orb
    }

    /// <summary>
    /// Current collectible type
    /// </summary>
    public Type type;

    /// <summary>
    /// Collectible quantity
    /// </summary>
    public int Quantity { get; set; }

}
