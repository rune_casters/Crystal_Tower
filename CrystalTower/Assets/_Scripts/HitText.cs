﻿using TMPro;
using UnityEngine;

/// <summary>
/// Handles Damage text on enemies 
/// </summary>
public class HitText : MonoBehaviour
{
    /// <summary>
    /// Hit data which is sent through a HitEvent
    /// </summary>
    private HitData hitData;

    /// <summary>
    /// Current calculated position (World to UI)
    /// </summary>
    private Vector3 currentCalc;

    /// <summary>
    /// Reference to the UI text
    /// </summary>
    [SerializeField] public TextMeshProUGUI textMesh;

    /// <summary>
    /// Updates position and visibility
    /// </summary>
    public void Update()
    {
        // Calculate position from world to UI
        currentCalc = Camera.main.WorldToScreenPoint(hitData.position);
        // Check depth
        textMesh.enabled = currentCalc.z > 0;
        transform.position = currentCalc;
    }

    /// <summary>
    /// Sets initial data
    /// </summary>
    public void Initialize(HitData _hitData)
    {
        hitData = _hitData;
        // Set position on a random range
        hitData.position += new Vector3(Random.Range(-1f, 1f), 0.5f, 0);
        // Do not display floating points
        textMesh.text = hitData.value.ToString("F0");
    }

    /// <summary>
    /// Stops the text destroying it
    /// </summary>
    public void Stop()
    {
        Destroy(gameObject);
    }
}
