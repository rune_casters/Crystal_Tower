﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Old Shop interface (removed comments due to not using it anymore)
/// </summary>
public class ShopUI : MonoBehaviour
{
    [Header("Shop data")]
    public TrinketData[] trinketPool;
    public uint trinketsToGenerate;
    public GameObject uiShopItemPrefab;
    public GameObject trinketPrefab;
    public GameEvent onShopCloseEvent;
    [Header("Player data")]
    public InventoryData inventoryData;
    public Currencies currencies;
    public SpellSystem spellSystem;
    [Header("UI References")]
    [SerializeField] private Transform trinketContainer;
    [SerializeField] private Transform spellUpgradeContainer;
    [SerializeField] private ItemDisplay uiItemDisplay;

    private GameObject currentHoveredItem;
    private GameObject currentSelectedItem;


    private void Awake()
    {
        GenerateTrinkets();
        GenerateSpells();
    }

    private void Update()
    {
        if (currentHoveredItem)
        {
            uiItemDisplay.transform.position = Input.mousePosition;

            if (Input.GetMouseButtonDown(0))
            {
                if (currentSelectedItem)
                    currentSelectedItem.GetComponent<Outline>().effectColor = Color.black;

                currentSelectedItem = currentHoveredItem;
                currentSelectedItem.GetComponent<Outline>().effectColor = Color.yellow;
            }
        }
    }

    public void GenerateTrinkets()
    {
        List<int> list = new List<int>();

        for (int i = 0; i < trinketsToGenerate; i++)
        {
            int randNumber = Random.Range(0, trinketPool.Length);

            while (list.Contains(randNumber))
                randNumber = Random.Range(0, trinketPool.Length);

            list.Add(randNumber);

            TrinketUI trinketUI = Instantiate(uiShopItemPrefab, trinketContainer).AddComponent<TrinketUI>();
            trinketUI.trinketData = trinketPool[randNumber];
            trinketUI.UpdateUI();

            EventTrigger trinketEventTrigger = trinketUI.GetComponent<EventTrigger>();

            EventTrigger.Entry hoverEntry = new EventTrigger.Entry();
            EventTrigger.Entry exitEntry = new EventTrigger.Entry();
            hoverEntry.eventID = EventTriggerType.PointerEnter;
            exitEntry.eventID = EventTriggerType.PointerExit;
            hoverEntry.callback.AddListener( (eventData) => { HoverTrinket(trinketUI.gameObject); });
            exitEntry.callback.AddListener((eventData) => { Unhovered(); });
            trinketEventTrigger.triggers.Add(hoverEntry);
            trinketEventTrigger.triggers.Add(exitEntry);
        }
    }

    public void GenerateSpells()
    {
        for (int i = 0; i < spellSystem.spells.Length; i++)
        {
            SpellData upgradeSpell = spellSystem.spells[i].spell.upgradeSpell;
            if (upgradeSpell)
            {
                //SpellDrop spellUpgradeUI = Instantiate(uiShopItemPrefab, spellUpgradeContainer).AddComponent<SpellDrop>();
                //spellUpgradeUI.spellData = upgradeSpell;
                //spellUpgradeUI.UpdateUI();

                //EventTrigger trinketEventTrigger = spellUpgradeUI.GetComponent<EventTrigger>();

                //EventTrigger.Entry hoverEntry = new EventTrigger.Entry();
                //EventTrigger.Entry exitEntry = new EventTrigger.Entry();
                //hoverEntry.eventID = EventTriggerType.PointerEnter;
                //exitEntry.eventID = EventTriggerType.PointerExit;
                //hoverEntry.callback.AddListener((eventData) => { HoverSpell(spellUpgradeUI.gameObject); });
                //exitEntry.callback.AddListener((eventData) => { Unhovered(); });
                //trinketEventTrigger.triggers.Add(hoverEntry);
                //trinketEventTrigger.triggers.Add(exitEntry);
            }
        }
    }

    public void HoverTrinket(GameObject trinketHovered)
    {
        currentHoveredItem = trinketHovered;
        uiItemDisplay.gameObject.SetActive(true);
        uiItemDisplay.UpdateDisplay(currentHoveredItem.GetComponent<TrinketUI>().trinketData);
    }

    public void HoverSpell(GameObject spellHovered)
    {
        currentHoveredItem = spellHovered;
        uiItemDisplay.gameObject.SetActive(true);
        uiItemDisplay.UpdateDisplay(currentHoveredItem.GetComponent<SpellDrop>().spellData);
    }
    public void Unhovered()
    {
        uiItemDisplay.gameObject.SetActive(false);
        currentHoveredItem = null;
    }


    public void Buy()
    {
        bool bought = false;

        if (currentSelectedItem)
        {
            TrinketUI currentTrinket = currentSelectedItem.GetComponent<TrinketUI>();
            SpellDrop currentSpellUpgrade = currentSelectedItem.GetComponent<SpellDrop>();

            if (currentTrinket)
            {
                TrinketData currentSelectedTrinketData = currentTrinket.trinketData;

                // Check cost against currencies 
                if (currencies.Skulls >= currentSelectedTrinketData.cost)
                {
                    currencies.RemoveSkulls(currentSelectedTrinketData.cost);

                    Trinket trinket = Instantiate(trinketPrefab, Vector3.zero, Quaternion.identity).GetComponent<Trinket>();
                    trinket.trinketData = currentSelectedTrinketData;

                    trinket.Equip();
                    inventoryData.AddTrinket(trinket);
                    Destroy(currentSelectedItem);

                    bought = true;
                }
                else
                {
                    // No money
                }
            }
            else if (currentSpellUpgrade)
            {
                SpellData currentSelectedSpellData = currentSpellUpgrade.spellData;

                // Check cost against currencies 
                if (currencies.Skulls >= currentSelectedSpellData.cost)
                {
                    currencies.RemoveSkulls(currentSelectedSpellData.cost);

                    spellSystem.ChangeSpell(currentSelectedSpellData);

                    Destroy(currentSelectedItem);

                    bought = true;
                }
                else
                {
                    // No money
                }
            }

            
        }
        else
        {
            // No Trinket Selected
        }

        if (bought)
            Close();
    }

    public void Close()
    {
        onShopCloseEvent.Raise();
        if (currentSelectedItem)
            currentSelectedItem.GetComponent<Outline>().effectColor = Color.black;
        currentHoveredItem = null;
        currentSelectedItem = null;
    }
}
