﻿using TMPro;
using UnityEngine;

/// <summary>
/// Handles currencies interface
/// </summary>
public class CurrenciesUI : MonoBehaviour
{
    /// <summary>
    /// Reference to the player currencies data
    /// </summary>
    public Currencies mageCurrencies;

    /// <summary>
    /// Reference to the skull count text
    /// </summary>
    [Header("UI References")]
    [SerializeField] private TextMeshProUGUI skullsCount;

    /// <summary>
    /// Reference to the souls count text
    /// </summary>
    [SerializeField] private TextMeshProUGUI soulsCount;

    private void Start()
    {
        // Updates skull count 
        UpdateSkullCount();
        // Updates souls count 
        UpdateSoulsCount();
    }

    /// <summary>
    /// Updates text with current skulls value
    /// </summary>
    public void UpdateSkullCount()
    {
        skullsCount.text = mageCurrencies.Skulls + "";
    }

    /// <summary>
    /// Updates text with current souls value
    /// </summary>
    public void UpdateSoulsCount()
    {
        soulsCount.text = mageCurrencies.Souls + "";
    }
}
