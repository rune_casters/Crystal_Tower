﻿using UnityEngine;

/// <summary>
/// Handles Flask interface (removed from the game so I removed comments)
/// </summary>
public class FlaskUI : MonoBehaviour
{
    public Flask flask;

    [Header("UI References")]
    [SerializeField] private MageUI.DynamicBar flaskChargeBar;
    [SerializeField] private MageUI.DynamicBar orbChargeBar;

    private void Start()
    {
        flaskChargeBar.Initialize(flask.currentCharges / flask.maxCharges);
        orbChargeBar.Initialize(flask.currentOrbCharge / flask.maxOrbCharge);
    }

    public void UpdateFlaskChargeBar()
    {
        flaskChargeBar.UpdateBar(flask.currentCharges, flask.maxCharges, flask.currentCharges.ToString("F0") + " / " + flask.maxCharges.ToString("F0"));
    }

    public void UpdateOrbChargeBar()
    {
        orbChargeBar.UpdateBar(flask.currentOrbCharge, flask.maxOrbCharge, flask.currentOrbCharge.ToString("F0") + " / " + flask.maxOrbCharge.ToString("F0"));
    }

    private void Update()
    {
        orbChargeBar.LerpBar();
    }
}
