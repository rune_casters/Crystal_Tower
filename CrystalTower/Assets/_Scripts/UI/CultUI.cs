﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles cult interface display
/// </summary>
public class CultUI : MonoBehaviour
{
    /// <summary>
    /// Reference to the cult text display
    /// </summary>
    [SerializeField] private TextMeshProUGUI cultName = null;

    /// <summary>
    /// Reference to the cult description display
    /// </summary>
    [SerializeField] private TextMeshProUGUI cultDescription = null;

    /// <summary>
    /// Reference to the cult image display
    /// </summary>
    [SerializeField] private Image cultSprite = null; 

    /// <summary>
    /// Handles the display of given spell
    /// </summary>
    [System.Serializable]
    public struct SpellContainerUI
    {
        /// <summary>
        /// Reference to the spell name text display
        /// </summary>
        [SerializeField] private TextMeshProUGUI spellName;

        /// <summary>
        /// Reference to the spell description text display
        /// </summary>
        [SerializeField] private TextMeshProUGUI spellDescription;

        /// <summary>
        /// Reference to the spell sprite image display
        /// </summary>
        [SerializeField] private Image spellSprite; 

        /// <summary>
        /// Updates display with given spell data
        /// </summary>
        public void UpdateContent(SpellData spellData)
        {   
            // If it exists
            if (spellData)
            {
                // Update fields
                spellName.text = spellData.name;
                spellDescription.text = spellData.description;
                spellSprite.sprite = spellData.sprite;
            }
        }
    }

    /// <summary>
    /// Field for basic spell
    /// </summary>
    [SerializeField] private SpellContainerUI basicSpellUI;

    /// <summary>
    /// Field for movement spell
    /// </summary>
    [SerializeField] private SpellContainerUI movSpellUI;

    /// <summary>
    /// Field for passive spell
    /// </summary>
    [SerializeField] private SpellContainerUI passiveSpellUI;

    /// <summary>
    /// Stores every cult data (filled through editor)
    /// </summary>
    public CultData[] cultDatas;

    /// <summary>
    /// Current selected cult
    /// </summary>
    private int currentCult;

    private void Start()
    {
        // Disables cult interface in case
        transform.GetChild(0).gameObject.SetActive(false);
        currentCult = 0;
        // Loads cult data
        SetCultData(cultDatas[currentCult]);
    }

    /// <summary>
    /// Loads and updates given cult data content
    /// </summary>
    public void SetCultData(CultData cultData)
    {
        // Updates fields
        cultName.text = cultData.name;
        cultDescription.text = cultData.description;
        cultSprite.sprite = cultData.sprite;

        // Updates spell fields
        basicSpellUI.UpdateContent(cultData.basicSpellData);
        movSpellUI.UpdateContent(cultData.movSpellData);
        passiveSpellUI.UpdateContent(cultData.passiveSpellData);
    }

    /// <summary>
    /// Displays next cult on the list
    /// </summary>
    public void NextCult()
    {
        // Go back to the first one if at the end
        if (++currentCult >= cultDatas.Length)
            currentCult = 0;

        // Update fields
        SetCultData(cultDatas[currentCult]);
    }

    /// <summary>
    /// Displays previous cult on the list
    /// </summary>
    public void PrevCult()
    {
        // Go to the last one if at the start
        if (--currentCult < 0)
            currentCult = cultDatas.Length - 1;

        // Update fields
        SetCultData(cultDatas[currentCult]);
    }

}
