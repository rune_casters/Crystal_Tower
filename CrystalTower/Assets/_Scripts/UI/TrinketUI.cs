﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Trinket container for the UI (removed from the game so I removed comments)
/// </summary>
public class TrinketUI : MonoBehaviour
{
    public TrinketData trinketData;
    private Image imageRef;
    private TextMeshProUGUI costText;

    public void UpdateUI()
    {
        imageRef = transform.GetChild(0).GetComponent<Image>();
        costText = imageRef.transform.GetChild(0).GetComponent<TextMeshProUGUI>();

        imageRef.sprite = trinketData.sprite;
        costText.text = trinketData.cost + "";
    }
}
