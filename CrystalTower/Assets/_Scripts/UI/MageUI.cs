﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles player bars and spells interface
/// </summary>
public class MageUI : MonoBehaviour
{
    /// <summary>
    /// Reference to the player's stats
    /// </summary>
    public Stats mageStats;

    /// <summary>
    /// Reference to the player's spell system
    /// </summary>
    public SpellSystem spellSystem;

    /// <summary>
    /// Reference to the dynamic health bar
    /// </summary>
    [Header("UI References")]
    [SerializeField] private DynamicBar healthBar;

    /// <summary>
    /// Reference to the dynamic shield bar (NOT USED)
    /// </summary>
    [SerializeField] private DynamicBar shieldBar;

    /// <summary>
    /// Reference to the cooldown bar used by the spells
    /// </summary>
    [SerializeField] private CooldownBar[] cdBars;

    /// <summary>
    /// Handles bar with lerping values
    /// </summary>
    [System.Serializable]
    public struct DynamicBar
    {
        /// <summary>
        /// Reference to image representing current bar
        /// </summary>
        public Image currentBar;

        /// <summary>
        /// Reference to image representing the bar that changes from old to current bar
        /// </summary>
        public Image differenceBar;

        /// <summary>
        /// Reference to the text used to indicate bar value
        /// </summary>
        public TextMeshProUGUI barText;

        /// <summary>
        /// Calculated old percentage (used for lerping purposes)
        /// </summary>
        private float oldPercentage;

        /// <summary>
        /// Current bar percentage 
        /// </summary>
        private float currentPercentage;

        /// <summary>
        /// Initializes bar with given value
        /// </summary>
        public void Initialize(float value)
        {
            oldPercentage = value;
        }

        /// <summary>
        /// Updates current bar with given values and text
        /// </summary>
        public void UpdateBar(float currentValue, float maxValue, string text)
        {
            currentPercentage = currentValue / maxValue;
            barText.text = text;
            currentBar.fillAmount = currentPercentage;
        }

        /// <summary>
        /// Linearly interpolates between old and new bar in order to obtain smooth transition
        /// </summary>
        public void LerpBar()
        {
            if (oldPercentage != currentPercentage)
                differenceBar.fillAmount = oldPercentage = Mathf.Lerp(oldPercentage, currentPercentage, Time.deltaTime * 5);

        }

    }

    /// <summary>
    /// Handles spells icons effects
    /// </summary>
    [System.Serializable]
    public struct CooldownBar
    {
        /// <summary>
        /// Reference to spell image
        /// </summary>
        public Image currentBar;

        /// <summary>
        /// Reference to cooldown background
        /// </summary>
        public Image image;

        /// <summary>
        /// Reference to cooldown text
        /// </summary>
        public TextMeshProUGUI barText;

        /// <summary>
        /// Current cooldown percentage
        /// </summary>
        private float currentPercentage;

        /// <summary>
        /// Updates current bar with given values
        /// </summary>
        public void UpdateBar(float currentValue, float maxValue)
        {
            // Enable cooldown text only when 
            barText.gameObject.SetActive(currentValue > 0);

            // Updates cooldown effect and text
            if(currentValue >= 0)
            {
                currentPercentage = currentValue / maxValue;
                barText.text = currentValue.ToString("F1");
                currentBar.fillAmount = 1.0f - currentPercentage;
            }
            else
            {
                currentBar.fillAmount = 1.0f;
            }
        }
        
        /// <summary>
        /// Initializes spell UI with given sprites
        /// </summary>
        public void Initialize(Sprite _sprite, Sprite _runeBar)
        {
            image.sprite = _sprite;
            currentBar.sprite = _runeBar;
        }
    }

    /// <summary>
    /// Initializes bars and updates spells
    /// </summary>
    private void Start()
    {
        healthBar.Initialize(mageStats.health.CurrentValue / mageStats.health.maxValue);
        //shieldBar.Initialize(mageStats.shield.CurrentValue / mageStats.shield.maxValue);
        UpdateSpells();
    }

    /// <summary>
    /// Updates health bar with current health value
    /// </summary>
    public void UpdateHealthBar()
    {
        healthBar.UpdateBar(mageStats.health.CurrentValue, mageStats.health.maxValue, mageStats.health.CurrentValue.ToString("F0") + " / " + mageStats.health.maxValue.ToString("F0"));
    }

    /// <summary>
    /// Updates shield bar with current shield value
    /// </summary>
    public void UpdateShieldBar()
    {
        shieldBar.UpdateBar(mageStats.shield.CurrentValue, mageStats.shield.maxValue, mageStats.shield.CurrentValue.ToString("F0") + " / " + mageStats.shield.maxValue.ToString("F0"));
    }

    /// <summary>
    /// Initializes spells UI
    /// </summary>
    public void UpdateSpells()
    {
        for (int i = 0; i < cdBars.Length; i++)
        {
            cdBars[i].Initialize(spellSystem.spells[i].spell.sprite, spellSystem.spells[i].spell.normalRune);
        }
    }

    /// <summary>
    /// Linear interpolates bars and updates spell cooldowns
    /// </summary>
    public void Update()
    {
        healthBar.LerpBar();
       // shieldBar.LerpBar();
        for (int i = 0; i < cdBars.Length; i++)
        {
            if(spellSystem.spells[i].CD >= 0)
                cdBars[i].UpdateBar(spellSystem.spells[i].CD, spellSystem.spells[i].spell.baseCd);
            else
                cdBars[i].UpdateBar(spellSystem.GcdTimeLeft,spellSystem.baseGcdTime);
        }

    }

}
