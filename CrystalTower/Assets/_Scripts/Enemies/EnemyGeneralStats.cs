﻿using UnityEngine;

/// <summary>
/// Stores general enemy events and enemy kills
/// </summary>
[CreateAssetMenu(menuName = "Enemies/General")]
public class EnemyGeneralStats : ScriptableObject
{
    /// <summary>
    /// Enemies killed by player count
    /// </summary>
    public int EnemiesKilled { get; set; }

    /// <summary>
    /// Whenever an enemy is hit
    /// </summary>
    public HitEvent     onEnemyHit;

    /// <summary>
    /// Whenever an enemy has been killed
    /// </summary>
    public GameEvent    onEnemyKilled;
}
