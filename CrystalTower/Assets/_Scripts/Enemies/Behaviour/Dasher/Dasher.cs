﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Handles the dasher AI
/// </summary>
public class Dasher : Enemy
{
    /// <summary>
    /// The prefab of the dasher projectile
    /// </summary>
    [SerializeField] private GameObject projectile;
    /// <summary>
    /// the sootpoints of the dasher, its hands
    /// </summary>
    [SerializeField] private Transform[] shootPoint;
    /// <summary>
    /// Tells from which shootpoint its gonna shoot next
    /// </summary>
    private int nextShootpoint;

    /// <summary>
    /// States of the dasher
    /// </summary>
    private enum States
    {
        CHASE,
        ATTACK,
        REPOS
    };
    private States state;
    /// <summary>
    /// its true while the enemy is dashing
    /// </summary>
    private bool dashing;
    /// <summary>
    /// True when the enemy can attack again after the attack speed 
    /// </summary>
    private bool canAttack;

    /// <summary>
    /// This function is called when the dasher is on attack state, and handles its attack
    /// </summary>
    protected override void Attack()
    {
        // Stop the dasher
        agent.speed = 0;
        // make it loot at the target position
        transform.LookAt(new Vector3(target.position.x,transform.position.y, target.position.z));
        // if the attack speed cd has ended
        if (canAttack)
        {
            canAttack = false;
            // set the attack animation
            anim.SetTrigger("Attack");
        }
    }
    /// <summary>
    /// This function is calles when the enemy is on reposition state
    /// </summary>
    private void Repos()
    {
        // Security check
        if (!dashing)
        {
            // set dashing to true
            dashing = true;
            // double the saher speed
            agent.speed = currentSpeed * 2;
            // get a random position in a donut and add the current position
            Vector3 randomPos = -GetRandomInDonut(14, 16, 360);
            randomPos += transform.position;
            // get the closest point of the navmesh of that random point
            NavMeshHit hit;
            NavMesh.SamplePosition(randomPos, out hit, 10, 1);
            // set the final positio to the navmesh point position
            Vector3 finalPosition = hit.position;
            agent.SetDestination(finalPosition);
        }
    }

    /// <summary>
    /// Every time the attack animation throws a projectile calls this function
    /// </summary>
    public void Shoot()
    {
        // get the hand which is shooting the projectile
        nextShootpoint = (nextShootpoint + 1) % 2;
        // create the projectile make it look the target, add the damage and add force to it
        GameObject clone = Instantiate(projectile, shootPoint[nextShootpoint].position, shootPoint[nextShootpoint].rotation);
        clone.transform.LookAt(new Vector3(target.position.x, target.position.y + 0.5f, target.position.z));
        clone.GetComponent<EnemyProjectile>().Damage = currentDamage;

        clone.GetComponent<Rigidbody>().AddForce(clone.transform.forward * 1500);
    }
    /// <summary>
    /// this function si called when the dash animation ends
    /// </summary>
    public void DashEnd()
    {
        // Set dashing to false so it can dash again
        dashing = false;
        // if the dash ended on a point where the dasher can attack the target change the state to Attack, if it cant attack change the state to chase
        if (CanAttackTarget())
        {
            anim.SetBool("Chasing", false);
            state = States.ATTACK;
        }
        else
        {
            anim.SetBool("Chasing", true);
            state = States.CHASE;
        }
    }

    /// <summary>
    /// This functios is called when the dasher is on chase state, goes to the target until it gets a position where it can attack the target
    /// </summary>
    protected override void FollowTarget()
    {
        agent.speed = currentSpeed;
        agent.SetDestination(target.position);
        if (CanAttackTarget()) state = States.ATTACK;
    }

    /// <summary>
    /// Initializes all the variables and adds delay to the first shot wave
    /// </summary>
    protected new void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
        Invoke("AttackCoolDown", 0.5f);
    }
    /// <summary>
    /// Handles the enemy states and calls the acording function
    /// </summary>
    protected new void Update()
    {
        base.Update();
        if (!stuned)
        {
            switch (state)
            {
                case States.CHASE:
                    FollowTarget();
                    break;
                case States.ATTACK:
                    Attack();
                    break;
                case States.REPOS:
                    Repos();
                    break;
            }
        }

    }

    /// <summary>
    /// This function is called when the attack animation ends
    /// </summary>
    private void AttackEnded()
    {
        // Sets the delay to the next attack
        Invoke("AttackCoolDown", stats.AttackCooldown);
        state = States.REPOS;
    }

    /// <summary>
    /// Lets the dasher attack again
    /// </summary>
    private void AttackCoolDown()
    {
        canAttack = true;
    }

    /// <summary>
    /// Changes the agent speed and the current speed
    /// </summary>
    /// <param name="value"> The new speed value</param>
    public override void SetSpeed(float value)
    {
        agent.speed = currentSpeed = value;
    }
    /// <summary>
    /// Calls the base vulnerable function
    /// </summary>
    /// <param name="_receivedDamageMul">the value of the multiplier</param>
    public override void Vulnerable(float _receivedDamageMul)
    {
        base.Vulnerable(_receivedDamageMul);
    }
    /// <summary>
    /// Handles the enemy hit
    /// </summary>
    /// <param name="damage"> the damage dealed to the dasher</param>
    /// <param name="hitPosition"> the position where the dasher has been hit</param>
    public override void Hit(float damage, Vector3 hitPosition)
    {
        // if the dasher is dashing doesnt receive damage
        if(!dashing)
            base.Hit(damage, hitPosition);
    }

    /// <summary>
    /// Stuns the dasher if the value ist true and set the speed to the currentSpeed else
    /// </summary>
    /// <param name="state"> if its true stuns the target, resets the speed else</param>
    public override void Stun(bool state)
    {
        stuned = state;
        if (stuned)
        {
            agent.speed = 0;
        }
        else
        {
            agent.speed = currentSpeed;
        }

    }
    /// <summary>
    /// Destroys the gameObject and spawns the ragdoll
    /// </summary>
    protected override void Die()
    {
        base.Die();
        Destroy(gameObject);
        Instantiate(stats.corpse, transform.position, transform.rotation);
    }
}
