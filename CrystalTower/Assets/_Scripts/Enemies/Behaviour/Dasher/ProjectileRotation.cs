﻿using UnityEngine;
/// <summary>
/// Handles the rotation of the dashers projectile so it doesnt need to be animated
/// </summary>
public class ProjectileRotation : MonoBehaviour
{
    /// <summary>
    /// The game object to rotate
    /// </summary>
    public GameObject rotable;
    /// <summary>
    /// The rotation of the game object
    /// </summary>
    private Vector3 rotation;
    /// <summary>
    /// The speed to rotate
    /// </summary>
    public float speed;
    /// <summary>
    /// the explosion effect fot the projectile
    /// </summary>
    public Transform effect;
    /// <summary>
    /// the mesh of the projectile
    /// </summary>
    public Transform bottle;
    /// <summary>
    /// Gets a random rotation on x y z
    /// </summary>
    void Start()
    {
        rotation = new Vector3(Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f));
    }
    /// <summary>
    /// Rotates the gameobject
    /// </summary>
    void Update()
    {
        rotable.transform.Rotate(rotation * Time.deltaTime * speed);
    }
    /// <summary>
    /// activates the explosion effect and disables the mesh on collision
    /// </summary>
    /// <param name="other"> The other object on the collision</param>
    private void OnTriggerEnter(Collider other)
    {
        if(effect) effect.gameObject.SetActive(true);
        if(bottle) bottle.gameObject.SetActive(false);
    }
}
