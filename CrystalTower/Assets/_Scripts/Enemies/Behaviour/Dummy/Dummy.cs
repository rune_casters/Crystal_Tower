﻿using UnityEngine;

/// <summary>
/// Enemy class that just can be hit but never dies
/// </summary>
public class Dummy : Enemy
{

    public override void SetSpeed(float newSpeed)
    {
        print("my speed is: " + newSpeed);
    }

    public override void Stun(bool state)
    {
        throw new System.NotImplementedException();
    }

    protected override void Attack()
    {
        throw new System.NotImplementedException();
    }

    protected override void FollowTarget()
    {
        throw new System.NotImplementedException();
    }

    public override void Hit(float damage, Vector3 hitPosition)
    {
        stats.enemyGeneralStats.onEnemyHit.Raise(new HitData(damage * receivedDamageMul, hitPosition));
        GetComponent<Animation>().Play();
    }

    protected new void Update()
    {
        base.Update();
    }
}
