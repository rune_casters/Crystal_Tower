﻿using UnityEngine;

/// <summary>
/// Base stats for each the enemies
/// </summary>
[CreateAssetMenu(menuName ="Enemies/Stats")]
public class EnemyStats : ScriptableObject
{
    /// <summary>
    /// Some stats an events are the same for all enemies
    /// </summary>
    public EnemyGeneralStats    enemyGeneralStats;
    /// <summary>
    /// The name of the enemy
    /// </summary>
    public string               enemyName;
    [Header("Spawn things")]
    /// <summary>
    /// the enemy prefab
    /// </summary>
    public GameObject           prefab;
    /// <summary>
    /// The enemy Ragdoll
    /// </summary>
    public GameObject           corpse;
    /// <summary>
    /// The enemy difficulty value
    /// </summary>
    public int dificultyValue;
    
    [Header("Base Stats")]
    /// <summary>
    /// The enemy base vision range
    /// </summary>
    [SerializeField]  private float visionRange;
    /// <summary>
    /// The enemy base attack range
    /// </summary>
    [SerializeField]  private float attackRange;
    /// <summary>
    /// The enemy base attack speed
    /// </summary>
    [SerializeField]  private float attackSpeed;
    /// <summary>
    /// The enemy damage
    /// </summary>
    [SerializeField]  private float damage;
    /// <summary>
    /// The enemy base speed
    /// </summary>
    [SerializeField]  private float speed;
    /// <summary>
    /// The enemy base heath
    /// </summary>
    [SerializeField]  private float health;
    /// <summary>
    /// The enemy vision layer mask
    /// </summary>
    public LayerMask VisionLayerMask;
    /// <summary>
    /// The loot table of the enemy
    /// </summary>
    public LootTable lootTable;

    #region Properties
    public float VisionRange
    {
        get { return visionRange; }
        private set { visionRange = value; }
    }
    public float AttackRange
    {
        get { return attackRange; }
        private set { attackRange = value; }
    }
    public float Damage
    {
        get { return damage; }
        private set { damage = value; }
    }
    public float Speed
    {
        get { return speed; }
        private set { speed = value; }

    }
    public float AttackCooldown
    {
        get { return attackSpeed; }
        private set { attackSpeed = value; }
    }
    public float Health
    {
        get { return health; }
        private set { health = value; }
    }
    #endregion
}
