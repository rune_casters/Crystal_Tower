﻿using UnityEngine;

/// <summary>
/// Handles Frog AI
/// </summary>
public class Frog : Enemy
{
    /// <summary>
    /// The cd between frog jumps
    /// </summary>
    [SerializeField] private float timeBetweenJumps;
    /// <summary>
    /// The explosion that the frog spawns on land
    /// </summary>
    public EnemyExplosion explosionPrefab;
    /// <summary>
    /// The states of the frog
    /// </summary>
    private enum States
    {
        JUMPING
    }
    private States state;
    /// <summary>
    /// The RigidBody of the from
    /// </summary>
    private Rigidbody rb;
    /// <summary>
    /// Checks if the frog is jumping
    /// </summary>
    private bool jumping;
    /// <summary>
    /// The collider of the frog
    /// </summary>
    private BoxCollider boxCollider;

    /// <summary>
    /// This functio is called when the frog touches the ground and spawns the explosion
    /// </summary>
    protected override void Attack()
    {
        // if i was jumping and im not stunned spawn the explosion and set the damage
        if (jumping && !stuned)
        {
            EnemyExplosion explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
            explosion.damage = currentDamage;
        }
    }
    /// <summary>
    /// This function is calles repeatidly every jumpCD value
    /// </summary>
    protected override void FollowTarget()
    {
        // if its not jumping and its not rooted neither stuned sets the jump animation
        if (!jumping && !stuned && currentSpeed != 0)
        {
            anim.SetTrigger("Jump");
        }
    }
    /// <summary>
    /// This function is called when the Jump animation starts jumping
    /// </summary>
    public void Jump()
    {
        // get the directio to the target
        Vector3 direction;

        direction = target.position - transform.position;
        // normalize it and add a y value to jump
        direction.Normalize();
        direction += Vector3.up;
        // create a clamped speed for the animation
        float speed = Mathf.Clamp(Vector3.Distance(transform.position, target.position) / 15,0.3f,1);
        anim.SetFloat("jumpSpeed", 1);
        anim.SetFloat("jumpSpeed", 1 + 1 - speed);
        // add force to the frog jump
        rb.AddForce(direction * 250 * speed, ForceMode.Impulse);
    }
    
    /// <summary>
    /// Initialize all the vairables and start invoking FollowTarget
    /// </summary>
    protected new void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody>();
        jumping = false;
        InvokeRepeating("FollowTarget", timeBetweenJumps, timeBetweenJumps);
        anim = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider>();
    }

    /// <summary>
    /// Looks the target while grounded and Groundchecks
    /// </summary>
    protected new void Update()
    {
        base.Update();
        if(CanSeeTarget() && !jumping)
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(new Vector3(target.position.x, 1, target.position.z) - new Vector3(transform.position.x, 1, transform.position.z)), 2f * Time.deltaTime);
        GroundCheck();        
    }
    
    /// <summary>
    /// if collides with the player deal damage
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            mage.mageStats.Hit(stats.Damage);
    }

    /// <summary>
    /// Set speed to the frog
    /// </summary>
    /// <param name="newSpeed"> The new speed value</param>
    public override void SetSpeed(float newSpeed)
    {
        currentSpeed = newSpeed;
    }
    /// <summary>
    /// Set the stun value to the frog
    /// </summary>
    /// <param name="state">the stun value</param>
    public override void Stun(bool state)
    {
        stuned = state;
    }

    /// <summary>
    /// Destroys the frog GameObject
    /// </summary>
    protected override void Die()
    {
        base.Die();
        Destroy(gameObject);
    }
    /// <summary>
    /// Checks if the frog is grounded
    /// </summary>
    private void GroundCheck()
    {
        // if the frog legs are 0.1 above the ground 
        if (Physics.Linecast(transform.position + Vector3.up * 0.3f, transform.position - transform.up*0.4f, 1))
        {
            // remove the trigger from the collider
            boxCollider.isTrigger = false;
            // spawn the explosion
            Attack();
            // set jumping to false
            jumping = false;
        }
        else
        {
            // if im on the air im jumping
            jumping = true;
            // set the collider trigger to true
            boxCollider.isTrigger = true;
        }
    }
    
}

