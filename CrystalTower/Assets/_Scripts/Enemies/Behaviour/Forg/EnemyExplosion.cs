﻿using UnityEngine;
/// <summary>
/// Handles dealing damage to the player when triggers with it
/// </summary>
public class EnemyExplosion : MonoBehaviour
{
    [HideInInspector] public float damage;

    /// <summary>
    /// Destroys the GameObject on a second if hasnt been destroyed
    /// </summary>
    private void Start()
    {
        Destroy(gameObject, 1f);
    }
    /// <summary>
    /// If the player triggers with the GameObject deal damage
    /// </summary>
    /// <param name="other"> The other GameObject in the collision. </param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            other.GetComponent<Mage>().Hit(damage);
    }
}
