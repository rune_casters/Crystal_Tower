﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all flying enemies
/// </summary>
public class FlyingEnemy : Enemy
{
    /// <summary>
    /// The path the enemy is going to follow
    /// </summary>
    protected List<Node> path;
    /// <summary>
    /// The current node of the path
    /// </summary>
    protected int currentNode = 0;
    /// <summary>
    /// the time needed to update the path
    /// </summary>
    protected float searchRate = 0.5f;
    /// <summary>
    /// the timer to update the path
    /// </summary>
    protected float searchTime = 0;
    /// <summary>
    /// the direction of the enemy
    /// </summary>
    protected Vector3 direction;
    /// <summary>
    /// if the enemy has a path
    /// </summary>
    protected bool hasPath;

    // Debug variables to draw the path
    // protected List<GameObject> cubePath;
    // public Color _color;
    

    // Enemy abstract function overrides
    public override void SetSpeed(float newSpeed)
    {
        throw new System.NotImplementedException();
    }

    public override void Stun(bool state)
    {
        throw new System.NotImplementedException();
    }

    protected override void Attack()
    {
        throw new System.NotImplementedException();
    }

    protected override void FollowTarget()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Find a new path every searchRate
    /// </summary>
    protected void UpdatePath()
    {
        searchTime += Time.deltaTime;
        if (searchTime >= searchRate)
        {
            searchTime = 0;
            FindPath();
        }
    }

    // Debug function to print the path
    //protected void PrintPath()
    //{
    //    foreach (GameObject cube in cubePath)
    //    {
    //        Destroy(cube);
    //    }
    //    foreach (Node node in path)
    //    {
    //        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
    //        cube.GetComponent<BoxCollider>().isTrigger = true;
    //        cube.GetComponent<MeshRenderer>().material.color = _color;
    //        cube.transform.position = GetVector3FromNode(node);
    //        cube.transform.localScale *= 2;
    //        cubePath.Add(cube);
    //    }
    //}

    /// <summary>
    /// Find a new path
    /// </summary>
    protected void FindPath()
    {
        // Refresh the flying enemy current node
        pathfinder.SetNodeValue(myNode, 0);
        myNode = GetNodeFromTransform(transform);
        pathfinder.SetNodeValue(myNode, 1);
        // Get the target current node
        targetNode = GetNodeFromTransform(target);
        // Find a path from the enemy node to the target node
        List<Node> testPath = pathfinder.findPath(myNode.x, myNode.y, myNode.z, targetNode.x, targetNode.y, targetNode.z);
        // If there is an available path 
        if(testPath != null)
        {
            // Update the path
            path = testPath;
            // Reset the current node
            currentNode = 0;
            // set hasPath to true
            hasPath = true;
            // change and normalize the direction of the enemy
            direction = GetVector3FromNode(path[currentNode]) - transform.position;
            direction.Normalize();
            //PrintPath();
        }
    }

}
