﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Handles the suicide AI
/// </summary>
public class Suicide : FlyingEnemy
{
    /// <summary>
    /// The states of the suicide
    /// </summary>
    private enum States
    {
        CHASE,
        ATTACK
    };
    private States state;
    /// <summary>
    /// if the suicide started attacking the target
    /// </summary>
    private bool locked;
    /// <summary>
    /// The suicide explosion prefab
    /// </summary>
    public EnemyExplosion explosionPrefab;

    /// <summary>
    /// Sets the new speed value to the current speed
    /// </summary>
    /// <param name="newSpeed"> the new speed value</param>
    public override void SetSpeed(float newSpeed)
    {
        currentSpeed = newSpeed;
    }
    /// <summary>
    /// Stuns the suicide
    /// </summary>
    /// <param name="state">stuns if true</param>
    public override void Stun(bool state)
    {
        stuned = state;
    }
    /// <summary>
    /// This is called when the suicide is on attack state
    /// </summary>
    protected override void Attack()
    {
        // if hasnt a locked direction set up the direction
        if (!locked)
        {
            anim.SetTrigger("Attack");
            direction = target.position - transform.position;
            direction -= Vector3.up * 0.7f;
            direction.Normalize();
            locked = true;
            transform.LookAt(target.position);
        }
        // Translate the suicide to that direction
        transform.Translate(direction * Time.deltaTime * stats.Speed*6, Space.World);
    }

    /// <summary>
    /// This is called when the suicide is on chase state
    /// </summary>
    protected override void FollowTarget()
    {
        // Update the path on FlyingEnemy
        UpdatePath();
        // Lerp the rotation between the next position and the actual rotation
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(GetVector3FromNode(path[currentNode]) - transform.position),3f * Time.deltaTime);
        // Translate the suicide towards the direction
        transform.Translate(direction * Time.deltaTime * stats.Speed, Space.World);
        // If its near enough to its next node
        if (hasPath && Vector3.Distance(transform.position, GetVector3FromNode(path[currentNode])) < 0.3f)
        {
            // Go to the next node or to the start if the player is unaccesable
            if (currentNode < path.Count - 1) currentNode++;
            else currentNode = 0;
            // set the new direction and normalize it
            direction = GetVector3FromNode(path[currentNode]) - transform.position;
            direction.Normalize();
        }
    }
    /// <summary>
    /// Initialize all the variables
    /// </summary>
    protected new void Start()
    {
        base.Start();
        path = new List<Node>();
        //cubePath = new List<GameObject>();
        state = States.CHASE;
        searchTime = searchRate;
        locked = false;
        anim = GetComponent<Animator>();
        transform.LookAt(target);
    }
    /// <summary>
    /// Handles the states and the state change
    /// </summary>
    protected new void Update()
    {
        if(CanAttackTarget()) state = States.ATTACK;
        
        switch (state)
        {
            case States.CHASE:
                base.Update();
                FollowTarget();
                break;
            case States.ATTACK:
                Attack();
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// When collides with the player or a building spawn the explosion 
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        // if the collided object is a building just explode if the suicide is locked, always explode if the suicide collides with the player
        if (other.gameObject.layer == LayerMask.NameToLayer("Default") && locked || other.CompareTag("Player"))
        {
            // Spawn the explosion add the damage rise the enemyKilled event and destroy the gameObject
            EnemyExplosion explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
            explosion.damage = currentDamage;
            stats.enemyGeneralStats.onEnemyKilled.Raise();
            Destroy(gameObject);
        }
    }
    /// <summary>
    /// Calls enemy Die and spawns the ragdoll
    /// </summary>
    protected override void Die()
    {
        base.Die();
        // spawn the ragdoll and destroy the gameObbject
        Instantiate(stats.corpse, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
