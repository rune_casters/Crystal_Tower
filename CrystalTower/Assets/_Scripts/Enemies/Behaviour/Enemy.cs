﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Base class for every enemy in the game
/// </summary>
public abstract class Enemy : MonoBehaviour, IHittable
{
    /// <summary>
    /// The base stats of the enemy
    /// </summary>
    public EnemyStats stats;
    /// <summary>
    /// Reference to the player data
    /// </summary>
    public Stats mageStats;
    /// <summary>
    /// The target of the enemy
    /// </summary>
    protected Transform target;
    /// <summary>
    /// Reference to the player script
    /// </summary>
    protected Mage mage;
    /// <summary>
    /// The agent of the enemy
    /// </summary>
    protected NavMeshAgent agent;
    /// <summary>
    /// The animator of the enemy
    /// </summary>
    protected Animator anim;
    /// <summary>
    /// The mesh of the enemy
    /// </summary>
    [SerializeField] protected SkinnedMeshRenderer enemyMesh;
    /// <summary>
    /// The base emissive texture the enemy uses
    /// </summary>
    protected Texture emissive;
    /// <summary>
    /// The base emissive color the enemy uses
    /// </summary>
    protected Color emissiveColor;
    /// <summary>
    /// The radius of the sphere the enemy uses to know if can see and attack the player
    /// </summary>
    [SerializeField] protected float visualCone = 1;

    // Current health speed and damage
    [HideInInspector] public float currentHealth;
    [HideInInspector] public float currentSpeed;
    [HideInInspector] public float currentDamage;

    /// <summary>
    /// If its true doesnt let the enemy move
    /// </summary>
    protected bool stuned;
    /// <summary>
    /// The recieved damage multiplayer for vulneability windows
    /// </summary>
    protected float receivedDamageMul = 1;

    /// <summary>
    /// Reference to the pathfinder
    /// </summary>
    protected OneShotPathfinder pathfinder;
    /// <summary>
    /// The size of the node used by the pathfinder
    /// </summary>
    protected float nodeSize;
    /// <summary>
    /// The offset on each axis of the walkable array
    /// </summary>
    protected Vector3 startPos;
    /// <summary>
    /// The node where the enemy is
    /// </summary>
    protected Node myNode;
    /// <summary>
    /// The node where the target is
    /// </summary>
    protected Node targetNode;
    /// <summary>
    /// Timer to update the node where the enemy and the target are
    /// </summary>
    protected float updateTime;
    /// <summary>
    /// Needed time to update the node where the enemy and the target are
    /// </summary>
    protected float updateRate = 0.5f;

    /// <summary>
    /// Abstract class that every enemy uses on a different way to attack
    /// </summary>
    protected abstract void Attack();
    /// <summary>
    /// Abstract class that every enemy uses on a different way to go to the target
    /// </summary>
    protected abstract void FollowTarget();
    /// <summary>
    /// Abstract class that every enemy uses on a different way to slow down
    /// </summary>
    public abstract void SetSpeed(float newSpeed);
    /// <summary>
    /// Abstract class that every enemy uses on a different way to get stunned
    /// </summary>
    public abstract void Stun(bool state);


    /// <summary>
    /// Initializes the variables
    /// </summary>
    protected virtual void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = currentSpeed = stats.Speed;
        currentDamage = stats.Damage;
        currentHealth = stats.Health;
        target = mageStats.playerTransform;
        mage = target.GetComponent<Mage>();
        if (enemyMesh.material.HasProperty("_EmissionMap"))
        {
            emissive = enemyMesh.material.GetTexture("_EmissionMap");
            emissiveColor = enemyMesh.material.GetColor("_EmissionColor");
        }
        updateTime = updateRate;
    }

    /// <summary>
    /// Every update rate updates the node of the enemy and the target
    /// </summary>
    protected virtual void Update()
    {
        updateTime += Time.deltaTime;
        if (updateTime >= updateRate)
        {
            UpdateNode();
        }
    }

    /// <summary>
    /// Updates the enemy and the target nodes
    /// </summary>
    protected virtual void UpdateNode()
    {
        // If the enemy has a pathfinder
        if (pathfinder != null)
        {
            // set the enemy last node to walkable
            pathfinder.SetNodeValue(myNode, 0);
            // get the new enemy node
            myNode = GetNodeFromTransform(transform);
            // set the new enemy node to not walkable
            pathfinder.SetNodeValue(myNode, 1);
            // set the target node to walkable always even if an enemy is in
            targetNode = GetNodeFromTransform(target);
            pathfinder.SetNodeValue(targetNode, 0);
        }
    }

    /// <summary>
    /// Base function for when an enemy gets hit
    /// </summary>
    /// <param name="damage"> The damage dealed to the enemy. </param>
    /// <param name="hitPosition"> The position where the enemy has been hit. </param>
    public virtual void Hit(float damage, Vector3 hitPosition)
    {
        // Subtract to the current health the damage multiplied by the damage multiplicator
        currentHealth -= damage * receivedDamageMul;
        // Rise onEnemyHit event
        stats.enemyGeneralStats.onEnemyHit.Raise(new HitData(damage * receivedDamageMul, hitPosition));
        // Remove the emission texture to make all the anemy body glow
        enemyMesh.material.SetTexture("_EmissionMap", null);
        // Set the emission color to red
        enemyMesh.material.SetColor("_EmissionColor", new Color(1.0f, 0.2f, 0.0f, 1.0f) * 3.0f);
        // If the current health is less or equal to 0 kill the enemy
        if (currentHealth <= 0) Die();
        // Else remove the hit effect from the enemy
        else Invoke("RemoveHitEffect", 0.1f);
    }

    /// <summary>
    /// Sets to the base emission texture and color to the enemy mesh
    /// </summary>
    protected void RemoveHitEffect()
    {
        enemyMesh.material.SetColor("_EmissionColor", emissiveColor);
        enemyMesh.material.SetTexture("_EmissionMap", emissive);
    }

    /// <summary>
    /// Adds the _receivedDamageMult to the current damage mul, where 0.1 is a 10% increase and -0.1 is a 10% decrease
    /// </summary>
    /// <param name="_receivedDamageMul"></param>
    public virtual void Vulnerable(float _receivedDamageMul)
    {
        receivedDamageMul += _receivedDamageMul;
    }

    /// <summary>
    /// Base function that every enemy uses when dies
    /// </summary>
    protected virtual void Die()
    {
        // Rise onEnemyKilled event
        stats.enemyGeneralStats.onEnemyKilled.Raise();
        // If the enemy can spawn something spawn the loot
        if (stats.lootTable)
            SpawnLoot();
    }

    /// <summary>
    /// Creates a GameObject with SpawnLoot and tells it to spawn something from the loot table
    /// </summary>
    private void SpawnLoot()
    {
        LootSpawn lootSpawn = new GameObject("Loot").AddComponent<LootSpawn>();
        lootSpawn.transform.position = transform.position;
        lootSpawn.SpawnLoot(stats.lootTable);
    }

    /// <summary>
    /// Checks if the enemy can attack the target
    /// </summary>
    /// <returns> True if the enemy can see ht target and its on the enemy's attack range </returns>
    protected bool CanAttackTarget()
    {
        //if the target is on the enemy attack range
        if (Vector3.Distance(target.position, transform.position) < stats.AttackRange + agent.radius)
        {
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, visualCone, target.position - transform.position + Vector3.up * 1.5f, out hit, stats.AttackRange, stats.VisionLayerMask))
            {
                // And the enemy can see the target
                if (hit.transform.CompareTag("Player")) return true;
            }
        }
        return false;
    }
    /// <summary>
    /// Checks if the enemy can see the target
    /// </summary>
    /// <returns> True if the enemy can see the target </returns>
    protected bool CanSeeTarget()
    {
        //if the target is on the enemy vision range
        if (Vector3.Distance(target.position, transform.position) < stats.VisionRange + agent.radius)
        {
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, visualCone, target.position - transform.position + Vector3.up * 1.5f, out hit, stats.VisionRange, stats.VisionLayerMask))
            {
                // And can see the target
                if (hit.transform.CompareTag("Player")) return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Gets a random point on a 2D donut 
    /// </summary>
    /// <param name="min"> the minimum distance of the point from the center </param>
    /// <param name="max"> the maximum distance of the point from the center </param>
    /// <param name="maxAngle"> The angle that restricts the the point position. </param>
    /// <returns></returns>
    protected Vector3 GetRandomInDonut(float min, float max, float maxAngle)
    {
        // Get the rotation on the xz plane
        float rot = Random.Range(1f, maxAngle);
        // Get the vector of the direction with that angle on the xz plane
        Vector3 direction = Quaternion.AngleAxis(rot, Vector3.up) * Vector3.forward;
        // Create a ray on that direction
        Ray ray = new Ray(Vector3.zero, direction);
        // Return a random point of that ray within the min and max space
        return ray.GetPoint(Random.Range(min, max));
    }

    /// <summary>
    /// Converts the unity vector3 position to a Node with x y z position for the pathfinder
    /// </summary>
    /// <param name="transform"> The transform from which its needed to get the node </param>
    /// <returns> Returns the node position for the pathfinder. </returns>
    protected Node GetNodeFromTransform(Transform transform)
    {
        int deltaX = (int)((transform.position.x - startPos.x + 1) / nodeSize);
        int deltaZ = (int)((transform.position.z - startPos.z + 1) / nodeSize);
        int deltaY = (int)((transform.position.y - startPos.y + 1) / nodeSize);

        return new Node(deltaX, deltaY, deltaZ);
    }

    /// <summary>
    /// Converts a Node to a Vector3 
    /// </summary>
    /// <param name="node"> The node from which is needed to get the position in world space</param>
    /// <returns> The world space position of the node. </returns>
    protected Vector3 GetVector3FromNode(Node node)
    {
        int deltaX = (int)(node.x * nodeSize + startPos.x);
        int deltaZ = (int)(node.z * nodeSize + startPos.z);
        int deltaY = (int)(node.y * nodeSize + startPos.y);

        return new Vector3(deltaX, deltaY, deltaZ);
    }
    /// <summary>
    /// Initilizes the pathfinder and sets the node size and the node start position
    /// </summary>
    /// <param name="_walkableArea"> The array with the information where the enemy can walk to</param>
    /// <param name="_startPos"> The world position of the start of the nodes,</param>
    /// <param name="_nodeSize"> The size of the nodes. </param>
    public void SetWalkableArea(int[,,] _walkableArea, Vector3 _startPos, float _nodeSize)
    {
        nodeSize = _nodeSize;
        startPos = _startPos;
        pathfinder = new OneShotPathfinder(_walkableArea);
    }


}

/// <summary>
/// Interface for every GameObject that can be hit
/// </summary>
public interface IHittable
{
    void Hit(float damage, Vector3 hitPosition);
}
