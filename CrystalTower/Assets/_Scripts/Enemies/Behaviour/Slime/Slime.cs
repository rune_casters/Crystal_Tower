﻿using UnityEngine;
/// <summary>
/// Handles the slime AI
/// </summary>
public class Slime : Enemy
{
    /// <summary>
    /// The states of the Slime
    /// </summary>
    private enum States
    {
        CHASE,
        ATTACK
    };
    private States state;
    /// <summary>
    /// The actual generation of the slime 
    /// </summary>
    private int generation;
    /// <summary>
    /// Lets the Slime attack
    /// </summary>
    private bool canAttack;
    /// <summary>
    /// Doesnt let the slime to get hit while doing de die animaion
    /// </summary>
    private bool dead;
    /// <summary>
    /// The number of 3rd generation slimes killed
    /// </summary>
    private static int miniSlimesKilled;

    /// <summary>
    /// This function is called while the slime is on attack state
    /// </summary>
    protected override void Attack()
    {
        // Stop the target
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
        // If can attack the target
        if (canAttack)
        {
            // start the attack animation and the attackCooldownCountdown
            canAttack = false;
            anim.SetTrigger("Attack");
            anim.SetBool("Chasing", false);
            Invoke("AttackEnded", stats.AttackCooldown);
        }
    }
    /// <summary>
    /// This function is called when the Slime is on Chase state
    /// </summary>
    protected override void FollowTarget()
    {
        agent.isStopped = false;
        anim.SetBool("Chasing", true);
        agent.SetDestination(target.position);
    }

    /// <summary>
    /// Spawns the new Generation and initilizes the new gen variables
    /// </summary>
    private void Spawn()
    {
        GameObject clone = Instantiate(stats.prefab, transform.position + GetRandomInDonut(0.5f, 2, 360), Quaternion.identity);
        clone.transform.localScale = new Vector3(0.75f / generation, 0.75f / generation, 0.75f / generation);
        Slime cloneSlime = clone.GetComponent<Slime>();
        cloneSlime.Start();
        cloneSlime.generation = generation;
        cloneSlime.pathfinder = pathfinder;
        cloneSlime.startPos = startPos;
        cloneSlime.nodeSize = nodeSize;
        cloneSlime.anim.SetTrigger("Revive");

    }

    /// <summary>
    /// Initializes all the variables
    /// </summary>
    protected new void Start()
    {
        base.Start();
        if(generation > 0)
        {
            SetSpeed(stats.Speed + 6 * generation);
            anim.SetFloat("AnimSpeed", 1 + generation * 1.5f);
            currentHealth = stats.Health * 0.75f / generation;
            currentDamage = stats.Damage * 0.75f / generation;
        }
        state = States.CHASE;
        anim = GetComponent<Animator>();
    }
    /// <summary>
    /// Handles the states and the state changes
    /// </summary>
    protected new void Update()
    {
        base.Update();
        if (!stuned && !dead)
        {
            if (CanAttackTarget()) state = States.ATTACK;
            else state = States.CHASE;

            switch (state)
            {
                case States.CHASE:
                    FollowTarget();
                    break;
                case States.ATTACK:
                    Attack();
                    break;
            }
        }

    }
    
    /// <summary>
    /// This function is called when the attack animation ends
    /// </summary>
    private void AttackEnded()
    {
        canAttack = false;
    }

    /// <summary>
    /// Sets the new speed value to the agent and the current speed
    /// </summary>
    /// <param name="value"> The new speed value</param>
    public override void SetSpeed(float value)
    {
        agent.speed = currentSpeed = value;
    }
    /// <summary>
    /// Calls to the enemy Vulnerable function
    /// </summary>
    /// <param name="_receivedDamageMul"> the value to add to the recieved damage multiplicator</param>
    public override void Vulnerable(float _receivedDamageMul)
    {
        base.Vulnerable(_receivedDamageMul);
    }
    /// <summary>
    /// Calls Enemy Hit function
    /// </summary>
    /// <param name="damage"> The damage dealed to the slime</param>
    /// <param name="hitPosition">Where the slime has been hit</param>
    public override void Hit(float damage, Vector3 hitPosition)
    {
        base.Hit(damage, hitPosition);
    }
    /// <summary>
    /// Stuns the slasher if the state is true and sets the agent speed to the current speed else
    /// </summary>
    /// <param name="state"> True if the target needs to be stuned false else</param>
    public override void Stun(bool state)
    {
        stuned = state;
        if (stuned)
        {
            agent.speed = 0;
        }
        else
        {
            agent.speed = currentSpeed;
        }

    }

    /// <summary>
    /// If isnt dying remove the emissive and starts the die animation
    /// </summary>
    protected override void Die()
    {
        if(!dead)
        {
            // remove the hit visual
            RemoveHitEffect();
            // set the bool to true
            dead = true;
            // if its the 3rd gen
            if(generation == 2)
            {
                // and the palyer has killed 4 mini slimes
                if ((++miniSlimesKilled) % 4 == 0)
                {
                    // Call enemy Die
                    base.Die(); 
                }
            }
            // start the Die animation and stop the slime
            anim.SetTrigger("Die");
            agent.isStopped = true;
            agent.velocity = Vector3.zero;
        }
    }
    /// <summary>
    /// Every slime spawns 2 slimes more for a total of 2 times
    /// </summary>
    private void NextGen()
    {
        // if isnt the 3rd generation
        if (++generation < 3)
        {
            for (int i = 0; i < 2; i++)
            {
                // Spawn 2 new slimes
                Spawn();
            }
        }
    }

    /// <summary>
    /// This functions is called when the die animation ends
    /// </summary>
    private void DeadEnd()
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// This function is called from the Attack animation 
    /// </summary>
    public void DealDamage()
    {
        // if the player is near enough to get damage deal damage
        if (Vector3.Distance(transform.position, target.position) < stats.AttackRange * 1.3)
        {
            mage.mageStats.Hit(currentDamage);
        }

    }
}
