﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Handles the Sniper AI
/// </summary>
public class Sniper : Enemy
{
    /// <summary>
    /// The cast time until the sniper shots
    /// </summary>
    [SerializeField] private float castTime;
    /// <summary>
    /// the offset on the y axis to aim the target
    /// </summary>
    [SerializeField] private float yOffset;
    /// <summary>
    /// The eyes of the sniper for the linecast
    /// </summary>
    [SerializeField] private Transform eyes;
    /// <summary>
    /// The reay shootpoint
    /// </summary>
    [SerializeField] private Transform ray;
    /// <summary>
    /// Where the implosion starts
    /// </summary>
    [SerializeField] private Transform implosion;
    /// <summary>
    /// The states of the Sniper
    /// </summary>
    private enum States
    {
        IDLE,
        ATTACK
    };
    private States state;
    /// <summary>
    /// The line renderer the sniper uses to aim the player
    /// </summary>
    private LineRenderer lr;
    /// <summary>
    /// Timer for shooting
    /// </summary>
    private float timeToShoot;

    /// <summary>
    /// This function is called when the sniper is on attack state
    /// </summary>
    protected override void Attack()
    {
        // if can attack the target
        if(CanAttackTarget())
        {
            // if can see the target with the offset
            RaycastHit hit;
            if (Physics.Linecast(transform.position, new Vector3(target.position.x, target.position.y + yOffset, target.position.z), out hit, stats.VisionLayerMask) && timeToShoot >= 0)
            {
                // if just started casting activate the implosion and start the cast animation
                if (timeToShoot == 0)
                {
                    implosion.gameObject.SetActive(true);
                    anim.SetBool("Casting", true);
                }
                // Add time to the timer
                timeToShoot += Time.deltaTime;
                // Set the line renderer positions
                Vector3[] laserPoints = { eyes.position, new Vector3(hit.transform.position.x, hit.transform.position.y + yOffset, hit.transform.position.z) };
                lr.SetPositions(laserPoints);
                // make the sniper look to the player
                transform.LookAt(new Vector3(hit.point.x, transform.position.y, hit.point.z));
                // if the cast time has passed dont let the sniper continue casting
                if (timeToShoot > castTime)
                {
                    timeToShoot = -1;
                }
            }
        }
        // if the sniper cant attack the target set it to idle and restart the cast time and effects
        else
        { 
            anim.SetBool("Casting", false);
            ray.gameObject.SetActive(false);
            implosion.gameObject.SetActive(false);
            state = States.IDLE;
            timeToShoot = 0;
            lr.enabled = false;

        }
    }

    /// <summary>
    /// Initialize the variables
    /// </summary>
    protected new void Start()
    {
        base.Start();
        state = States.IDLE;
        lr = GetComponent<LineRenderer>();
        target = FindObjectOfType<Mage>().transform;
        anim = GetComponent<Animator>();
    }

    /// <summary>
    /// Handles the Sniper states
    /// </summary>
    protected new void Update()
    {
        base.Update();
        if (!stuned)
        {
            switch (state)
            {
                case States.IDLE:
                    Idle();
                    break;
                case States.ATTACK:
                    Attack();
                    break;
            }
        }
    }

    /// <summary>
    /// This is called when the sniper is on Idle state
    /// </summary>
    private void Idle()
    {
        // If can see the target look at him
        if (CanSeeTarget()) transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
        // if can attack the target
        if (CanAttackTarget() && timeToShoot == 0)
        {
            lr.enabled = true;
            state = States.ATTACK;
        }
    }

    /// <summary>
    /// This functions is called from the attack animation
    /// </summary>
    /// <returns></returns>
    public IEnumerator Shoot()
    {
        // disable the line renderer
        lr.enabled = false;
        // make the ray transform look the player and activate it so the effect starts
        ray.LookAt(target);
        ray.gameObject.SetActive(true);
        // if the distance between the player and the las point of the line renderer is enough deal damage to the player
        if (Vector3.Distance(target.position, lr.GetPosition(1)) < 2.5f)
        {
            mage.mageStats.Hit(stats.Damage);
            //mage.mageStats.DestroyShield();
        }
        // Set the state and animation to idle
        state = States.IDLE;
        anim.SetBool("Casting", false);
        // Set off the implosion effect
        implosion.gameObject.SetActive(false);
        // Wait for the cast cd and set off the ray effect and let the sniper cast again
        yield return new WaitForSeconds(3.0f);
        ray.gameObject.SetActive(false);
        timeToShoot = 0;
    }

    /// <summary>
    /// The sniper is istatic so it cant follow the target
    /// </summary>
    protected override void FollowTarget()
    {
        //cant move
    }

    /// <summary>
    /// The sniper cant move so it cant get slowed
    /// </summary>
    /// <param name="newSpeed"> The new speed value</param>
    public override void SetSpeed(float newSpeed)
    {
        // cant move
    }

    /// <summary>
    /// The sniper cant be stuned
    /// </summary>
    /// <param name="state"></param>
    public override void Stun(bool state)
    {
        // cant be stunned
    }

    /// <summary>
    /// Calls Enemy Die destroys the gameObject and spawns the ragdoll
    /// </summary>
    protected override void Die()
    {
        base.Die();
        //destroy the gameObject
        Destroy(gameObject);
        // Spawn the ragdoll
        Instantiate(stats.corpse, transform.position, transform.rotation);
    }
}
