﻿using System.Collections.Generic;
using UnityEngine;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 * ONE SHOT PATHFINDER                                                         *
 * Copyright (c) 2010 Ángel Rodríguez Ballesteros                              *
 *                                                                             *
 * Distributed under the Boost Software License, Version 1.0.                  *
 * See https://www.boost.org/LICENSE_1_0.txt                                   *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *   Version: 0.9.0                                                            *
 * Developer: Angel                                                            *
 *   Created: 26.Jun.10                                                        *
 *  Modified: 20.Apr.19                                                        *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * TO DO                                                                       *
 *                                                                             *
 * .Improving the path optimization to avoid walking on the corners of hard    *
 *  tiles.                                                                     *
 * .Adding the possibility of using valued maps.                               *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

public class OneShotPathfinder
{
    private static int MAX_DISTANCE = 2147483647;        // Max positive signed int value
    private static int MAX_USABLE_STACK = 32768;

    private static int[,,] map;                          // Reference to the given map
    private int mapX;                                    // Map dimensions
    private int mapY;
    private int mapZ;

    private byte[,,] moveControlMatrix;                  // Matrix of the same size of the map to mark
                                                         //  the tiles that have been visited
    private int maxDepth;                                // Max depth allowed for recursive calls (or path length)

    private int firstResultDepth;                        // Length of the first unoptimized path found
    private Node[] firstResult;                          // Nodes  of the first unoptimized path found

    private int endX;                                    // Target coordinates
    private int endY;
    private int endZ;

    private int dx;                                      // Local variables moved out from moveTo()
    private int dy;                                      //  to save room from the stack
    private int dz;
    private int bestIndex;
    private int bestDelta;


    // CONSTRUCTOR
    //
    public OneShotPathfinder(int[,,] givenMap)
    {
        map = givenMap;
        mapX = map.GetLength(0);
        mapZ = map.GetLength(1);
        mapY = map.GetLength(2);
        maxDepth = MAX_USABLE_STACK / (8 + 8 + 4) * 4;                  // Stack+Locals+Args_size

        moveControlMatrix = new byte[mapX,mapZ,mapY];
    }

    // FIND PATH
    //
    public List<Node> findPath(int startX, int startY, int startZ, int _endX, int _endY, int _endZ)
    {
        if (findFirstPath(startX, startY, startZ, _endX, _endY, _endZ))
        {
            return (getOptimizedResult());
        }

        return (null);
    }

    // FIND NEXT NODE
    //
    public Node findNextNode(int startX, int startY, int startZ, int _endX, int _endY, int _endZ)
    {
        if (findFirstPath(startX, startY, startZ, _endX, _endY, _endZ))
        {
            return (new Node(firstResult[findNextOptimalNode(0)]));
        }

        return (null);
    }

    // FIND FIRST PATH
    //
    private bool findFirstPath(int startX, int startY, int startZ, int _endX, int _endY, int _endZ)
    {

        if (startX != _endX || startY != _endY || startZ != _endZ)
        {
            firstResult = null;

            resetMoveControlMatrix();

            endX = _endX;
            endY = _endY;
            endZ = _endZ;

            return (walkTo(startX, startY, startZ, 0));
        }

        return (false);
    }

    // RESET MOVE CONTROL MATRIX
    //
    private void resetMoveControlMatrix()
    {
        for (int y = 0; y < mapY; y++)
        {
            for (int x = 0; x < mapX; x++)
            {
                for (int z = 0; z < mapZ; z++)
                {
                    moveControlMatrix[x,z,y] = (byte)map[x,z,y];
                }
            }
        }
    }

    // WALK TO
    //
    private bool walkTo(int x, int y, int z, int depth)
    {
        // Check basic return conditions:

        if (x == endX && y == endY && z == endZ)
        {
            (firstResult = new Node[firstResultDepth = depth + 1])[depth] = new Node(x, y,z);

            return (true);
        }
        else if (depth > maxDepth)
        {
            return (false);
        }

        // Mark this tile as blocked:

        moveControlMatrix[x,z,y] = 1;

        // These are the local variables declared for this function. The purpose
        // is keeping the stack requirement low (and the code still fast).

        int delta0 = 0;
        int delta1 = 0;
        int delta2 = 0;
        int delta3 = 0;
        int delta4 = 0;
        int delta5 = 0;

        // Calculate the advantage of walking on each possible side:

        dy = endY - y;
        dx = endX - x;
        dz = endZ - z;

        if (x > 0 && moveControlMatrix[x - 1,z,y] == 0)
        {
            delta0 = endX - x + 1;
            delta0 = delta0 * delta0 + dz * dz + dy * dy;
        }
        else
            delta0 = MAX_DISTANCE;

        if (x < mapX - 1 && moveControlMatrix[x + 1,z,y] == 0)
        {
            delta1 = endX - x - 1;
            delta1 = delta1 * delta1 + dz * dz + dy * dy;
        }
        else
            delta1 = MAX_DISTANCE;

        if (z > 0 && moveControlMatrix[x, z - 1, y] == 0)
        {
            delta4 = endZ - z + 1;
            delta4 = dx * dx + delta4 * delta4 + dy * dy;
        }
        else
            delta4 = MAX_DISTANCE;

        if (z < mapZ - 1 && moveControlMatrix[x, z + 1, y] == 0)
        {
            delta5 = endZ - z - 1;
            delta5 = dx * dx + delta5 * delta5 + dy * dy;
        }
        else
            delta5 = MAX_DISTANCE;

        if (y > 0 && moveControlMatrix[x,z,y - 1] == 0)
        {
            delta2 = endY - y + 1;
            delta2 = dx * dx + dz * dz + delta2 * delta2;
        }
        else
            delta2 = MAX_DISTANCE;

        if (y < mapY - 1 && moveControlMatrix[x,z,y + 1] == 0)
        {
            delta3 = endY - y - 1;
            delta3 = dx * dx + dz * dz + delta3 * delta3;
        }
        else
            delta3 = MAX_DISTANCE;

        

        // Try to walk to the nearby tiles if possible following an optimal order:

       
        // Find the optimal side to walk through:

        bestIndex = -1;                      // Index of the next optimal side
        bestDelta = MAX_DISTANCE - 1;        // Optimal delta (lowest)

        if (delta0 < bestDelta) { bestIndex = 0; bestDelta = delta0; }
        if (delta1 < bestDelta) { bestIndex = 1; bestDelta = delta1; }
        if (delta2 < bestDelta) { bestIndex = 2; bestDelta = delta2; }
        if (delta3 < bestDelta) { bestIndex = 3; bestDelta = delta3; }
        if (delta4 < bestDelta) { bestIndex = 4; bestDelta = delta4; }
        if (delta5 < bestDelta) { bestIndex = 5; bestDelta = delta5; }

        // If all the sides are blocked or have been visited then go back:

        if (bestIndex < 0)
        {
            return (false);
        }

        // Go to the appropriate side:
        bool checkNext = false;
        switch (bestIndex)
        {
            case 0:
                {
                    checkNext = moveControlMatrix[x - 1, z, y] == 0 && walkTo(x - 1, y, z, depth + 1);
                    delta0 = MAX_DISTANCE;

                    break;
                }

            case 1:
                {
                    checkNext = moveControlMatrix[x + 1, z, y] == 0 && walkTo(x + 1, y, z, depth + 1);
                    delta1 = MAX_DISTANCE;

                    break;
                }

            case 2:
                {
                    checkNext = moveControlMatrix[x, z, y - 1] == 0 && walkTo(x, y - 1, z, depth + 1);
                    delta2 = MAX_DISTANCE;

                    break;
                }

            case 3:
                {
                    checkNext = moveControlMatrix[x, z, y + 1] == 0 && walkTo(x, y + 1, z, depth + 1);
                    delta3 = MAX_DISTANCE;

                    break;
                }
            case 4:
                {
                    checkNext = moveControlMatrix[x, z - 1, y] == 0 && walkTo(x, y, z - 1, depth + 1);
                    delta4 = MAX_DISTANCE;

                    break;
                }
            case 5:
                {
                    checkNext = moveControlMatrix[x, z + 1, y] == 0 && walkTo(x, y, z + 1, depth + 1);
                    delta5 = MAX_DISTANCE;

                    break;
                }
        }
        if(checkNext) firstResult[depth] = new Node(x, y, z);
        return checkNext;
        
    }

    // OPTIMIZE RESULT
    //
    private List<Node> getOptimizedResult()
    {
        List<Node> optimizedResult = new List<Node>(firstResultDepth);

        for (int index = 0; index < firstResultDepth - 1;)
        {
            optimizedResult.Add(new Node(firstResult[index = findNextOptimalNode(index)]));
        }

        return (optimizedResult);
    }

    // FIND NEXT OPTIMAL NODE
    //
    private int findNextOptimalNode(int startIndex)
    {
        int x = firstResult[startIndex].x;
        int y = firstResult[startIndex].y;
        int z = firstResult[startIndex].z;

        int bestNextIndex = startIndex + 1;

        for (int index = bestNextIndex + 1; index < firstResultDepth; index++)
        {
            Node node = firstResult[index];

            int deltaX = x - node.x;

            if (deltaX >= -1 && deltaX <= +1)
            {
                int deltaY = y - node.y;

                if (deltaY >= -1 && deltaY <= +1)
                {
                    int deltaZ = z - node.z;

                    if (deltaZ >= -1 && deltaZ <= +1)
                    {
                        bestNextIndex = index;
                    }
                }
            }
        }

        return (bestNextIndex);
    }

    public void SetNodeValue(Node node, int value)
    {
        if (node != null) map[node.x, node.z, node.y] = value;
    }
}

[System.Serializable]
public class Node
{
    public int x;
    public int y;
    public int z;

    public Node(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Node(Node node)
    {
        x = node.x;
        y = node.y;
        z = node.z;
    }
}
