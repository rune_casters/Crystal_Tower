﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Handles the Slasher AI
/// </summary>
public class Slasher : Enemy
{
    /// <summary>
    /// The slasher projectile prefab
    /// </summary>
    [SerializeField] private GameObject slash;

    /// <summary>
    /// Slasher states
    /// </summary>
    private enum States
    {
        CHASE,
        ATTACK,
        REPOS
    };
    private States state;
    /// <summary>
    /// Its true while repositioning
    /// </summary>
    private bool repositioning;
    /// <summary>
    /// Lets attack the slasher when its true
    /// </summary>
    private bool canAttack;
    /// <summary>
    /// If the projectile needs to go horizontal, its vertical by defualt
    /// </summary>
    private bool horizontal;
    /// <summary>
    /// The shoot point of the slasher
    /// </summary>
    [SerializeField] private Transform shootPoint;

    /// <summary>
    /// This function is called when the slasher is on attack state, and determinates if the attack is horizontal or vertical
    /// </summary>
    protected override void Attack()
    {
        // Stop the target and look to the target
        agent.speed = 0;
        transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
        anim.SetBool("Walking", false);
        // If its able to attack
        if (canAttack)
        {
            canAttack = false;
            // Has a 50% chance to attack horizontally or vertically
            if (Random.Range(.0f, 1.0f) <= 0.5f)
            {
                anim.SetTrigger("AttackHor");
                horizontal = true;
            }
            else
            {
                anim.SetTrigger("AttackVer");
                horizontal = false;
            }
        }
        
    }
    /// <summary>
    /// This function is called when the slasher is on Repos state
    /// </summary>
    private void Repos()
    {
        // if isnt repositioning 
        if (!repositioning)
        {
            // Get a random point in a donut to go
            repositioning = true;
            agent.speed = currentSpeed;
            Vector3 randomDirection = -GetRandomInDonut(3, 4, 360);
            randomDirection += transform.position;
            // Get the nearest point to that position on the navmesh
            NavMeshHit hit;
            NavMesh.SamplePosition(randomDirection, out hit, 4, 1);
            Vector3 finalPosition = hit.position;
            // Go to the new destination
            agent.SetDestination(finalPosition);
        }
        // if im close enough and im repositioning stop
        else if (agent.remainingDistance < 1)
        {
            repositioning = false;
            // if i can attack the target go to attack state, go to chase state else
            if (CanAttackTarget()) state = States.ATTACK;
            else state = States.CHASE;
        }
    }

    /// <summary>
    /// This function is called from the slasher attack animation, and spawns the projectile
    /// </summary>
    public void Slash()
    {
        // Spawn the projectile and make it look the target
        GameObject clone = Instantiate(slash, shootPoint.position, shootPoint.rotation);
        clone.transform.LookAt(target);
        // Set the damahe and rotate it if needed
        clone.GetComponent<EnemyProjectile>().Damage = currentDamage;
        clone.transform.Rotate(new Vector3(0, 0, horizontal ? 0 : 90), Space.Self);
        // Add force to the projectile
        clone.GetComponent<Rigidbody>().AddForce(clone.transform.forward *1300);
    }
    /// <summary>
    /// This function in called when the slashes is on Chase state
    /// </summary>
    protected override void FollowTarget()
    {
        // Go to the target until the salsher can attack the target, then go to attack state
        agent.speed = currentSpeed;
        agent.SetDestination(target.position);
        if (CanAttackTarget()) state = States.ATTACK;
    }

    /// <summary>
    /// Initialize the vairables
    /// </summary>
    protected new void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
        // set a delay for the first attack
        Invoke("AttackEnded", stats.AttackCooldown);
    }
    /// <summary>
    /// Handles the states
    /// </summary>
    protected new void Update()
    {
        base.Update();
        if (!stuned)
        {
            switch (state)
            {
                case States.CHASE:
                    FollowTarget();
                    break;
                case States.ATTACK:
                    Attack();
                    break;
                case States.REPOS:
                    Repos();
                    break;
            }
        }

    }
    /// <summary>
    /// This function is called when the attack animation ends
    /// </summary>
    private void AttackEnded()
    {
        // Start the cooldown countdown
        Invoke("AttackCoolDown", stats.AttackCooldown);
        // Reposition
        anim.SetBool("Walking", true);
        state = States.REPOS;
    }
    /// <summary>
    /// Lets the slasher attack again
    /// </summary>
    private void AttackCoolDown()
    {
        canAttack = true;
    }

    /// <summary>
    /// Sets the new speed value to the agent and the current speed
    /// </summary>
    /// <param name="value"> The new speed value</param>
    public override void SetSpeed(float value)
    {
        agent.speed = currentSpeed = value;
    }
    /// <summary>
    /// Calls to the enemy Vulnerable function
    /// </summary>
    /// <param name="_receivedDamageMul"> the value to add to the damage multiplicator</param>
    public override void Vulnerable(float _receivedDamageMul)
    {
        base.Vulnerable(_receivedDamageMul);
    }
    /// <summary>
    /// Calls Enemy Hit function
    /// </summary>
    /// <param name="damage">The damage dealed to the slasher</param>
    /// <param name="hitPosition"> The position where the slasher has been hit</param>
    public override void Hit(float damage, Vector3 hitPosition)
    {
        base.Hit(damage, hitPosition);
    }
    /// <summary>
    /// Stuns the slasher if the state is true and sets the agent speed to the current speed else
    /// </summary>
    /// <param name="state"> True if the target needs to be stuned false else</param>
    public override void Stun(bool state)
    {
        stuned = state;
        if (stuned)
        {
            agent.speed = 0;
        }
        else
        {
            agent.speed = currentSpeed;
        }

    }
    /// <summary>
    /// Calls Enemy Die and spawns the slasher Ragdoll
    /// </summary>
    protected override void Die()
    {
        base.Die();
        // spawn the ragdoll and destroy the gameObject
        Instantiate(stats.corpse, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
