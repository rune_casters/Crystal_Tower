﻿using UnityEngine;

/// <summary>
/// Spcript that deals damage to the player when triggers with it
/// </summary>
public class EnemyProjectile : MonoBehaviour
{

    /// <summary>
    /// The damage of the enemy 
    /// </summary>
    public float Damage { get; set; }
    /// <summary>
    /// The collider of the projectile or explosion
    /// </summary>
    private Collider col;
    /// <summary>
    /// Gets the collider of the GameObject and sets a destroy on 3 seconds
    /// </summary>
    private void Start()
    {
        Destroy(gameObject, 3);
        col = GetComponent<Collider>();
    }
    /// <summary>
    /// Handles the collision of the GameObject
    /// </summary>
    /// <param name="other"> The collided object</param>
    protected void OnTriggerEnter(Collider other)
    {
        // If the collided object is the player deal damage to him
        if (other.CompareTag("Player"))
        {
            other.GetComponent<Mage>().Hit(Damage);
        }
        // After the first trigger disable the collider and destroy the GameObject
        col.enabled = false;
        Destroy(gameObject, 0.6f);
    }
}
