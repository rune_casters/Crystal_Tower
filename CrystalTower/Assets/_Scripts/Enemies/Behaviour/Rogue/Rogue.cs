﻿using UnityEngine;
/// <summary>
/// Handles the Rogue AI
/// </summary>
public class Rogue : Enemy
{
    /// <summary>
    /// Damage multiplicator for the first invisible attack
    /// </summary>
    [SerializeField] private float invisibleHitMult;
    /// <summary>
    /// The Rogue material for when is visible
    /// </summary>
    [SerializeField] private Material defaultRogueMat;
    /// <summary>
    /// The dagger material for when is visible
    /// </summary>
    [SerializeField] private Material defaultDaggerMat;
    /// <summary>
    /// The mesh of the dagger
    /// </summary>
    [SerializeField] private SkinnedMeshRenderer daggerMesh;
    /// <summary>
    /// The porbability to stop while following the target
    /// </summary>
    [SerializeField] [Range(0.0f,1.0f)] private float dumbProb;

    /// <summary>
    /// The states of the rogue
    /// </summary>
    private enum States
    {
        CHASE,
        ATTACK
    };
    private States state;
    /// <summary>
    /// Starts on true and after getting hit or attacking goes to false
    /// </summary>
    private bool invisible;
    /// <summary>
    /// After starting the attack goes to false until it can attack again
    /// </summary>
    private bool canAttack;
    /// <summary>
    /// If he has stopped while following the target
    /// </summary>
    private bool dumb;

    /// <summary>
    /// This function is called while the Rogue is on Attack state
    /// </summary>
    protected override void Attack()
    {
        // Stop the rogue
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
        // If isnt attacking start the attack animation and start the attack CD
        if (canAttack)
        {
            canAttack = false;
            anim.SetTrigger("Attack");
            anim.SetBool("Chasing", false);
            Invoke("AttackEnded", stats.AttackCooldown);
        }
    }
    /// <summary>
    /// This functions is called while the Rogue is on Chase state, and handles if stops while following the target
    /// </summary>
    protected override void FollowTarget()
    {
        // if isnt stopped
        if(!dumb)
        {
            // has a 20% chance per second to stop if isnt invisible
            if (invisible || Random.Range(0f, 1f) > 0.2f * Time.deltaTime)
            {
                agent.isStopped = false;
                anim.SetBool("Chasing", true);
                agent.SetDestination(target.position);
            }
            else
            {
                dumb = true;
                agent.isStopped = true;
                anim.SetTrigger("Dumb");
            }
        }
    }

    /// <summary>
    /// Initialize the variables
    /// </summary>
    protected new void Start()
    {
        base.Start();
        state = States.CHASE;
        invisible = true;
        anim = GetComponent<Animator>();
        anim.SetBool("Stealth", true);
        emissive = defaultRogueMat.GetTexture("_EmissionMap");
        emissiveColor = defaultRogueMat.GetColor("_EmissionColor");
    }
    /// <summary>
    /// Handles the state change and states
    /// </summary>
    protected new void Update()
    {
        base.Update();
        if (!stuned)
        {
            if (CanAttackTarget()) state = States.ATTACK;
            else state = States.CHASE;

            switch (state)
            {
                case States.CHASE:
                    FollowTarget();
                    break;
                case States.ATTACK:
                    Attack();
                    break;
            }
        }

    }
    /// <summary>
    /// Chages rogue and dagger materials 
    /// </summary>
    private void Visible()
    {
        // Set the bool to false
        invisible = false;
        // Change the animation
        anim.SetBool("Stealth", false);
        // Change the materials
        enemyMesh.material = defaultRogueMat;
        daggerMesh.material = defaultDaggerMat;
    }
    /// <summary>
    /// This function is called from Attack after AttackCooldown seconds have passed
    /// </summary>
    private void AttackEnded()
    {
        canAttack = true;
    }
    /// <summary>
    /// This function is called when the dumb animation ends
    /// </summary>
    private void DumbEnd()
    {
        dumb = false;
    }

    /// <summary>
    /// Changes the rogue speed and if its invisible sets it visible
    /// </summary>
    /// <param name="value">The new speed value</param>
    public override void SetSpeed(float value)
    {
        if (invisible) Visible();
        agent.speed = currentSpeed = value;
    }
    /// <summary>
    /// Calls  Enemy Vulnerable function and if its invisible sets it visible
    /// </summary>
    /// <param name="_receivedDamageMul">The damage multiplicator</param>
    public override void Vulnerable(float _receivedDamageMul)
    {
        if (invisible) Visible();
        base.Vulnerable(_receivedDamageMul);
    }

    /// <summary>
    /// Calls Enemy Hit function and if its invisible sets it visible
    /// </summary>
    /// <param name="damage"> The damage dealed to the rogue</param>
    /// <param name="hitPosition"> The position where the rogue has been hit</param>
    public override void Hit(float damage, Vector3 hitPosition)
    {
        base.Hit(damage, hitPosition);
        if (invisible) Visible();
    }

    /// <summary>
    /// Stuns the rogue if the state is true and sets the agent speed to the current speed else
    /// </summary>
    /// <param name="state"> True if the target needs to be stuned false else</param>
    public override void Stun(bool state)
    {
        stuned = state;
        if (stuned)
        {
            agent.speed = 0;
        }
        else
        {
            agent.speed = currentSpeed;
        }

    }
    /// <summary>
    /// Calls Enemy Die and spawns the rogue Ragdoll
    /// </summary>
    protected override void Die()
    {
        base.Die();
        // spawn the ragdoll
        Instantiate(stats.corpse, transform.position, transform.rotation);
        // Destroy the gameObject
        Destroy(gameObject);
    }
    /// <summary>
    /// Thos function is called from the attack animations
    /// </summary>
    public void DealDamage()
    {
        // If the distance between the rogue and the player is less than rogues attack Range
        if (Vector3.Distance(transform.position, target.position) < stats.AttackRange * 1.3)
        {
            // if its invisible deal more damage with the multiplicator and go visible, deal normal damage else
            if (invisible)
            {
                mage.mageStats.Hit(stats.Damage * invisibleHitMult);
                Visible();
            }
            else
            {
                mage.mageStats.Hit(currentDamage);
            }
        }

    }

}
