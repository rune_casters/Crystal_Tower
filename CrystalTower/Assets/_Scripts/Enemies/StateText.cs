﻿using UnityEngine;

/// <summary>
/// Component used to display damage numbers on enemies
/// </summary>
public class StateText : MonoBehaviour
{
    /// <summary>
    /// Horizontal range (used for randomizer)
    /// </summary>
    [SerializeField] private float randomHorizontalRange = 0.008f;
    /// <summary>
    /// Text direction
    /// </summary>
    public Vector3 randomDirection;

    /// <summary>
    /// Sets initial text position (normally above enemy)
    /// </summary>
    public void Start()
    {
        //Each text takes a different direction (left or right) on a different up speed
        randomDirection = new Vector3(Random.Range(-randomHorizontalRange, randomHorizontalRange), 0.01f, 0);
    }

    /// <summary>
    /// Updates texts movement and scale using linear interpolation
    /// </summary>
    public void Update()
    {
        // Check if game is not paused
        if (Time.timeScale != 0)
        {
            // Check if scale hasnt reached a small value
            if (transform.localScale.x > 0.05f)
            {
                // Scale text from current to zero
                transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, Time.deltaTime);
                // Move it on that random direction
                transform.Translate(randomDirection);
                transform.Rotate(0, 180, 0);
            }
            // Otherwise destroy text
            else
            {
                Destroy(gameObject);
            }
        }
    }

}
