﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Handles the spawning of the enemies on the rooms
/// </summary>
public class SpawnManager : MonoBehaviour
{
    /// <summary>
    /// The root of all the EnemySpawn in the room.
    /// </summary>
    public Transform enemySpawnRoot;
    /// <summary>
    /// The data of the room.
    /// </summary>
    public RoomData data;
    /// <summary>
    /// EnemySpawn array with all the children of the enemySpawnRoot.
    /// </summary>
    private EnemySpawn[] enemySpawns;
    /// <summary>
    /// GameEvent that rises when an enemy dies.
    /// </summary>
    public GameEvent onEnemyDie;
    /// <summary>
    /// the listener and the event that executes when the event rises
    /// </summary>
    private GameEventListener listener;
    private UnityEvent response; 
    /// <summary>
    /// While its on true spawns enemies.
    /// </summary>
    private bool isSpawning;

    /// <summary>
    /// The number of enemies alive on the scene
    /// </summary>
    private int currentAliveEnemies;
    /// <summary>
    /// The maximum allowed alive enemies on the scene
    /// </summary>
    public int maxAliveEnemies;

    /// <summary>
    /// Timer to spawn enemies
    /// </summary>
    private float currentTimeEnemies = 0;
    /// <summary>
    /// The time needed to spawn a new enemy
    /// </summary>
    private float timeForNextEnemy = 0;
    /// <summary>
    /// The number of enemies that spawned in the same wave
    /// </summary>
    private int currentEnemiesWave = 0;

    /// <summary>
    /// The time needed to start a new wave of spawns
    /// </summary>
    public float timeBetweenWeaves;
    /// <summary>
    /// The time needed to spawn a new enemy on the same wave
    /// </summary>
    public float timeBetweenEnemies;
    /// <summary>
    /// The number of enemies per wave
    /// </summary>
    public int enemiesInWave;
    /// <summary>
    /// Stablishes the total number of enemies that can spawn by their difficulty value
    /// </summary>
    public int roomDificulty;


    /// <summary>
    /// Gets all the children of the enemySpawnRoot and sets them in the enemySpawns array
    /// </summary>
    private void Awake()
    {
        enemySpawns = new EnemySpawn[enemySpawnRoot.transform.childCount];
        for (int i = 0; i < enemySpawnRoot.transform.childCount; i++)
        {
            enemySpawns[i] = enemySpawnRoot.transform.GetChild(i).GetComponent<EnemySpawn>();
            enemySpawns[i].spawnManagerRef = this;
        }
    }

    /// <summary>
    /// Creates the listener to the GameEvent and starts spawning
    /// </summary>
    public void StartSpawning()
    {
        // set the isSpawning bool on true so the Update can start working
        isSpawning = true;
        // Set a wave delat to the spawn
        timeForNextEnemy = timeBetweenWeaves;
        // Set to 0 the enemies alive
        currentAliveEnemies = 0;

        // Listener Setup
        response = new UnityEvent();
        response.AddListener(RemoveEnemy);

        listener = gameObject.AddComponent<GameEventListener>();
        listener.gameEvent = onEnemyDie;
        listener.response = response;
        listener.gameEvent.RegisterListener(listener);
        
    }

    private void Update()
    {
        // if can spawn
        if (isSpawning)
        {
            // if there are less enmies than allowed and the time to spawn the next enemy is greater than needed and the roomDificulty is greater than 0
            currentTimeEnemies += Time.deltaTime;
            if (currentAliveEnemies < maxAliveEnemies && currentTimeEnemies >= timeForNextEnemy && roomDificulty > 0)
            {
                // reset the time to spawn the next enemy
                currentTimeEnemies = 0;
                // set the new time needed to spawn enemies to the value of the needed time to spawn in a wave
                timeForNextEnemy = timeBetweenEnemies;
                // if the number of enemies spawned is greater or equal to the enemies in a wave end the wave 
                if (++currentEnemiesWave >= enemiesInWave)
                {
                    // reset the enemies in wave
                    currentEnemiesWave = 0;
                    // set the needed time to spawn enemies to the time needed to start the next spawn wave
                    timeForNextEnemy = timeBetweenWeaves;
                }
                // Call a random EnemySpawn on the Scene to spawn an enemy
                enemySpawns[Random.Range(0,enemySpawns.Length)].Spawn();
            }
        }
    }

    /// <summary>
    /// Function that executes when an enemy dies
    /// </summary>
    public void RemoveEnemy()
    {
        // Decrease the number of enmies alive
        --currentAliveEnemies;
        // If the room dificulty and the current enemies alive are less or equal to 0 the room is clear
        if (roomDificulty <= 0 && currentAliveEnemies <= 0)
        {
            // Stop spawning
            isSpawning = false;
            // Destroy the listener
            Destroy(listener);
            // Stablish that the room has been cleared
            GetComponent<Room>().RoomCleared();
        }
    }

    /// <summary>
    /// Enemy spawn calls this functios each time it spawns an enemy
    /// </summary>
    /// <param name="value"> The difficulty value of the enemy.</param>
    public void AddEnemy(int value)
    {
        // Increase the number of enemies alive
        ++currentAliveEnemies;
        // Subtract the enemy dificulty value to the room dificulty value
        roomDificulty -= value;
    }


    /// <summary>
    /// Resturns the walkable area saved on the room data
    /// </summary>
    /// <returns>Int[,,] with the walkable area</returns>
    public int[,,] GetWalkableArea()
    {
        int[,,] result = new int[data.walkableArea.Length, data.walkableArea[0].walkableLine.Length, data.walkableArea[0].walkableLine[0].walkableColumn.Length];
        for (int i = 0; i < data.walkableArea.Length; i++)
        {
            for (int j = 0; j < data.walkableArea[i].walkableLine.Length; j++)
            {
                for (int k = 0; k < data.walkableArea[i].walkableLine[j].walkableColumn.Length; k++)
                {
                    if (data.walkableArea[i].walkableLine[j].walkableColumn[k].value)
                    {
                        result[i, j, k] = 0;
                    }
                    else
                    {
                        result[i, j, k] = 1;
                    }

                }
            }
        }
        return result;
    }
}
