﻿using UnityEngine;

/// <summary>
/// Handles loot creation
/// </summary>
public class LootSpawn : MonoBehaviour
{
    /// <summary>
    /// Creates loot based on the given loot table and optional position offset
    /// </summary>
    public void SpawnLoot(LootTable lootTable, Vector3 offset = default)
    {
        // Cache loot datas
        LootTable.LootData[] lootDatas = lootTable.lootDatas;

        // Calculate initial data
        int randomPercentage = Random.Range(0, lootTable.totalPercentage + 1);
        int calculatedPercentage = 0;
        int lootDataLength = lootDatas.Length;
        int selectedIndex = 0;
        
        // Loop through loot data and get random loot index
        for (int i = 0; i < lootDataLength; i++)
        {
            calculatedPercentage += lootDatas[i].percentage;
            if (randomPercentage <= calculatedPercentage)
            {
                selectedIndex = i;
                break;
            }
        }

        // Select calculated random index which correspond to an item
        LootTable.LootData selectedLoot = lootDatas[selectedIndex];
        GameObject lootGameobject = null;
        // Depending on that item type 
        switch (selectedLoot.type)
        {
            // Sometimes you get nothing :(
            case LootTable.LootType.Nothing:
                break;
            // Create a skull currency with a random value
            case LootTable.LootType.Skull:
                lootGameobject = Instantiate(selectedLoot.prefab, transform.position, Quaternion.identity);
                lootGameobject.GetComponent<Collectible>().Quantity = Random.Range(selectedLoot.minValue, selectedLoot.maxValue + 1);
                break;
            // Create a health orb
            case LootTable.LootType.HealthOrb:
                lootGameobject = Instantiate(selectedLoot.prefab, transform.position, Quaternion.identity);
                lootGameobject.GetComponent<Collectible>().Quantity = selectedLoot.minValue;
                break;
            // Create a random trinket
            case LootTable.LootType.Trinket:
                lootGameobject = Instantiate(selectedLoot.prefab, transform.position, Quaternion.identity);
                lootGameobject.GetComponent<Trinket>().trinketData = lootTable.trinketDatas[Random.Range(0, lootTable.trinketDatas.Length)];
                break;
            // Create a random spell
            case LootTable.LootType.Spell:
                lootGameobject = Instantiate(selectedLoot.prefab, transform.position, Quaternion.identity);
                lootGameobject.GetComponent<SpellDrop>().spellData = lootTable.spellDatas[Random.Range(0, lootTable.spellDatas.Length)];
                break;
            // Create a random spell upgrade
            case LootTable.LootType.SpellUpgrade:
                // Look if the player can upgrade any of his spells
                SpellData spellUpgradeCheck = GetRandomSpellUpgrade(lootTable.spellSystem);
                if (spellUpgradeCheck)
                {
                    // Create spell upgrade
                    lootGameobject = Instantiate(selectedLoot.prefab, transform.position, Quaternion.identity);
                    SpellUpgrade spellUpgrade = lootGameobject.GetComponent<SpellUpgrade>();
                    spellUpgrade.spellSlot = spellUpgradeCheck.slot;
                    spellUpgrade.upgradeSpell = spellUpgradeCheck;
                }
                break;
            default:
                break;
        }

        if (lootGameobject)
            lootGameobject.transform.position += offset;

    }

    /// <summary>
    /// Returns a possible spell upgrade
    /// </summary>
    private SpellData GetRandomSpellUpgrade(SpellSystem spellSystem)
    {
        int iterations = 0;
        SpellData upgradedSpell = null;
        SpellData checkSpell = null;
        // Check if we either found a spell upgrade
        while (iterations++ < 10 || !upgradedSpell)
        {
            // Look for a random spell from the player
            checkSpell = spellSystem.spells[Random.Range(0, spellSystem.spells.Length)].spell;
            // Check if it's upgradable
            if (checkSpell.upgradeSpell)
                upgradedSpell = checkSpell.upgradeSpell;
        }
        // Return upgraded spell
        return upgradedSpell;
    }
}
