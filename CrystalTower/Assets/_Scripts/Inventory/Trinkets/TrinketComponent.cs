﻿using UnityEngine;

/// <summary>
/// Base Trinket component class that stores a reference to the player stats
/// </summary>
[System.Serializable]
public abstract class TrinketComponent : ScriptableObject
{
    /// <summary>
    /// Player stats reference
    /// </summary>
    public Stats mageStats;

    /// <summary>
    /// When trinket is picked up
    /// </summary>
    public abstract void Initialize();

    /// <summary>
    /// Whenever trinket is being used
    /// </summary>
    public abstract void Use();
}
