﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Stores trinket information such as name, sprite, cost and event
/// </summary>
[CreateAssetMenu(menuName = "Trinket/TrinketData")]
public class TrinketData : ScriptableObject
{
    /// <summary>
    /// Trinket name
    /// </summary>
    [Header("Trinket Data")]
    public new string           name;

    /// <summary>
    /// Trinket sprite
    /// </summary>
    public Sprite               sprite;

    /// <summary>
    /// Trinket description
    /// </summary>
    [TextArea] public string    description;
    
    /// <summary>
    /// Trinket rarity
    /// </summary>
    public RarityColors.RARITY  rarity;

    /// <summary>
    /// Trinket cost
    /// </summary>
    [Range(0, 1000)]public int  cost;

    /// <summary>
    /// Event to raise on equip
    /// </summary>
    [Header("On Equip")]
    public UnityEvent           onEquipEvent;

    /// <summary>
    /// Event to listen to that raises unity event
    /// </summary>
    [Header("On Event")]
    public GameEvent            gameEvent;
    /// <summary>
    /// Event to raise when game event is triggered
    /// </summary>
    public UnityEvent           unityEvent;

    /// <summary>
    /// If the trinket is destructible
    /// </summary>
    public bool                 destructible = true;

    /// <summary>
    /// Times of use before destruction (if destructible)
    /// </summary>
    public int                  usesBeforeDestruction;

    /// <summary>
    /// If the trinket has use cooldown
    /// </summary>
    public bool                 hasCooldown;

    /// <summary>
    /// Base trinket cooldown
    /// </summary>
    public float                baseCooldown;

    /// <summary>
    /// Event raised on unequip
    /// </summary>
    [Header("On UnEquip")]
    public UnityEvent           onUnequipEvent;

    /// <summary>
    /// Trinket component spells used on event
    /// </summary>
    public TrinketComponent[]   trinketComponents;
}

