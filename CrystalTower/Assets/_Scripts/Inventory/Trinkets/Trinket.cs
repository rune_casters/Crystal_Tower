﻿using UnityEngine;

/// <summary>
/// Stores Trinket data and event listener
/// </summary>
public class Trinket : MonoBehaviour
{
    /// <summary>
    /// Reference to the trinket data (name, sprite etc...)
    /// </summary>
    public TrinketData trinketData;

    /// <summary>
    /// Listener reference
    /// </summary>
    private TrinketEventListener gameEventListener;

    /// <summary>
    /// Trinket uses
    /// </summary>
    public int CurrentUses { get; set; }

    /// <summary>
    /// Cooldown left
    /// </summary>
    public float CurrentCooldown { get; set; }

    /// <summary>
    /// If the trinkets displays full information or not
    /// </summary>
    public bool DisplayInfo { get; set; }

    /// <summary>
    /// Equiping a trinket toggle it's possible events (stat change or spells)
    /// </summary>
    public void Equip()
    {
        // Check if the trinket has a recurring event such as when Player is hit
        if (trinketData.gameEvent != null)
        {
            // Add event to listener and register the listener
            gameEventListener = gameObject.AddComponent<TrinketEventListener>();
            gameEventListener.gameEvent = trinketData.gameEvent;
            gameEventListener.response = trinketData.unityEvent;
            gameEventListener.gameEvent.RegisterListener(gameEventListener);
        }

        CurrentUses = trinketData.usesBeforeDestruction;

        // Check if the trinket has an equip event and raise it
        if (trinketData.onEquipEvent != null)
            trinketData.onEquipEvent.Invoke();

        // Disable physics and model
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;

        // Initialize components (spells)
        for (int i = 0; i < trinketData.trinketComponents.Length; i++)
            trinketData.trinketComponents[i].Initialize();
    }

    // Use trinket components
    public void Use()
    {
        for (int i = 0; i < trinketData.trinketComponents.Length; i++)
            trinketData.trinketComponents[i].Use();
    }

    // Unregister trinket listeners
    public void Unequip()
    {
        trinketData.onUnequipEvent.Invoke();
        gameEventListener.gameEvent.UnregisterListener(gameEventListener);
    }

}
