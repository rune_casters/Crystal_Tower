﻿using UnityEngine;

/// <summary>
/// Trinket spawn component used to spawn given spell whenever trinket event is triggered
/// </summary>
[CreateAssetMenu(menuName = "Trinket/Components/TrinketSpawnComponent")]
public class TrinketSpawnComponent : TrinketComponent
{
    /// <summary>
    /// Spell reference
    /// </summary>
    public SpellData spell;

    /// <summary>
    /// Initializes spell shooting point transform
    /// </summary>
    public override void Initialize()
    {
        Transform[] transform = { mageStats.playerTransform };
        spell.Initialize(transform);
    }

    /// <summary>
    /// Casts spell when trinket component is on use
    /// </summary>
    public override void Use()
    {
        spell.Cast();
    }
}


