﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Trinket percentage component used to check whenever an event is triggered depending on a stat percentage
/// </summary>
[CreateAssetMenu(menuName = "Trinket/Components/TrinketPercentageComponent")]
public class TrinketPercentageComponent : TrinketComponent
{
    /// <summary>
    /// Trinket percentage component types
    /// </summary>
    public enum StatEnum
    {
        HEALTH,
        DAMAGE
    };

    /// <summary>
    /// Current type
    /// </summary>
    public StatEnum statEnum;

    /// <summary>
    /// Stat that will be used
    /// </summary>
    private Stats.Stat stat;

    /// <summary>
    /// Percentage (0-100%)
    /// </summary>
    [Range(0, 1)]
    public float percentage;
    /// <summary>
    /// If the event has to trigger above that percentage or below
    /// </summary>
    public bool superiorTo;
    private bool inState;
    private bool started;

    /// <summary>
    /// Event to trigger when correct percentage is reached
    /// </summary>
    public UnityEvent onCondition;

    /// <summary>
    /// Event to trigger when incorrect percentage is reached
    /// </summary>
    public UnityEvent onNotCondition;

    /// <summary>
    /// Cache trinket compared stat
    /// </summary>
    public override void Initialize()
    {
        switch (statEnum)
        {
            case StatEnum.HEALTH:
                stat = mageStats.health;
                break;
            case StatEnum.DAMAGE:
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Whenever trinket is being used check percentage and trigger the event
    /// </summary>
    public override void Use()
    {
        // If the trinket has to get triggered when the value is superior to the percentage
        if (superiorTo)
        {
            if (stat.CurrentValue / stat.maxValue >= percentage)
            {
                if (!started)
                    started = true;

                if (!inState)
                {
                    // Raise trinket event
                    inState = true;
                    onCondition.Invoke();
                }
            }
            else
            {
                if (started)
                {
                    if (inState)
                    {
                        inState = false;
                        onNotCondition.Invoke();
                    }
                }
            }
        }
        else
        {
            if (stat.CurrentValue / stat.maxValue <= percentage)
            {
                if (!started)
                    started = true;

                if (!inState)
                {
                    // Raise trinket event
                    inState = true;
                    onCondition.Invoke();
                }
            }
            else
            {
                if (started)
                {
                    if (inState)
                    {
                        inState = false;
                        onNotCondition.Invoke();
                    }
                }
            }
        }


    }
}
