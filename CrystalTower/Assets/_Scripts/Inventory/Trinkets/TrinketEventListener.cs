﻿using UnityEngine;

/// <summary>
/// Custom event listener made for trinkets
/// </summary>
public class TrinketEventListener : GameEventListener
{
    /// <summary>
    /// Trinket reference
    /// </summary>
    private Trinket trinket;
    
    public override void OnEnable()
    {
        // Cache trinket
        base.OnEnable();
        trinket = GetComponent<Trinket>();
    }

    /// <summary>
    /// Trinket cooldown check
    /// </summary>
    public void Update()
    {
        // Update cooldown if it has any
        if (trinket.trinketData.hasCooldown && trinket.CurrentCooldown > 0)
            trinket.CurrentCooldown -= Time.deltaTime;
    }

    /// <summary>
    /// Whenever a trinket event is being raised
    /// </summary>
    public override void OnEventRaiser()
    {
        // If the trinket has no cooldown or the cooldown is over
        if (!trinket.trinketData.hasCooldown || trinket.trinketData.hasCooldown && trinket.CurrentCooldown <= 0)
        {
            // Use trinket and raise event
            trinket.Use();
            response.Invoke();

            // Reset cooldown
            if (trinket.trinketData.hasCooldown)
                trinket.CurrentCooldown = trinket.trinketData.baseCooldown;

            // Check if trinket can be destroyed
            if (trinket.trinketData.destructible)
            {
                // Remove uses and destroy if it reaches zero
                trinket.CurrentUses--;
                if (trinket.CurrentUses == 0)
                {
                    trinket.Unequip();
                    Destroy(gameObject);
                }
            }
        }


        
    }
}
