﻿using UnityEngine;

/// <summary>
/// Handles inventory management and looting
/// </summary>
public class Inventory : MonoBehaviour
{
    /// <summary>
    /// Looting camera used for the raycast
    /// </summary>
    [Header("Raycast")]
    public Camera raycastCamera;

    /// <summary>
    /// Physics layer used for looting
    /// </summary>
    public LayerMask lootLayerMask;

    /// <summary>
    /// Caching hit event
    /// </summary>
    private RaycastHit hit;

    // Selected items
    private Trinket selectedTrinket;
    private SpellDrop selectedSpell;
    private SpellUpgrade selectedUpgrade;
    /// <summary>
    /// Effect used on selection
    /// </summary>
    public GameObject selectorEffect;

    /// <summary>
    /// Small display used to show the looting key
    /// </summary>
    [Header("UI References")]
    public InputDisplay inputDisplay;
    public KeyCode keyCode;
    public string lootTrinketMessage;
    public string buyUpgradeMessage;
    /// <summary>
    /// Complete display used to show item info
    /// </summary>
    public ItemDisplay itemDisplay;
    
    [Header("Inventory Data")]
    [SerializeField] private InventoryData inventoryData;
    [SerializeField] private SpellSystem spellSystem;
    [SerializeField] private Currencies currencies;

    /// <summary>
    /// Caching hit object
    /// </summary>
    private Transform objectHit;

    /// <summary>
    /// Looting raycast checker
    /// </summary>
    public void Update()
    {
        // Item has been hit
        if (Physics.Raycast(raycastCamera.transform.position, raycastCamera.transform.forward, out hit, 2, lootLayerMask))
        {
            // If a different object is hit clear the display
            if (objectHit && objectHit != hit.transform)
                Clear();

            // Cache hit object transform and active selection effect
            objectHit = hit.transform;
            selectorEffect.SetActive(true);
            selectorEffect.transform.position = objectHit.position;

            // Depending on the tag display different info
            switch (objectHit.tag)
            {
                case "Trinket":
                    // Get trinket and update UI
                    selectedTrinket = objectHit.GetComponent<Trinket>();
                    if (selectedTrinket.DisplayInfo)
                    {
                        itemDisplay.UpdateDisplay(selectedTrinket.trinketData);
                        itemDisplay.gameObject.SetActive(true);
                    }
                    else
                    {
                        inputDisplay.SetData(keyCode, lootTrinketMessage);
                        inputDisplay.gameObject.SetActive(true);
                    }
                    break;
                case "SpellDrop":
                    // Get spell and update UI
                    selectedSpell = objectHit.GetComponent<SpellDrop>();
                    itemDisplay.UpdateDisplay(selectedSpell.spellData);
                    itemDisplay.gameObject.SetActive(true);
                    break;
                case "SpellUpgrade":
                    // Get spell upgrade and update UI
                    selectedUpgrade = objectHit.GetComponent<SpellUpgrade>();
                    inputDisplay.SetData(keyCode, buyUpgradeMessage + "<color=red>"+ selectedUpgrade.spellSlot + "</color>");
                    inputDisplay.gameObject.SetActive(true);
                    break;
                default:
                    break;
            }

            // Display info depending on the selected item
            if (selectedTrinket)
                HoverTrinket();
            else if (selectedSpell)
                HoverSpellDrop();
            else if (selectedUpgrade)
                HoverSpellUpgrade();
        }
        else 
            // Clear display
            Clear();
    }

    /// <summary>
    /// Clears display
    /// </summary>
    private void Clear()
    {
        // Disable displays and effect
        selectorEffect.SetActive(false);
        itemDisplay.gameObject.SetActive(false);
        inputDisplay.gameObject.SetActive(false);
        selectedTrinket = null;
        selectedSpell = null;
        selectedUpgrade = null;
    }

    /// <summary>
    /// When user hovers trinkets
    /// </summary>
    private void HoverTrinket()
    {
        selectedUpgrade = null;
        selectedSpell = null;

        // Update display position
        if (selectedTrinket.DisplayInfo)
            itemDisplay.UpdatePosition(objectHit.position);
        else
            inputDisplay.UpdatePosition(objectHit.position);

        // When user picks up item
        if (Input.GetButtonDown("Interact"))
        {
            // If the item is being sold
            if (selectedTrinket.DisplayInfo)
            {
                // Check user money
                if (!CheckCost(selectedTrinket.trinketData.cost, selectedTrinket.transform.parent.gameObject))
                    return;
            }

            // Equip trinket and clear
            selectedTrinket.Equip();
            inventoryData.AddTrinket(selectedTrinket);
            selectorEffect.SetActive(false);
            Clear();
        }
    }

    /// <summary>
    /// When user hovers spell
    /// </summary>
    private void HoverSpellDrop()
    {
        selectedTrinket = null;
        selectedUpgrade = null;

        // Update display position
        itemDisplay.UpdatePosition(objectHit.position);

        // When user picks up item
        if (Input.GetButtonDown("Interact"))
        {
            // If the item is being sold
            if (selectedSpell.sold)
            {
                // Check user money
                if (!CheckCost(selectedSpell.spellData.cost, selectedSpell.transform.parent.gameObject))
                    return;
            }

            // Equip spell and clear
            spellSystem.ChangeSpell(selectedSpell.spellData);
            selectorEffect.SetActive(false);
            Destroy(selectedSpell.gameObject);
            Clear();
        }
    }

    /// <summary>
    /// When user hovers spell upgrade
    /// </summary>
    private void HoverSpellUpgrade()
    {
        selectedTrinket = null;
        selectedSpell = null;

        inputDisplay.UpdatePosition(objectHit.position);

        // When user picks up item
        if (Input.GetButtonDown("Interact"))
        {
            // If the item is being sold
            if (selectedUpgrade.sold)
            {
                // Check user money
                if (!CheckCost(selectedUpgrade.upgradeSpell.cost, selectedUpgrade.transform.parent.gameObject))
                    return;
            }

            // Equip spell upgrade and clear
            spellSystem.UpgradeSpellSlot(selectedUpgrade.spellSlot);
            selectorEffect.SetActive(false);
            Destroy(selectedUpgrade.gameObject);
            Clear();
        }
    }

    /// <summary>
    /// Returns if the user can buy an item by the given cost
    /// </summary>
    /// <returns></returns>
    private bool CheckCost(int cost, GameObject deactivate)
    {
        bool bought = false;
        // Check money with cost
        if (currencies.Skulls >= cost)
        {
            // Bought, retract money
            currencies.RemoveSkulls(cost);
            deactivate.SetActive(false);
            bought = true;
            deactivate.transform.parent.parent.GetComponent<Shop>().UpdateCosts();
        }
        else
        {
            // NO Money
            // message
        }
        return bought;
    }
}
