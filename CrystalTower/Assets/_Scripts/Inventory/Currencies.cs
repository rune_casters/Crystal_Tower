﻿using UnityEngine;

/// <summary>
/// Stores game currencies and handles communication between events and currency data
/// </summary>
[CreateAssetMenu(menuName = "Mage/Currencies")]
public class Currencies : ScriptableObject
{
    /// <summary>
    /// Skulls currency starting value
    /// </summary>
    public int startingSkulls = 0;

    /// <summary>
    /// Souls currency starting value
    /// </summary>
    public int startingSouls = 0;

    /// <summary>
    /// Skulls currency additive value obtained through trinkets
    /// (let's say when you kill an enemy you get 2 souls, if you take a trinket that adds 5 to your skulls additive the next
    /// time you kill an enemy you will get 2+5 souls)
    /// </summary>
    public int skullAdditive = 0;

    /// <summary>
    /// Souls currency additive value obtained through trinkets
    /// </summary>
    public int soulsAdditive = 0;

    /// <summary>
    /// Current skulls count
    /// </summary>
    public int Skulls { get; set; }

    /// <summary>
    /// Current souls count
    /// </summary>
    public int Souls { get; set; }

    /// <summary>
    /// Event raised when Skulls value is changed
    /// </summary>
    public GameEvent onSkullsChanged;

    /// <summary>
    /// Event raised when Souls value is changed
    /// </summary>
    public GameEvent onSoulsChanged;

    /// <summary>
    /// Add skulls to the player currencies
    /// </summary>
    public void AddSkulls(int value)
    {
        // Add skulls with given value considering additive value
        Skulls += value + skullAdditive;
        // Raise event
        onSkullsChanged.Raise();
    }

    /// <summary>
    /// Remove skulls to the player currencies
    /// </summary>
    public void RemoveSkulls(int value)
    {
        // Remove skulls with given value
        Skulls -= value;
        // Raise event
        onSkullsChanged.Raise();
    }

    /// <summary>
    /// Add souls to the player currencies
    /// </summary>
    public void AddSouls(int value)
    {
        // Add souls with given value considering additive value
        Souls += value + soulsAdditive;
        // Raise event
        onSoulsChanged.Raise();
    }

    /// <summary>
    /// Add souls when enemy dies (called through event on editor)
    /// </summary>
    public void AddEnemySouls()
    {
        AddSouls(Random.Range(1, 3));
    }

    /// <summary>
    /// Multiply current skulls by given value
    /// </summary>
    public void MultiplySkulls(int value)
    {
        Skulls *= value;
        // Raise event
        onSkullsChanged.Raise();
    }

    /// <summary>
    /// Multiply current souls by given value
    /// </summary>
    public void MultiplySouls(int value)
    {
        Souls *= value;
        // Raise event
        onSoulsChanged.Raise();
    }

    /// <summary>
    /// Add given value to additive skulls
    /// </summary>
    public void AddSkullsAdditive(int value)
    {
        skullAdditive += value;
    }

    /// <summary>
    /// Add given value to additive souls
    /// </summary>
    public void AddSoulsAdditive(int value)
    {
        soulsAdditive += value;
    }

    /// <summary>
    /// Reset entire currencies
    /// </summary>
    public void Reset()
    {
        Skulls = startingSkulls;
        Souls = startingSouls;
        skullAdditive = 0;
        soulsAdditive = 0;
        onSkullsChanged.Raise();
        onSoulsChanged.Raise();
    }
}
