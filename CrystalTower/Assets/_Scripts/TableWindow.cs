﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

/// <summary>
/// Handles display of trinkets and spells (not finished so not commented)
/// </summary>
public class TableWindow : EditorWindow
{
    private static List<TrinketData> trinketDataList    = new List<TrinketData>();
    private static List<SpellData> spellDataList        = new List<SpellData>();


    [MenuItem("| LevelDesign |/ EditTable", false, 101)]
    public static void ShowWindow()
    {
        GetWindow<TableWindow>();
        trinketDataList = Load<TrinketData>();
        spellDataList   = Load<SpellData>();
    }

    public enum tableTypes
    {
        TRINKETS,
        SPELLS
    }

    public tableTypes currentTable = tableTypes.TRINKETS;


    private void OnGUI()
    {
        currentTable = (tableTypes)EditorGUILayout.EnumPopup(currentTable);

        if (GUILayout.Button("Reload"))
        {
            trinketDataList = Load<TrinketData>();
            spellDataList   = Load<SpellData>();
        }

        switch (currentTable)
        {
            case tableTypes.TRINKETS:
                DisplayTrinkets();
                break;
            case tableTypes.SPELLS:
                DisplaySpells();
                break;
            default:
                break;
        }
        
    }

    public void DisplayTrinkets()
    {
        float win = Screen.width;
        float w1 = win * 0.20f; float w2 = win * 0.10f; float w3 = win * 0.35f; float w4 = win * 0.20f;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Name", EditorStyles.boldLabel, GUILayout.Width(w1));
        GUILayout.Label("Rarity", EditorStyles.boldLabel, GUILayout.Width(w2));
        GUILayout.Label("Description", EditorStyles.boldLabel, GUILayout.Width(w3));
        GUILayout.Label("Sprite", EditorStyles.boldLabel, GUILayout.Width(w4));
        GUILayout.EndHorizontal();

        foreach (TrinketData trinketData in trinketDataList)
        {
            GUILayout.BeginHorizontal();
            trinketData.name = GUILayout.TextField(trinketData.name, GUILayout.Width(w1));
            trinketData.rarity = (RarityColors.RARITY) EditorGUILayout.EnumPopup(trinketData.rarity, GUILayout.Width(w2));
            trinketData.description = GUILayout.TextField(trinketData.description, GUILayout.Width(w3));
            trinketData.sprite = (Sprite)EditorGUILayout.ObjectField(trinketData.sprite, typeof(Sprite), true, GUILayout.Width(w4));

            if (GUILayout.Button("Open", GUILayout.Width(50)))
                Selection.activeObject = trinketData;

            GUILayout.EndHorizontal();
        }
    }

    public void DisplaySpells()
    {
        float win = Screen.width;
        float w1 = win * 0.20f; float w2 = win * 0.10f; float w3 = win * 0.35f; float w4 = win * 0.20f;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Name", EditorStyles.boldLabel, GUILayout.Width(w1));
        GUILayout.Label("Slot", EditorStyles.boldLabel, GUILayout.Width(w2));
        GUILayout.EndHorizontal();

        foreach (SpellData spellData in spellDataList)
        {
            GUILayout.BeginHorizontal();

            spellData.name = GUILayout.TextField(spellData.name, GUILayout.Width(w1));
            spellData.slot = (SpellData.Slot) EditorGUILayout.EnumPopup(spellData.slot, GUILayout.Width(w2));

            GUILayout.EndHorizontal();
        }
    }

    public static List<T> Load<T>() where T : Object
    {
        List<T> selectedItems = new List<T>();
        string[] guids1 = AssetDatabase.FindAssets("t:" + typeof(T).ToString(), null);
        foreach (string item in guids1)
            selectedItems.Add((T)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(item), typeof(T)));
        return selectedItems;
    }
}

#endif