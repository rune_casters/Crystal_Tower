﻿using UnityEngine;

/// <summary>
/// Custom listener that handles HitEvents
/// </summary>
public class HitEventListener : MonoBehaviour
{
    /// <summary>
    /// Reference to the hit event
    /// </summary>
    public HitEvent hitEvent;

    /// <summary>
    /// Hit text prefab reference
    /// </summary>
    public GameObject prefab;

    /// <summary>
    /// UI canvas reference
    /// </summary>
    public GameObject canvasio;

    /// <summary>
    /// Add listener to the event
    /// </summary>
    public virtual void OnEnable()
    {
        if (hitEvent)
            hitEvent.hitEventData.AddListener(HitEvent);
    }

    /// <summary>
    /// Remove listener from the event
    /// </summary>
    public void OnDisable()
    {
        if (hitEvent)
            hitEvent.hitEventData.RemoveListener(HitEvent);
    }

    /// <summary>
    /// Creates damage text after receiving a HitEvent on the given position
    /// </summary>
    public void HitEvent(HitData hitData)
    {
        // Create text
        HitText newHitText = Instantiate(prefab).GetComponent<HitText>();
        // Set canvas as parent
        newHitText.transform.SetParent(canvasio.transform, false);
        // Set data
        newHitText.Initialize(hitData);
    }
}
