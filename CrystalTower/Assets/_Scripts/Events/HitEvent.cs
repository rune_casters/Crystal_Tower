﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Handles hit events (whenever an enemy is being hit a hitdata is being raised)
/// </summary>
[CreateAssetMenu(menuName = "Event/HitEvent")]
public class HitEvent : ScriptableObject
{
    /// <summary>
    /// Reference to the hit event data
    /// </summary>
    [HideInInspector]
    public HitEventData hitEventData = new HitEventData();

    /// <summary>
    /// Raises hit event data so on the HUD side it can read the event and create hit text
    /// </summary>
    /// <param name="hitData"></param>
    public void Raise(HitData hitData)
    {
        hitEventData.Invoke(hitData);
    }
}

/// <summary>
/// Simple struct that stores hit data with damage and position
/// </summary>
public struct HitData
{
    public float value;
    public Vector3 position;

    public HitData(float _value, Vector3 _position)
    {
        value = _value;
        position = _position;
    }
}

/// <summary>
/// Necessary hit empty event data used to serialize in inspector
/// </summary>
public class HitEventData : UnityEvent<HitData>
{

}
