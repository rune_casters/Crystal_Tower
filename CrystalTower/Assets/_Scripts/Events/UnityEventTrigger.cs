﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Handles raising given unity events when player enters or exits trigger
/// </summary>
public class UnityEventTrigger : MonoBehaviour
{
    /// <summary>
    /// Event to raise when player enters collider
    /// </summary>
    public UnityEvent enterEvent;
    /// <summary>
    /// Event to raise when player exits collider
    /// </summary>
    public UnityEvent exitEvent;

    /// <summary>
    /// Raises enter event if player enters and an enter event exists
    /// </summary>
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && enterEvent != null)
            enterEvent.Invoke();
    }

    /// <summary>
    /// Raises exit event if player exits and an exit event exists
    /// </summary>
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && exitEvent != null)
            exitEvent.Invoke();
    }
}
