﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Event Manager that stores and handles Unity Events (not Game Events)
/// </summary>
public class EventManager : MonoBehaviour
{
    /// <summary>
    /// Static reference of the event manager
    /// </summary>
    public static EventManager instance;

    /// <summary>
    /// Stores unity events by name
    /// </summary>
    private Dictionary<string, UnityEvent> eventDictionary;
    
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Init();
    }

    /// <summary>
    /// Creates event dictionary
    /// </summary>
    private void Init()
    {
        if (eventDictionary == null)
            eventDictionary = new Dictionary<string, UnityEvent>();
    }

    /// <summary>
    /// Registers listener from event through given name or creates a new one 
    /// </summary>
    public static void StartListening(string eventName, UnityAction listener)
    {
        UnityEvent thisEvent = null;
        // Check if it exists
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            thisEvent.AddListener(listener);
        else
        {
            // Create event and add listener
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    /// <summary>
    /// Unregisters listener from event through given name 
    /// </summary>
    public static void StopListening(string eventName, UnityAction listener)
    {
        if (instance == null) return;
        UnityEvent thisEvent = null;
        // Check if it exists
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            thisEvent.RemoveListener(listener);
    }

    /// <summary>
    /// Trigger event through given name
    /// </summary>
    public static void TriggerEvent(string eventName)
    {
        UnityEvent thisEvent = null;
        // If it exists then invoke it
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            thisEvent.Invoke();
    }


}
