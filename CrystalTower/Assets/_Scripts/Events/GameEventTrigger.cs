﻿using UnityEngine;

/// <summary>
/// Handles raising given game events when player enters or exits trigger
/// </summary>
public class GameEventTrigger : MonoBehaviour
{
    /// <summary>
    /// Event to raise when player enters collider
    /// </summary>
    public GameEvent enterEvent;

    /// <summary>
    /// Event to raise when player exits collider
    /// </summary>
    public GameEvent exitEvent;

    /// <summary>
    /// Raises enter event if player enters and an enter event exists
    /// </summary>
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && enterEvent)
            enterEvent.Raise();
    }

    /// <summary>
    /// Raises exit event if player exits and an exit event exists
    /// </summary>
    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && exitEvent)
            exitEvent.Raise();
    }
}
