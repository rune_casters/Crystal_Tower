﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Display given key with message
/// </summary>
public class InputDisplay : MonoBehaviour
{
    /// <summary>
    /// Reference to the key text
    /// </summary>
    [SerializeField] private TextMeshProUGUI inputKey;

    /// <summary>
    /// Reference to the key sprite
    /// </summary>
    [SerializeField] private Image spriteKey;

    /// <summary>
    /// Camera reference
    /// </summary>
    [SerializeField] private Camera cam;

    private Image parentImage;

    private void Awake()
    {
        // Cache background
        parentImage = GetComponentInParent<Image>();
    }

    /// <summary>
    /// Updates display text with given keycode and message
    /// </summary>
    public void SetData(KeyCode keyCode, string message)
    {
        inputKey.text = "Press " + keyCode + message;
    }

    /// <summary>
    /// Updates display so it follows given position
    /// </summary>
    /// <param name="position"></param>
    public void UpdatePosition(Vector3 position)
    {
        Vector3 worldPos = cam.WorldToScreenPoint(position);
        // Depth check
        bool visible = worldPos.z > 0;
        parentImage.enabled = visible;
        inputKey.enabled    = visible;
        spriteKey.enabled   = visible;
        transform.position  = worldPos;
    }
}
