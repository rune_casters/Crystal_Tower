﻿using UnityEngine;

/// <summary>
/// Handles communication between input and the current trigger
/// </summary>
public class GameEventInputTrigger : MonoBehaviour
{
    /// <summary>
    /// Reference to the used input display
    /// </summary>
    public InputDisplay inputDisplay;

    /// <summary>
    /// Keycode used currently
    /// </summary>
    public KeyCode keyCode;

    /// <summary>
    /// Current transform that it follows
    /// </summary>
    public Transform transformFollow;

    /// <summary>
    /// Current message displayed
    /// </summary>
    public string message;

    /// <summary>
    /// Event to trigger when player enters trigger and hits correct keycode
    /// </summary>
    public GameEvent enterEvent;

    /// <summary>
    /// Event to trigger when player exits trigger
    /// </summary>
    public GameEvent exitEvent;

    /// <summary>
    /// If the target is inside trigger
    /// </summary>
    private bool insideTrigger;

    /// <summary>
    /// Displays input display when player enters trigger
    /// </summary>
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Display input display with given keycode and message
            inputDisplay.SetData(keyCode, message);
            inputDisplay.gameObject.SetActive(true);
            // Enables flag
            insideTrigger = true;
        }
    }

    /// <summary>
    /// Updates input display position and check for correct user input
    /// </summary>
    public void Update()
    {
        // Check if player is inside
        if (insideTrigger)
        {
            // Update UI position 
            inputDisplay.UpdatePosition(transformFollow.position);
            // Check for input to raise event
            if (Input.GetKeyDown(keyCode))
                enterEvent.Raise();
        }
    }

    /// <summary>
    /// Disable input display when player is out of trigger
    /// </summary>
    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            insideTrigger = false;
            inputDisplay.gameObject.SetActive(false);
        }
    }
}
