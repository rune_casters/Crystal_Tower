﻿using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Design Helper class that handles small tool methods
/// </summary>
public class DesignToolset : MonoBehaviour
{
    #if UNITY_EDITOR

    /// <summary>
    /// Create room structure to help designer build levels faster
    /// </summary>
    [MenuItem("| LevelDesign |/RoomStructure", false, 53)]
    public static void RoomStructure()
    {
        // Get Presets
        UnityEditor.Presets.Preset roomBasePreset = (UnityEditor.Presets.Preset)Resources.Load("RoomBasePreset");
        UnityEditor.Presets.Preset navMeshPreset = (UnityEditor.Presets.Preset)Resources.Load("NavMeshPreset");
        UnityEditor.Presets.Preset spawnManagerPreset = (UnityEditor.Presets.Preset)Resources.Load("SpawnManagerBasePreset");

        // Create Room Components and apply presets
        GameObject room = Selection.activeTransform.gameObject;
        Room roomComponent = room.AddComponent<Room>();
        SpawnManager spawnManager = room.AddComponent<SpawnManager>();
        NavMeshSurface navMesh = room.AddComponent<NavMeshSurface>();
        roomBasePreset.ApplyTo(roomComponent);
        navMeshPreset.ApplyTo(navMesh);
        spawnManagerPreset.ApplyTo(spawnManager);

        // Create room structure
        GameObject structureRoot = new GameObject("Structure");
        GameObject portalsRoot = new GameObject("Portals");
        GameObject spawnsRoot = new GameObject("Spawns");
        GameObject walkable = new GameObject("Walkable");
        GameObject startPos = new GameObject("StartPos");

        // Set parent and reset to 0
        structureRoot.transform.parent = room.transform;
        portalsRoot.transform.parent = room.transform;
        spawnsRoot.transform.parent = room.transform;
        walkable.transform.parent = room.transform;
        startPos.transform.parent = room.transform;

        for (int i = 0; i < room.transform.childCount; i++)
        {
            room.transform.GetChild(i).localPosition = Vector3.zero; 
        }

        // Set references
        roomComponent.portalsContainer = portalsRoot.transform;
        roomComponent.spawnManagerRef = spawnManager;
        spawnManager.enemySpawnRoot = spawnsRoot.transform;
        // Build navmesh
        navMesh.BuildNavMesh();

    }

    #endif
}
