﻿using UnityEngine;

/// <summary>
/// Handles comunication between player's data references such as stats/currencies and the scene
/// </summary>
public class Mage : MonoBehaviour
{
    /// <summary>
    /// Static reference to the Character controller (FPS physics controller)
    /// </summary>
    public static RigidbodyFirstPersonController rigidFps;

    /// <summary>
    /// Player's animator reference
    /// </summary>
    public Animator animator;

    /// <summary>
    /// Player stats
    /// </summary>
    public Stats mageStats;

    /// <summary>
    /// Currency stats
    /// </summary>
    public Currencies currencies;

    /// <summary>
    /// Cache references
    /// </summary>
    public void Awake()
    {
        rigidFps = GetComponent<RigidbodyFirstPersonController>();
        mageStats.playerTransform = transform;
    }

    /// <summary>
    /// Reset player stats
    /// </summary>
    public void Start()
    {
        mageStats.animator = animator;
        mageStats.CreateDictionary();
        mageStats.Reset();
        currencies.Reset();
    }

    /// <summary>
    /// When a player is hit by the given value send it to the stats
    /// </summary>
    public void Hit(float value)
    {
        mageStats.Hit(value);
    }

}
