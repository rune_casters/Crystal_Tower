﻿using UnityEngine;

/// <summary>
/// Base spell class that stores the spell prefab (mesh, effects and more) and a reference to the spell data
/// </summary>
public abstract class Spell : MonoBehaviour
{
    /// <summary>
    /// Physics layers used by the spell
    /// </summary>
    public LayerMask spellLayerMask;

    /// <summary>
    /// Prefab used by the spell
    /// </summary>
    protected GameObject prefab;

    /// <summary>
    /// Data used by the spell
    /// </summary>
    [HideInInspector] public SpellData data;

    /// <summary>
    /// Add effects to the given obj
    /// </summary>
    /// <param name="obj"></param>
    public void AddEffects(GameObject obj)
    {
        foreach (EffectData eff in data.effects)
        {
            eff.DoEffect(obj);
        }
    }
}
