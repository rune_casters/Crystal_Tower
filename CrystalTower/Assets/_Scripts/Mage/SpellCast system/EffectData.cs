﻿using UnityEngine;

/// <summary>
/// Base class for all spell effects that add component to game objects
/// </summary>
public abstract class EffectData : ScriptableObject
{
    public abstract void DoEffect(GameObject obj);
}
