﻿using UnityEngine;

/// <summary>
/// Handles the spell GCD, spell changes and functionalities that all spells has
/// </summary>
[CreateAssetMenu]
public class SpellSystem : ScriptableObject
{
    /// <summary>
    /// the actual player spells
    /// </summary>
    [Header("Spells")]
    public PlayerSpell[] spells;
    /// <summary>
    /// Event that raised when a spell is changed
    /// </summary>
    public GameEvent OnSpellChanged;
    /// <summary>
    /// The GCD bas time
    /// </summary>
    public float baseGcdTime = 0.5f;
    /// <summary>
    /// Time left on the gcd
    /// </summary>
    public float GcdTimeLeft { get; set; }
    /// <summary>
    /// The spells the player starts with each game
    /// </summary>
    public SpellData[] baseSpells;

    /// <summary>
    /// Global cooldown countdown
    /// </summary>
    /// <returns> return true if the time left is less or equal to 0</returns>
    public bool UpdateGlobalCooldown()
    {
        if (GcdTimeLeft >= 0) GcdTimeLeft -= Time.deltaTime;
        return GcdTimeLeft <= 0;
    }

    /// <summary>
    /// Resets the gloabl cooldown
    /// </summary>
    public void ResetGlobalCooldown()
    {
        GcdTimeLeft = baseGcdTime;
    }
    /// <summary>
    /// Resets all the spells CD 
    /// </summary>
    public void Reset()
    {
        foreach (PlayerSpell spell in spells)
            spell.CD = 0;
    }

    /// <summary>
    /// Reduces the given value from all spells cd
    /// </summary>
    /// <param name="value">the value to reduce from the spells cd</param>
    public void ReduceAllSpellsCd(float value)
    {
        foreach (PlayerSpell spell in spells)
        {
            spell.CD = Mathf.Max(0, spell.CD - value);
        }
    }

    /// <summary>
    /// changes the given spell for the same on its slot
    /// </summary>
    /// <param name="newSpell">the new spell</param>
    public void ChangeSpell(SpellData newSpell)
    {
        foreach (PlayerSpell currentSpell in spells)
        {
            if (currentSpell.spell.slot == newSpell.slot)
            {
                currentSpell.spell = newSpell;
            }
        }
        // Raise the spell change event
        OnSpellChanged.Raise();
    }

    /// <summary>
    /// Resets every spell to their base state
    /// </summary>
    public void ChangeToBaseSpells()
    {
        for (int i = 0; i < spells.Length; i++)
        {
            spells[i].spell = baseSpells[i];
        }

        OnSpellChanged.Raise();
    }

    /// <summary>
    /// Upgrades the spell on the given slot
    /// </summary>
    /// <param name="spellSlot">The slot to upgrade</param>
    public void UpgradeSpellSlot(SpellData.Slot spellSlot)
    {
        foreach (PlayerSpell currentSpell in spells)
        {
            if (currentSpell.spell.slot == spellSlot)
            {
                currentSpell.spell = currentSpell.spell.upgradeSpell;
            }
        }
        // Raise the spell change event
        OnSpellChanged.Raise();
    }


}
