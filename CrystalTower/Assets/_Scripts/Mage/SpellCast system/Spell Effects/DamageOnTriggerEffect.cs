﻿using UnityEngine;
/// <summary>
/// EffectData that adds DamageOnTriggerComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName = "Ability/Effects/DamageOnTrigger")]
public class DamageOnTriggerEffect : EffectData
{
    /// <summary>
    /// Damage multiplicator
    /// </summary>
    public float damageMul;
    /// <summary>
    /// adds DamageOnTriggerComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<DamageOnTriggerComponent>().damageMul = damageMul;
    }
}
