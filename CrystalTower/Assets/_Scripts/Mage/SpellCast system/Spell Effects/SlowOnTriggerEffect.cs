﻿using UnityEngine;
/// <summary>
/// EffectData that adds SlowOnTriggerComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName = "Ability/Effects/SlowOnTrigger")]
public class SlowOnTriggerEffect : EffectData
{
    /// <summary>
    /// slow amount
    /// </summary>
    public float slowAmount;
    /// <summary>
    /// Effect duration
    /// </summary>
    public float slowTime;
    /// <summary>
    /// Effect chance
    /// </summary>
    public float effectChance;
    /// <summary>
    /// Visual effect
    /// </summary>
    public GameObject effect;
    /// <summary>
    /// Adds SlowOnTriggerComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<SlowOnTriggerComponent>().Initilaize(slowTime,slowAmount,effectChance,effect);
    }

}
