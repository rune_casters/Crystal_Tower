﻿using UnityEngine;
/// <summary>
/// EffectData that adds GoToFloorComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName = "Ability/Effects/GoToFloor")]
public class GoToFloorEffect : EffectData
{
    /// <summary>
    /// Adds GoToFloorComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<GoToFloorComponent>();
    }
}
