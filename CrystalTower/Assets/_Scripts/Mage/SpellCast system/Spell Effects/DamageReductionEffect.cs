﻿using UnityEngine;
/// <summary>
/// EffectData that adds DamageReductionComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName = "Ability/Effects/DamageReduction")]
public class DamageReductionEffect : EffectData
{
    /// <summary>
    /// Damage reduction amount
    /// </summary>
    public float damageReduction;
    /// <summary>
    /// Damage reduction buff time
    /// </summary>
    public float buffTime;
    /// <summary>
    /// adds DamageReductionComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        DamageReductionComponent clone = obj.AddComponent<DamageReductionComponent>();
        clone.damageReduction = damageReduction;
        clone.buffTime = buffTime;
    }
}
