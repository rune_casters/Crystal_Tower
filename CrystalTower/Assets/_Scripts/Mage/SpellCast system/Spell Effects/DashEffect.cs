﻿using UnityEngine;
/// <summary>
/// EffectData that adds DashComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName = "Ability/Effects/Dash")]
public class DashEffect : EffectData
{
    /// <summary>
    /// the dash force
    /// </summary>
    public float force;
    /// <summary>
    /// Adds DashComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<DashComponent>().force = force;
    }
}
