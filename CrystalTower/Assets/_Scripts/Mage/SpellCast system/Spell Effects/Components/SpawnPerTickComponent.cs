﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// SpawnPerTickEffect functionalities
/// </summary>
public class SpawnPerTickComponent : MonoBehaviour
{
    /// <summary>
    /// Objects to spawn list
    /// </summary>
    public List<SpellData> objToSpawn;
    /// <summary>
    /// Spawn rate
    /// </summary>
    public float rate;

    /// <summary>
    /// invokes repeating Spawn
    /// </summary>
    void Start()
    {
        Spell spell = GetComponent<Spell>();
        InvokeRepeating("Spawn", 0, rate);
    }
    /// <summary>
    /// Spawns all the spells in the list every tick 
    /// </summary>
    private void Spawn()
    {
        Transform[] tarray = new Transform[3];
        tarray[0] = transform;
        tarray[1] = transform;
        tarray[2] = transform;
        for (int i = 0; i < objToSpawn.Count; i++)
        {
            objToSpawn[i].Initialize(tarray);
            objToSpawn[i].Cast();
        }
    }
}
