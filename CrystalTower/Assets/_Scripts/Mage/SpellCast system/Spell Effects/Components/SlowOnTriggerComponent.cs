﻿using UnityEngine;
/// <summary>
/// Slow on trigger effect functionalities
/// </summary>
public class SlowOnTriggerComponent : MonoBehaviour
{
    /// <summary>
    /// slow amount
    /// </summary>
    public float slowAmount;
    /// <summary>
    /// Effect duration
    /// </summary>
    public float slowTime;
    /// <summary>
    /// Effect chance
    /// </summary>
    public float effectChance;
    /// <summary>
    /// Visual effect
    /// </summary>
    public GameObject effect;
    /// <summary>
    /// Adds slow effect to the collided enemy
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Enemy>() && Random.Range(.0f,100.0f) <= effectChance)
        {
            Slow slow = other.gameObject.AddComponent<Slow>();
            slow.Initialize(slowTime,slowAmount, Instantiate(effect, other.transform));
        }
    }
    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_slowTime"></param>
    /// <param name="_slowAmount"></param>
    /// <param name="_effectChance"></param>
    /// <param name="_effect"></param>
    public void Initilaize(float _slowTime, float _slowAmount, float _effectChance, GameObject _effect)
    {
        slowTime = _slowTime;
        slowAmount = _slowAmount;
        effectChance = _effectChance;
        effect = _effect;
    }
}
