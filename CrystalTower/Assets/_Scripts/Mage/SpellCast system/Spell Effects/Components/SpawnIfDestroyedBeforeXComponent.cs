﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// SpawnIfDestroyedBeforeXEffect functionalities
/// </summary>
public class SpawnIfDestroyedBeforeXComponent : MonoBehaviour
{
    /// <summary>
    /// Object to spawn
    /// </summary>
    private List<SpellData> objToSpawn;
    /// <summary>
    /// time before the effect spires
    /// </summary>
    private float maxTime;
    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_objToSpawn"></param>
    /// <param name="_time"></param>
    public void Initialize(List<SpellData> _objToSpawn, float _time)
    {
        objToSpawn = _objToSpawn;
        maxTime = _time;
    }
    /// <summary>
    /// Adds the SpawnIfDestroyedBeforeX script to the collided enemy
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Enemy>())
        {
            other.gameObject.AddComponent<SpawnIfDestroyedBeforeX>().Initialize(objToSpawn, maxTime);
        }
    }

    
}
