﻿using UnityEngine;
/// <summary>
/// TeleEffect functionalities
/// </summary>
public class TeleComponent : MonoBehaviour
{
    /// <summary>
    /// The gameObject with the scan trigger
    /// </summary>
    public GameObject detector;
    /// <summary>
    /// Spawns the detector with the gameObjects 
    /// </summary>
    void Start()
    {
        Instantiate(detector, transform);
    }

}
