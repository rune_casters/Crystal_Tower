﻿using UnityEngine;
/// <summary>
/// DotEffect functionalities
/// </summary>
public class DotOnTriggerComponent : MonoBehaviour
{
    /// <summary>
    /// visual effect
    /// </summary>
    public GameObject effect;
    /// <summary>
    /// damage tick rate
    /// </summary>
    public float tickRate;
    /// <summary>
    /// damage buff duration
    /// </summary>
    public float maxLenght;
    /// <summary>
    /// Damage per tick
    /// </summary>
    public float tickDamage;
    /// <summary>
    /// Effect name
    /// </summary>
    public string dotName;
    /// <summary>
    /// Y axis offset to set the visual
    /// </summary>
    public float yOffset;
    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_tickRate"></param>
    /// <param name="_maxLength"></param>
    /// <param name="_tickDamage"></param>
    /// <param name="_effect"></param>
    /// <param name="_name"></param>
    /// <param name="_yOffset"></param>
    public void Initialize(float _tickRate, float _maxLength, float _tickDamage, GameObject _effect, string _name, float _yOffset)
    {
        tickRate = _tickRate;
        maxLenght = _maxLength;
        tickDamage = _tickDamage;
        effect = _effect;
        dotName = _name;
        yOffset = _yOffset;
    }

    /// <summary>
    /// Adds the Dot class to the gameObject and spawns the visual effect
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Enemy>())
        {
            GameObject go = Instantiate(effect, other.transform);
            go.transform.position += new Vector3(0, yOffset, 0);
            other.gameObject.AddComponent<Dot>().Initialize(tickRate,maxLenght,tickDamage, GetComponent<Spell>().data.mageRef.damage.CurrentValue, name, go, yOffset);
        }
    }
}
