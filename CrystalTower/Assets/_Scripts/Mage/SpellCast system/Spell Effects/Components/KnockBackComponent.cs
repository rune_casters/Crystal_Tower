﻿using UnityEngine;
/// <summary>
/// KnockBackComponent functionalities
/// </summary>
public class KnockBackComponent : MonoBehaviour
{
    /// <summary>
    /// The knockBack force
    /// </summary>
    public float force;
    /// <summary>
    /// Gets the player controller and adds force backwards
    /// </summary>
    private void Start()
    {
        RigidbodyFirstPersonController rb = transform.parent.GetComponentInParent<RigidbodyFirstPersonController>();
        rb.AddForce(-transform.parent.forward * force);
    }
}
