﻿using UnityEngine;
/// <summary>
/// Damage on Trigger effect functionalities
/// </summary>
public class DamageOnTriggerComponent : MonoBehaviour
{
    /// <summary>
    /// The damage multiplicator
    /// </summary>
    public float damageMul;
    /// <summary>
    /// Deals damage on trigger enter 
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        // if the hited object is hiteable
        if (other.GetComponent<IHittable>() != null)
        {
            // Get the damage and hit the hiteable object
            Spell spell = GetComponent<Spell>();
            Stats.Stat damage = spell.data.mageRef.damage;
            other.GetComponent<IHittable>().Hit(damageMul * damage.CurrentValue * damage.multiplicator, other.transform.position);
        }
    }
}


