﻿using UnityEngine;
/// <summary>
/// StunOnXHitEffect functionalities
/// </summary>
public class StunOnXHitComponent : MonoBehaviour
{
    /// <summary>
    /// stun duration
    /// </summary>
    private float stunDuration;
    /// <summary>
    /// Initializes the stun duration
    /// </summary>
    /// <param name="_stunDuration"></param>
    public void Initialize(float _stunDuration)
    {
        stunDuration = _stunDuration;
    }
    /// <summary>
    /// Stuns the collided enemy
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Enemy>())
        {
            Stun stun = other.GetComponent<Stun>();
            if (stun)
            {
                stun.StunEnemy();
            }
            else
            {
                other.gameObject.AddComponent<Stun>().Initialize( stunDuration);
            }
        }
    }
}
