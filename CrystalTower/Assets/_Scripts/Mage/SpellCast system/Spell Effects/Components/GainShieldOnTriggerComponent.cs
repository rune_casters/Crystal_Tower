﻿using UnityEngine;
/// <summary>
/// Gain shield effect functionalities
/// </summary>
public class GainShieldOnTriggerComponent : MonoBehaviour
{
    /// <summary>
    /// shield gain value
    /// </summary>
    public float shieldGain;
    /// <summary>
    /// shield duration
    /// </summary>
    public float shieldTime;
    /// <summary>
    /// if triggers with an enemy gives shield to the palyer
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Spell spell = GetComponent<Spell>();
            spell.data.mageRef.GainShield(shieldGain, shieldTime);
        }
    }
}
