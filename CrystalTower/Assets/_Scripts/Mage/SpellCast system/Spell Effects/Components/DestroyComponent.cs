﻿using UnityEngine;
/// <summary>
/// Destroy effect functionalities 
/// </summary>
public class DestroyComponent : MonoBehaviour
{
    /// <summary>
    /// time left to destroy the gameObject
    /// </summary>
    public float timeToDestroy;
    /// <summary>
    /// the gameObject rigidbody
    /// </summary>
    private Rigidbody rb;
    /// <summary>
    /// The gameObject renderer
    /// </summary>
    private Renderer rend;
    /// <summary>
    /// The gameObject collider
    /// </summary>
    private Collider col;

    /// <summary>
    /// Initializes all the variables and invokes DisableGameObject 
    /// </summary>
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
        col = GetComponent<Collider>();
        
        Invoke("DisableGameObject", timeToDestroy);
    }
    /// <summary>
    /// Disables the rigidbodu the renderer and the collider and destroys the game object after 0.5s
    /// </summary>
    private void DisableGameObject()
    {
        if (rb) rb.Sleep();
        if (rend) rend.enabled = false;
        if (col) col.enabled = false;
        Destroy(gameObject, 0.5f);
    }

}
