﻿using UnityEngine;
/// <summary>
/// DamageReduction effect functionality
/// </summary>
public class DamageReductionComponent : MonoBehaviour
{
    /// <summary>
    /// damage reduction value
    /// </summary>
    public float damageReduction;
    /// <summary>
    /// the time the effect lasts
    /// </summary>
    public float buffTime;
    /// <summary>
    /// Initializes all the varibales and sets the player recieved damage multiplicator
    /// </summary>
    void Start()
    {
        Spell spell = GetComponent<Spell>();
        spell.data.mageRef.SetDamageReceivedMultiplicator(spell.data.mageRef.damageReceivedMultiplicator.CurrentValue - damageReduction);
        Destroy(gameObject, buffTime);
    }
    /// <summary>
    /// Resets the player recieved damage multiplicator
    /// </summary>
    private void OnDestroy()
    {
        Spell spell = GetComponent<Spell>();
        spell.data.mageRef.SetDamageReceivedMultiplicator(spell.data.mageRef.damageReceivedMultiplicator.CurrentValue + damageReduction);
    }
    

}
