﻿using UnityEngine;
/// <summary>
/// VulnerableOnTriggerEffect functionalities
/// </summary>
public class VulnerableOnTriggerComponent : MonoBehaviour
{
    /// <summary>
    /// damage multiplicator
    /// </summary>
    public float damageMul;
    /// <summary>
    /// vulnerable window time
    /// </summary>
    public float time;
    /// <summary>
    /// effect chance
    /// </summary>
    public float effectChance;
    /// <summary>
    /// visual effect
    /// </summary>
    public GameObject effect;
    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_damageMul"></param>
    /// <param name="_time"></param>
    /// <param name="_effectChance"></param>
    /// <param name="_effect"></param>
    public void Initialize(float _damageMul, float _time, float _effectChance, GameObject _effect)
    {
        damageMul = _damageMul;
        time = _time;
        effectChance = _effectChance;
        effect = _effect;
    }
    /// <summary>
    /// Adds Vulnerable to the collided enemy
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Enemy>() && Random.Range(0f, 100f) <= effectChance)
        {
            other.gameObject.AddComponent<Vulnerable>().Initilaize(time, damageMul,Instantiate(effect, other.transform));
        }
    }
}
