﻿using UnityEngine;
/// <summary>
/// RaiseEvent effect functionalities
/// </summary>
public class RaiseEventOnTriggerComponent : MonoBehaviour
{
    /// <summary>
    /// the layers to avoid with the trigger
    /// </summary>
    private LayerMask layersToAvoid; 
    /// <summary>
    /// The event to rise on trigger enter
    /// </summary>
    private GameEvent eventToRaise;
    /// <summary>
    /// The effect chance
    /// </summary>
    private float percentage;
    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_event"></param>
    /// <param name="_layersToAvoid"></param>
    /// <param name="_percentage"></param>
    public void Initialize(GameEvent _event,LayerMask _layersToAvoid, float _percentage)
    {
        eventToRaise = _event;
        layersToAvoid = _layersToAvoid;
        percentage = _percentage;
    }
    /// <summary>
    /// if triggers with something raises the event
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (layersToAvoid != (layersToAvoid | (1 << other.gameObject.layer)) && percentage >= Random.Range(.0f,100f))
        {
            eventToRaise.Raise();
        }
    }
}
