﻿using UnityEngine;
/// <summary>
/// DestroyOnTrigger effect functionalitie
/// </summary>
public class DestroyOnTriggerComponent : MonoBehaviour
{
    /// <summary>
    /// the gameObject rigidbody
    /// </summary>
    private Rigidbody rb;
    /// <summary>
    /// The gameObject renderer
    /// </summary>
    private Renderer rend;
    /// <summary>
    /// The gameObject collider
    /// </summary>
    private Collider col;
    /// <summary>
    /// the layers to avoid for the collider
    /// </summary>
    public LayerMask layersToAvoid; 
    /// <summary>
    /// Initializes all the variables 
    /// </summary>
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<MeshRenderer>();
        for (int i = 0; i < transform.childCount && !rend; i++)
        {
            rend = transform.GetChild(i).GetComponent<MeshRenderer>();
        }
        col = GetComponent<Collider>();
    }
    /// <summary>
    /// Disables the rigidbodu the renderer and the collider and destroys the game object after 0.5s
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        
        if(layersToAvoid != (layersToAvoid | (1<<other.gameObject.layer)))
        {
            if (rb) rb.Sleep();
            if (rend) rend.enabled = false;
            if (col) col.enabled = false;
            Destroy(gameObject, 0.5f);
        }
    }

    
}

