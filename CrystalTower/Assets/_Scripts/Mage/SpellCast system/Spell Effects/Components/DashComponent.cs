﻿using UnityEngine;
/// <summary>
/// Dash effect functionalities
/// </summary>
public class DashComponent : MonoBehaviour
{
    /// <summary>
    /// the dash force
    /// </summary>
    public float force;
    /// <summary>
    /// Calls the player controller dash function
    /// </summary>
    private void Start()
    {
        transform.parent.GetComponentInParent<RigidbodyFirstPersonController>().Dash(force);
    }
}
