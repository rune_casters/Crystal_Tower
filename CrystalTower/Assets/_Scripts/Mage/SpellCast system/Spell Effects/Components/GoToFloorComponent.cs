﻿using UnityEngine;
/// <summary>
/// GoToFloorEffect functionalities
/// </summary>
public class GoToFloorComponent : MonoBehaviour
{
    /// <summary>
    /// changes the gameObject position to go to the Ground
    /// </summary>
    void Start()
    {
        Spell spell = GetComponent<Spell>();
        RaycastHit newHit;
        if(Physics.Linecast(transform.position, transform.position + Vector3.down * 10, out newHit, spell.spellLayerMask))
        {
            transform.position = newHit.point;
        }
    }
}
