﻿using UnityEngine;
/// <summary>
/// GoThroughEnemiesEffect functionalities
/// </summary>
public class GoThroughEnemiesComponent : MonoBehaviour
{
    /// <summary>
    /// effect time
    /// </summary>
    public float time;
    /// <summary>
    /// Changes the player layer to avoid the enemy collision
    /// </summary>
    void Start()
    {
        Spell spell = GetComponent<Spell>();
        transform.parent.parent.gameObject.layer = LayerMask.NameToLayer("NoPlayerCollision");
        // Reset the player layer to the normal
        Invoke("ComeBack",time);
    }
    /// <summary>
    /// Reset the player layer
    /// </summary>
    private void ComeBack()
    {
        transform.parent.parent.gameObject.layer = 8;
    }


}
