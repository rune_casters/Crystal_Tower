﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// SpawnOnTriggerEffect functionalities
/// </summary>
public class SpawnOnTriggerComponent : MonoBehaviour
{
    /// <summary>
    /// object to spawn
    /// </summary>
    public List<SpellData> objToSpawn;
    /// <summary>
    /// layers to avoid with the trigger
    /// </summary>
    public LayerMask layersToAvoid;

    /// <summary>
    /// spawns all the objects in the list when triggers with something
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (layersToAvoid != (layersToAvoid | (1 << other.gameObject.layer)))
        {
            Transform[] tarray = new Transform[3];
            tarray[2] = transform;
            for (int i = 0; i < objToSpawn.Count; i++)
            {
                objToSpawn[i].Initialize(tarray);
                objToSpawn[i].Cast(transform.position);
            }
        }
    }
}
