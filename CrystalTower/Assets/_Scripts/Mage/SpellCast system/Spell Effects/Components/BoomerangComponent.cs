﻿using UnityEngine;

/// <summary>
/// Bonemeramg effect functionalities
/// </summary>
public class BoomerangComponent : MonoBehaviour
{
    /// <summary>
    /// max range of the effect
    /// </summary>
    public float maxRange;
    /// <summary>
    /// if the game object is going back
    /// </summary>
    private bool comeBack;
    /// <summary>
    /// the max range position of the boomerang
    /// </summary>
    private Vector3 lastPos;
    /// <summary>
    /// the rigidbody of the game object
    /// </summary>
    private Rigidbody rb;
    /// <summary>
    /// the transform where the game object comes back
    /// </summary>
    private Transform playerTransform;
    /// <summary>
    /// Initializes all the variables
    /// </summary>
    private void Start()
    {
        lastPos = transform.position + maxRange * transform.forward;
        rb = GetComponent<Rigidbody>();
        playerTransform = GetComponent<Spell>().data.mageRef.playerTransform;
    }
    /// <summary>
    /// Handles the game object movement
    /// </summary>
    private void Update()
    {
        // if is going to the lastPos and the distance to the last pos is low 
        if (Vector3.Distance(transform.position, lastPos) < 0.2f && !comeBack)
        {
            // rotate the gameObject
            transform.Rotate(0f, 180f, 0f);
            // change the rigidbody velocity direction
            Vector3 toTarget = playerTransform.position - transform.position + Vector3.up * 0.5f; ;
            Vector3 newVelocity = Vector3.RotateTowards(rb.velocity, toTarget, 99999 * Mathf.Deg2Rad * Time.fixedDeltaTime, 0);
            rb.velocity = newVelocity;
            // change the comeback bool to true
            comeBack = true;
        }
        // if is coming back and is near the player destroy the game object
        if (Vector3.Distance(transform.position, playerTransform.position) < 1.0f && comeBack ) Destroy(gameObject);
       
    }
    /// <summary>
    /// Handles collision to come back or destroy
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<IHittable>() == null)
        {
            // If the object is coming back and collides with something destroy the gameObject
            if (comeBack) Destroy(gameObject);
            // Come back else
            else comeBack = true;
        }
    }
}
