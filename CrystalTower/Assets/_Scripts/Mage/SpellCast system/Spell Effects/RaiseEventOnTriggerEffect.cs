﻿using UnityEngine;
/// <summary>
/// EffectData that adds RaiseEventOnTriggerComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu (menuName ="Ability/Effects/EventOnTrigger")]
public class RaiseEventOnTriggerEffect : EffectData
{
    /// <summary>
    /// the event to raise
    /// </summary>
    public GameEvent eventToRaise;
    /// <summary>
    /// layers to avoid the trigger
    /// </summary>
    public LayerMask layersToAvoid;
    /// <summary>
    /// Effect Chance
    /// </summary>
    public float percentage;
    /// <summary>
    /// Adds RaiseEventOnTriggerComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<RaiseEventOnTriggerComponent>().Initialize(eventToRaise, layersToAvoid,percentage);
    }
    
}
