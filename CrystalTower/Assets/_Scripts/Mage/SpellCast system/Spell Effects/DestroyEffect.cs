﻿using UnityEngine;
/// <summary>
/// EffectData that adds DestroyComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName = "Ability/Effects/DestroyOnTime")]
public class DestroyEffect : EffectData
{
    /// <summary>
    /// The time to destroy the game object
    /// </summary>
    public float timeToDestroy;
    /// <summary>
    /// Adds DestroyComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<DestroyComponent>().timeToDestroy = timeToDestroy;
    }
}