﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// EffectData that adds SpawnPerTickComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName = "Ability/Effects/SpawnPerTick")]
public class SpawnPerTickEffect : EffectData
{
    /// <summary>
    /// objects to spawn
    /// </summary>
    public List<SpellData> objToSpawn;
    /// <summary>
    /// spawn rate in seconds
    /// </summary>
    public float rate;
    /// <summary>
    /// Adds SpawnPerTickComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        SpawnPerTickComponent clone = obj.AddComponent<SpawnPerTickComponent>();
        clone.objToSpawn = objToSpawn;
        clone.rate = rate;
    }
}
