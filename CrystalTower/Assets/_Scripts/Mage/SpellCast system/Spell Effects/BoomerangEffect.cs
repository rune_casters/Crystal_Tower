﻿using UnityEngine;
/// <summary>
/// EffectData that adds BoomerangComponent to the given obj
/// </summary>
[CreateAssetMenu(menuName ="Ability/Effects/BoomerangEffect")]
public class BoomerangEffect : EffectData
{
    /// <summary>
    /// Boomerang max distance
    /// </summary>
    public float maxRange;
    /// <summary>
    /// adds BoomerangComponent to the given obj
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<BoomerangComponent>().maxRange = maxRange;
    }
}
