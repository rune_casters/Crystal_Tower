﻿using UnityEngine;
/// <summary>
/// EffectData that adds GainShieldOnTriggerComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName =("Ability/Effects/GainShieldOnTrigger"))]
public class GainShieldOnTriggerEffect : EffectData
{
    /// <summary>
    /// shield amount gain
    /// </summary>
    public float shieldGain;
    /// <summary>
    /// shield duration
    /// </summary>
    public float shieldTime;
    /// <summary>
    /// Adds GainShieldOnTriggerComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        GainShieldOnTriggerComponent a = obj.AddComponent<GainShieldOnTriggerComponent>();
        a.shieldGain = shieldGain;
        a.shieldTime = shieldTime;
    }
}
