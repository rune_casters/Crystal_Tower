﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// EffectData that adds SpawnOnTriggerComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName ="Ability/Effects/SpawnOnTrigger")]
public class SpawnOnTriggerEffect : EffectData
{
    /// <summary>
    /// obecjt to spawn
    /// </summary>
    public List<SpellData> objToSpawn;
    /// <summary>
    /// layers to avoid the trigger
    /// </summary>
    public LayerMask layersToAvoid;
    /// <summary>
    /// Adds SpawnOnTriggerComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        SpawnOnTriggerComponent component = obj.AddComponent<SpawnOnTriggerComponent>() as SpawnOnTriggerComponent;
        component.objToSpawn = objToSpawn;
        component.layersToAvoid = layersToAvoid;
    }
    
}
