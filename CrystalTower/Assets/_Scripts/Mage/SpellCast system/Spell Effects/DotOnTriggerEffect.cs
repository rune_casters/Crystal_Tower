﻿using UnityEngine;
/// <summary>
/// EffectData that adds DotOnTriggerComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName = "Ability/Effects/BurnOnTrigger")]
public class DotOnTriggerEffect : EffectData
{
    /// <summary>
    /// visual effect
    /// </summary>
    public GameObject effect;
    /// <summary>
    /// damage tick rate
    /// </summary>
    public float tickRate;
    /// <summary>
    /// damage buff duration
    /// </summary>
    public float maxLenght;
    /// <summary>
    /// Damage per tick
    /// </summary>
    public float tickDamage;
    /// <summary>
    /// Effect name
    /// </summary>
    public string dotName;
    /// <summary>
    /// Y axis offset to set the visual
    /// </summary>
    public float yOffset;
    /// <summary>
    /// Adds DotOnTriggerComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<DotOnTriggerComponent>().Initialize(tickRate,maxLenght,tickDamage,effect,dotName, yOffset);
    }
}
