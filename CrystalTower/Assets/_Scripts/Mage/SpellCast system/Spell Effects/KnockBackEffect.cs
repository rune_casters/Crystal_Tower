﻿using UnityEngine;
/// <summary>
/// EffectData that adds KnockBackComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName ="Ability/Effects/KnockBack")]
public class KnockBackEffect : EffectData
{
    /// <summary>
    /// Kockback force
    /// </summary>
    public float force;
    /// <summary>
    /// Adds KnockBackComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<KnockBackComponent>().force = force;
    }

}
