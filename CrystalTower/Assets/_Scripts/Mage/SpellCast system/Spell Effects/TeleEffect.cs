﻿using UnityEngine;
/// <summary>
/// EffectData that adds TeleComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu (menuName = "Ability/Effects/Tele")]
public class TeleEffect : EffectData
{
    /// <summary>
    /// a game object with a sphere collider
    /// </summary>
    public GameObject detector;
    /// <summary>
    /// Adds TeleComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        TeleComponent tc = obj.AddComponent<TeleComponent>();
        tc.detector = detector;
    }
}
