﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// EffectData that adds SpawnIfDestroyedBeforeXComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu (menuName ="Ability/Effects/SpawnIfDestroyedBeforeX")]
public class SpawnIfDestroyedBeforeXEffect : EffectData
{
    /// <summary>
    /// Objects to spawn list
    /// </summary>
    public List<SpellData> objToSpawn;
    /// <summary>
    /// Limit time to spawn
    /// </summary>
    public float time;
    /// <summary>
    /// Adds SpawnIfDestroyedBeforeXComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<SpawnIfDestroyedBeforeXComponent>().Initialize(objToSpawn, time);
    }

}
