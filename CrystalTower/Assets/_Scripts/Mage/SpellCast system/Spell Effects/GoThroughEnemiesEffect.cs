﻿using UnityEngine;
/// <summary>
/// EffectData that adds GoThroughEnemiesComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName ="Ability/Effects/GoThroughEnemies")]
public class GoThroughEnemiesEffect : EffectData
{
    /// <summary>
    /// Effect time
    /// </summary>
    public float time;
    /// <summary>
    /// Adds GoThroughEnemiesComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<GoThroughEnemiesComponent>().time = time;
    }
}
