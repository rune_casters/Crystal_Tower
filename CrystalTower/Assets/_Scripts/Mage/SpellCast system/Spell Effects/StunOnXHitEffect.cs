﻿using UnityEngine;
/// <summary>
/// EffectData that adds StunOnXHitComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu (menuName = "Ability/Effects/StunOnXHit")]
public class StunOnXHitEffect : EffectData
{
    /// <summary>
    /// stun duration
    /// </summary>
    public float stunDuration;
    /// <summary>
    /// Adds StunOnXHitComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<StunOnXHitComponent>().Initialize(stunDuration);
    }
}
