﻿using UnityEngine;
/// <summary>
/// EffectData that adds VulnerableOnTriggerComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName ="Ability/Effects/VulnerableOnTrigger")]
public class VulnerableOnTriggerEffect : EffectData
{
    /// <summary>
    /// damage multiplicator
    /// </summary>
    public float damageMul;
    /// <summary>
    /// vulnerable window time
    /// </summary>
    public float time;
    /// <summary>
    /// effect chance
    /// </summary>
    public float effectChance;
    /// <summary>
    /// visual effect
    /// </summary>
    public GameObject effect;
    /// <summary>
    /// Adds VulnerableOnTriggerComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<VulnerableOnTriggerComponent>().Initialize(damageMul,time,effectChance, effect);
    }
}
