﻿using UnityEngine;
/// <summary>
/// EffectData that adds DestroyOnTriggerComponent to the given obj and initializes it
/// </summary>
[CreateAssetMenu(menuName = "Ability/Effects/DestroyOnTrigger")]
public class DestroyOnTriggerEffect : EffectData
{
    /// <summary>
    /// layers to avoid the trigger
    /// </summary>
    public LayerMask layersToAvoid;
    /// <summary>
    /// Adds DestroyOnTriggerComponent to the given obj and initializes it
    /// </summary>
    /// <param name="obj"></param>
    public override void DoEffect(GameObject obj)
    {
       obj.AddComponent<DestroyOnTriggerComponent>().layersToAvoid = layersToAvoid;
    }
}

