﻿using UnityEngine;

/// <summary>
/// Rotates a gameObject speed angles per second on the y axis
/// </summary>
public class BonemerangRotate : MonoBehaviour
{
    /// <summary>
    /// the angles per second to rotate around y axis
    /// </summary>
    public float speed;
    
    /// <summary>
    /// Rotates the gameObject 
    /// </summary>
    void Update()
    {
        transform.Rotate(0, speed * Time.deltaTime, 0);
    }
}
