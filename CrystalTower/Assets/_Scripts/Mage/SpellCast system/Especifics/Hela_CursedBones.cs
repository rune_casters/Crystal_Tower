﻿using UnityEngine.Events;
using UnityEngine;

/// <summary>
/// Adds 3 charges of speed power up and loses 1 each time the event raises
/// </summary>
public class Hela_CursedBones : MonoBehaviour
{
    /// <summary>
    /// The event that the spell needs
    /// </summary>
    private GameEvent onPlayerHit;
    /// <summary>
    /// the listener and the unity event to answer the event raise
    /// </summary>
    private GameEventListener listener;
    private UnityEvent unityEvent;
    /// <summary>
    /// The count of the speed power up charges
    /// </summary>
    private float marks;
    /// <summary>
    /// the duration of the effect
    /// </summary>
    private float time;

    /// <summary>
    /// Initializes the effect
    /// </summary>
    /// <param name="_event"> The needed event to remove marks</param>
    /// <param name="_time"> The time the effect lasts</param>
    public void Initialize(GameEvent _event, float _time)
    {
        onPlayerHit = _event;
        time = _time;
    }
    
    /// <summary>
    /// Sets up the listener and adds 3 speed charges
    /// </summary>
    void Start()
    {
        // sets up the response
        unityEvent = new UnityEvent();
        unityEvent.AddListener(ReduceMark);

        listener = gameObject.AddComponent<GameEventListener>();
        listener.gameEvent = onPlayerHit;
        listener.response = unityEvent;
        listener.gameEvent.RegisterListener(listener);

        // adds 3 speed charges
        marks = 3;
        for (int i = 0; i < marks; i++)
        {
            ChangeSpeed(1);
        }
        // stablishes the duration of the buff
        Destroy(gameObject, time);
    }
    /// <summary>
    /// Reduces a mark when the event raises
    /// </summary>
    public void ReduceMark()
    {
        marks--;
        if(marks >= 0)
        {
            ChangeSpeed(-1);
        }
    }

    /// <summary>
    /// When the gameObject is detroyed remove all the marks left
    /// </summary>
    private void OnDestroy()
    {
        for (int i = 0; i < marks; i++)
        {
            ChangeSpeed(-1);
        }
    }
    /// <summary>
    /// Removes or adds speed
    /// </summary>
    /// <param name="value"> 1 to add speed -1 for removing speed</param>
    private void ChangeSpeed(float value)
    {
        Stats mageStats = GetComponent<Spell>().data.mageRef;
        mageStats.AddMaxSpeed(mageStats.speed.baseValue * 0.15f * value);
    }

  


}
