﻿using UnityEngine;

/// <summary>
/// Specific spell effect to add to a spell type
/// </summary>
[CreateAssetMenu(menuName ="Ability/Effects/CursedBones")]
public class CursedBonesEffect : EffectData
{
    /// <summary>
    /// The event the spell needs
    /// </summary>
    public GameEvent onPlayerHit;
    /// <summary>
    /// The time the effect lasts
    /// </summary>
    public float time;

    /// <summary>
    /// adds the spell component to the given gameObject
    /// </summary>
    /// <param name="obj"> The spell</param>
    public override void DoEffect(GameObject obj)
    {
        obj.AddComponent<Hela_CursedBones>().Initialize(onPlayerHit, time);
    }
}
