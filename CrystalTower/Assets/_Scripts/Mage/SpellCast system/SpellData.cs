﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class used to create float string dictionaries
/// </summary>
[System.Serializable]
public class FloatStringDictionary
{
    public string name;
    public float value;
}

/// <summary>
/// Data class that stores spell data such as slot used, name, description, cooldown, mesh, audio and more
/// </summary>
public abstract class SpellData : ScriptableObject
{
    /// <summary>
    /// Spell slot types
    /// </summary>
    public enum Slot
    {
        Basic,
        Hab1,
        Hab2,
        Mov,
        Spawn
    }

    /// <summary>
    /// Spell name
    /// </summary>
    [Header("Spell basics")]
    public new string name;
    /// <summary>
    /// Spell description
    /// </summary>
    [TextArea]
    public string description;
    /// <summary>
    /// Spell used slot
    /// </summary>
    public Slot slot;
    /// <summary>
    /// Spell sprite
    /// </summary>
    public Sprite sprite;
    // Not used currently
    public Sprite normalRune;
    // Not used currently
    public Sprite upgradedRune;
    /// <summary>
    /// Spell clip on launch
    /// </summary>
    public AudioClip audioClip;
    /// <summary>
    /// Spell animation name
    /// </summary>
    public string[] animationName;
    /// <summary>
    /// Spell prefab (mesh + effects)
    /// </summary>
    public GameObject prefab;
    /// <summary>
    /// Spell cooldown
    /// </summary>
    public float baseCd;
    public float secondsToCast;
    /// <summary>
    /// Mage stats reference
    /// </summary>
    public Stats mageRef;
    /// <summary>
    /// Base spell (previous)
    /// </summary>
    public SpellData baseSpell;
    /// <summary>
    /// Upgraded spell (next)
    /// </summary>
    public SpellData upgradeSpell;
    /// <summary>
    /// Shop cost
    /// </summary>
    public int cost = 250;

    /// <summary>
    /// List of spell effects (damage on trigger, explosion etc...)
    /// </summary>
    [Header("Spell Effects")]
    public List<EffectData> effects;

    /// <summary>
    /// Initializes spell on given shooting point transform
    /// </summary>
    public abstract void Initialize(Transform[] shootPoint = null);

    /// <summary>
    /// Casts spell on given position
    /// </summary>
    /// <param name="pos"></param>
    public abstract void Cast(Vector3 pos = default);

}
