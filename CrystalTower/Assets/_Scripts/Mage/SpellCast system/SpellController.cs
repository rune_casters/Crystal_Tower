﻿using UnityEngine;
/// <summary>
/// Handles the spell cooldown, the global cooldown and the casts
/// </summary>
[System.Serializable]
public class PlayerSpell
{
    /// <summary>
    /// The spell name for editor
    /// </summary>
    public string name;
    /// <summary>
    /// The buttom that activates the spell on that slot
    /// </summary>
    public KeyCode button;
    /// <summary>
    /// the spell on the slot
    /// </summary>
    public SpellData spell;
    /// <summary>
    /// the event that raises when pressed the buttom
    /// </summary>
    public GameEvent onCastEvent;
    /// <summary>
    /// The tiem left to be able to cast again
    /// </summary>
    public float CD { get; set; }

    /// <summary>
    /// subtracts time from the cooldown
    /// </summary>
    public void CoolDown()
    {
        if (CD >= 0)
            CD -= Time.deltaTime;
    }

    /// <summary>
    /// Casts the spell on the slot raises the event and restets the cooldown
    /// </summary>
    public void Cast()
    {
        CD = spell.baseCd;
        onCastEvent.Raise();
        spell.Cast();
    }
    /// <summary>
    /// Returns if the spell has cd left or not
    /// </summary>
    /// <returns> true when CD is less or equal to 0</returns>
    public bool CheckCd()
    {
        return CD <= 0;
    }
    /// <summary>
    /// Sets the first cd on 0
    /// </summary>
    public void Initialize()
    {
        CD = 0;
    }

    /// <summary>
    /// if the spell is upgraded resets it to the base form
    /// </summary>
    public void Reset()
    {
        if (spell.baseSpell)
        {
            spell = spell.baseSpell;
            CD = 0;
        }
    }
}
/// <summary>
/// Checks for inputs and casts spells if possible
/// </summary>
public class SpellController : MonoBehaviour
{
    /// <summary>
    /// Data object with all the spells the player has
    /// </summary>
    public SpellSystem spellSystem;
    /// <summary>
    /// The palyer shootPoints
    /// </summary>
    public Transform[] shootPoints;
    /// <summary>
    /// if the player can cast
    /// </summary>
    private bool canCast;
    /// <summary>
    /// if there is a spell in the queue
    /// </summary>
    private bool queue;
    /// <summary>
    /// the queued spell
    /// </summary>
    private PlayerSpell queuedSpell;
    /// <summary>
    /// the player audio source
    /// </summary>
    public AudioSource playerAudioSource;

    /// <summary>
    /// Sets all the spells to their base form and initializes them
    /// </summary>
    void Start()
    {
        spellSystem.ChangeToBaseSpells();
        spellSystem.Reset();
        canCast = true;

        UpdateSpells();

    }

    /// <summary>
    /// Checks if the player can cast and casts if there is any input
    /// </summary>
    void Update()
    {
        if (Time.timeScale != 0)
        {
            // if there isnt any global cooldown
            canCast = spellSystem.UpdateGlobalCooldown();
            foreach (PlayerSpell currentSpell in spellSystem.spells)
            {
                // refresh all the spells cooldown
                currentSpell.CoolDown();
                // if the a spell button is pressed and the spell isnt on cooldown
                if (Input.GetKey(currentSpell.button) && currentSpell.CheckCd())
                {
                    // if the player can cast
                    if (canCast)
                    {
                        // if there is a spell in queue
                        if (queue)
                        {
                            // Cast the queued spell
                            queuedSpell.Cast();
                            // Add the audio
                            playerAudioSource.clip = queuedSpell.spell.audioClip;
                            queue = false;
                        }
                        else
                        {
                            // Cast the spell
                            currentSpell.Cast();
                            // Add the audio
                            playerAudioSource.clip = currentSpell.spell.audioClip;
                        }
                        // Play the sound
                        playerAudioSource.Play();
                        // Start the gloabl cooldown
                        spellSystem.ResetGlobalCooldown();            
                    }
                    //if the gcd is up and the player pressed a buttom add that spell to the queue
                    else if (!queue)
                    {
                        queue = true;
                        queuedSpell = currentSpell;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Initializes all the spells
    /// </summary>
    public void UpdateSpells()
    {
        foreach (PlayerSpell currentSpell in spellSystem.spells)
        {
           currentSpell.spell.Initialize(shootPoints);
        }
    }

}
