﻿using UnityEngine;
/// <summary>
/// Damage on time effect
/// </summary>
public class Dot : MonoBehaviour
{
    /// <summary>
    /// damage tick frequence
    /// </summary>
    private float tickRate;
    /// <summary>
    /// The effect time left
    /// </summary>
    private float timeLeft;
    /// <summary>
    /// The maximum duration of the effect
    /// </summary>
    private float maxLenght;
    /// <summary>
    /// time left for the next tick
    /// </summary>
    private float nextTick;
    /// <summary>
    /// the damage percentage dealed by each tick based on the player damage
    /// </summary>
    private float tickDamage;
    /// <summary>
    /// The player damage
    /// </summary>
    private float mageDamage;
    /// <summary>
    /// The visual effect
    /// </summary>
    private GameObject effect;
    /// <summary>
    /// The enemy reference
    /// </summary>
    private Enemy enem;
    /// <summary>
    /// The offset to set the effect
    /// </summary>
    private float yOffset;
    /// <summary>
    /// The name of the effect
    /// </summary>
    public string DotName { get; private set; }
    
    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_tickRate"></param>
    /// <param name="_maxLength"></param>
    /// <param name="_tickDamage"></param>
    /// <param name="_mageDamage"></param>
    /// <param name="_name"></param>
    /// <param name="_effect"></param>
    /// <param name="_yOffset"></param>
    public void Initialize(float _tickRate,float _maxLength,float _tickDamage,float _mageDamage, string _name, GameObject _effect, float _yOffset)
    {
        timeLeft = maxLenght = _maxLength;
        tickRate = _tickRate;
        nextTick = tickRate;
        tickDamage = _tickDamage;
        mageDamage = _mageDamage;
        DotName = _name;
        effect = _effect;
        enem = GetComponent<Enemy>();
        yOffset = _yOffset;
    }

    /// <summary>
    /// Deals damage every tickrate while there is still time left on the effect
    /// </summary>
    void Update()
    {
        // if there is still time
        if (timeLeft >= 0)
        {
            // sbutract time to time left and add time to next tick
            timeLeft -= Time.deltaTime;
            nextTick += Time.deltaTime;
            // if the next tick surpases the tick rate deal damage and restart the time to the nect tick
            if(nextTick >= tickRate)
            {
                nextTick = 0;
                enem.Hit(tickDamage * mageDamage,transform.position);
            }
        }
        else
        {
            // if there isnt more time left destroy the effect and the visual
            Destroy(effect);
            Destroy(this);
        }
    }

    /// <summary>
    /// Refreshes the time of the damage effect
    /// </summary>
    public void Refresh()
    {
        timeLeft = maxLenght;
    }
}
