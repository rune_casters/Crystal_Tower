﻿using UnityEngine;
/// <summary>
/// Slows an enemy an amount for a while
/// </summary>
public class Slow : MonoBehaviour
{
    /// <summary>
    /// the reference to the enemy
    /// </summary>
    private Enemy enem;
    /// <summary>
    /// The slow time left
    /// </summary>
    private float timeLeft;
    /// <summary>
    /// the maximum slow time
    /// </summary>
    private float slowTime;
    /// <summary>
    /// the slow amount
    /// </summary>
    private float slowAmount;
    /// <summary>
    /// the visual effect
    /// </summary>
    private GameObject effect;

    /// <summary>
    /// Initializes all the variables and slows the enmy
    /// </summary>
    /// <param name="_slowTime"></param>
    /// <param name="_slowAmount"></param>
    /// <param name="_effect"></param>
    public void Initialize(float _slowTime, float _slowAmount, GameObject _effect)
    {
        timeLeft = slowTime = _slowTime;
        slowAmount = _slowAmount;
        effect = _effect;
        enem = GetComponent<Enemy>();
        enem.SetSpeed(enem.currentSpeed - enem.stats.Speed * slowAmount);
    }
    
    /// <summary>
    /// destroys the slow effect and the visual after the time passes
    /// </summary>
    private void Update()
    {
        if (timeLeft >= 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            Destroy(this);
            Destroy(effect);
        }
    }
    /// <summary>
    /// Resets the enmy speed when the effect is destroyed
    /// </summary>
    private void OnDestroy()
    {
        enem.SetSpeed(enem.currentSpeed + enem.stats.Speed * slowAmount);
    }

    /// <summary>
    /// refreshes the time left to the max time
    /// </summary>
    public void Refresh()
    {
        timeLeft = slowTime;
    }
}
