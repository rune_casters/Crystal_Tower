﻿using UnityEngine;
/// <summary>
/// Makes the game object go directly to an enemy
/// </summary>
public class AutoProjectile : MonoBehaviour
{
    /// <summary>
    /// the targeted enemy
    /// </summary>
    private Transform target;
    /// <summary>
    /// The rigidbody of the game object
    /// </summary>
    private Rigidbody rb;
    /// <summary>
    /// the layers that the game object can see 
    /// </summary>
    public LayerMask layerLinecast;

    /// <summary>
    /// Caches the rigidbody
    /// </summary>
    private void Awake()
    {
        rb = transform.parent.GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Sets the target to null
    /// </summary>
    private void OnEnable()
    {
        target = null;
    }

    /// <summary>
    /// When triggers with an enemy and there is a clear path between the gameObject center and the enemy sets it as a target
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Enemy>() && !target)
        {
            RaycastHit hitInfo;
            if(Physics.Linecast(transform.position, other.transform.position, out hitInfo, layerLinecast))
                if (hitInfo.transform.position == other.transform.position)
                    target = other.transform;
        }

    }

    /// <summary>
    /// Follows the target if it has any
    /// </summary>
    void FixedUpdate () {
        //matches the velocity of the rigidbody with the new direction if it has a target
        if (target)
        {
            if (target.gameObject.activeSelf)
            {
                Vector3 toTarget = target.position - transform.position + Vector3.up * 0.5f; ;
                Vector3 newVelocity = Vector3.RotateTowards(rb.velocity, toTarget, 99999 * Mathf.Deg2Rad * Time.fixedDeltaTime, 0);
                rb.velocity = newVelocity;

            }
            else target = null;
        }
	}
}
