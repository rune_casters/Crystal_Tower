﻿using UnityEngine;
/// <summary>
/// Stuns the enemy for a time
/// </summary>
public class Stun : MonoBehaviour
{
    /// <summary>
    /// The duration of the stun
    /// </summary>
    public float stunDuration;
    /// <summary>
    /// the stunned enemy
    /// </summary>
    private Enemy enem;

    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_stunDuration"></param>
    public void Initialize(float _stunDuration)
    {
        stunDuration = _stunDuration;
        enem = GetComponent<Enemy>();
    }
    /// <summary>
    /// stuns the enemy and destroys the slow if it was slowed
    /// </summary>
    public void StunEnemy()
    {
        Invoke("Restore", stunDuration);
        enem.Stun(true);
        Slow slow = GetComponent<Slow>();
        if (slow)
        {
            Destroy(slow);
        }
    }
    /// <summary>
    /// removes the stun
    /// </summary>
    private void Restore()
    {
        enem.Stun(false);
        Destroy(this);
    }

    
}
