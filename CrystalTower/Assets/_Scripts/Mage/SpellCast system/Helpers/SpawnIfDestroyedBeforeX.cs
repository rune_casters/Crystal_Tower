﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Spawns the given gameObjects if the script is destroyed before the given time passes
/// </summary>
public class SpawnIfDestroyedBeforeX : MonoBehaviour
{
    /// <summary>
    /// the list of the spells to spawn
    /// </summary>
    private List<SpellData> objToSpawn;
    /// <summary>
    /// The time needed to pass
    /// </summary>
    private float timeLeft;

    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_objToSpawn"></param>
    /// <param name="_time"></param>
    public void Initialize(List<SpellData> _objToSpawn, float _time)
    {
        objToSpawn = _objToSpawn;
        timeLeft = _time;
    }
    /// <summary>
    /// removes time from the time left
    /// </summary>
    void Update()
    {
        if (timeLeft >= 0) timeLeft -= Time.deltaTime;
    }

    /// <summary>
    /// Spawns the game objects if there is still time left
    /// </summary>
    private void OnDestroy()
    {
        if (timeLeft > 0)
        {
            // all the spells need an array of transforms to initialize, in this case there is just 1 transform for the 3 spots
            Transform[] tarray = new Transform[3];
            tarray[0] = transform;
            tarray[1] = transform;
            tarray[2] = transform;
            // initialize and spawn each spell
            for (int i = 0; i < objToSpawn.Count; i++)
            {
                objToSpawn[i].Initialize(tarray);
                objToSpawn[i].Cast();
            }
        }
    }
}
