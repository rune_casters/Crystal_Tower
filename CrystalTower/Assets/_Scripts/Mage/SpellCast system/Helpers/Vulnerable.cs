﻿using UnityEngine;
/// <summary>
/// Adds recieved damage multiplicator to an enemy
/// </summary>
public class Vulnerable : MonoBehaviour
{
    /// <summary>
    /// The enemy to set vulnerable
    /// </summary>
    private Enemy enem;
    /// <summary>
    /// The effect time left
    /// </summary>
    private float timeLeft;
    /// <summary>
    /// The effect maximum time
    /// </summary>
    private float vulnerableTime;
    /// <summary>
    /// The damage received multiplicator
    /// </summary>
    private float damageMul;
    /// <summary>
    /// The visual effect
    /// </summary>
    private GameObject effect;

    /// <summary>
    /// Initilizes all the variables and adds the vulnerable effect
    /// </summary>
    /// <param name="_slowTime"></param>
    /// <param name="_damageMul"></param>
    /// <param name="_effect"></param>
    public void Initilaize(float _slowTime, float _damageMul, GameObject _effect)
    {
        timeLeft = vulnerableTime = _slowTime;
        damageMul = _damageMul;
        effect = _effect;
        enem = GetComponent<Enemy>();
        enem.Vulnerable(damageMul);
    }

    /// <summary>
    /// Does a countdown and when it ends destroys the effect and the visual
    /// </summary>
    private void Update()
    {
        if (timeLeft >= 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            Destroy(this);
            Destroy(effect);
        }
    }
    /// <summary>
    /// When the object is destroyed resets the amount of received damage multiplicator
    /// </summary>
    private void OnDestroy()
    {
        enem.Vulnerable(-damageMul);
    }

    /// <summary>
    /// Refreshes the effect duration to the max duration
    /// </summary>
    public void Refresh()
    {
        timeLeft = vulnerableTime;
    }
}
