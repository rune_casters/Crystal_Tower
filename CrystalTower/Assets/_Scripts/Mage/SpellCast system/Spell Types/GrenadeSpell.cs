﻿using UnityEngine;

/// <summary>
/// Grenade spell data 
/// </summary>
[CreateAssetMenu (menuName ="Ability/Grenade")]
public class GrenadeSpell : SpellData
{
    /// <summary>
    /// The force to throw the grenade
    /// </summary>
    public int force;
    /// <summary>
    /// The angle to throw the grenade
    /// </summary>
    public int angle;
    /// <summary>
    /// The grenade type component that has all the functionalities
    /// </summary>
    private GrenadeType gt;

    /// <summary>
    /// Calls Fire of the Greande Type
    /// </summary>
    /// <param name="pos"></param>
    public override void Cast(Vector3 pos = default)
    {
        gt.Fire();
    }

    /// <summary>
    /// Gets the GrenadeType from the prefab and Initializes it
    /// </summary>
    /// <param name="shootPoint"> the possible shootpoints for the grenade</param>
    public override void Initialize(Transform[] shootPoint = null)
    {
        gt = prefab.GetComponent<GrenadeType>();
        gt.Initialize(this, force,angle,shootPoint);
    }
}
