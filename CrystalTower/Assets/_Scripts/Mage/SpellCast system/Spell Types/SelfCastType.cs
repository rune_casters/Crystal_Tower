﻿using UnityEngine;
/// <summary>
/// SelfCast Spell functionalities
/// </summary>
public class SelfCastType : Spell
{
    /// <summary>
    /// The transform where all the selfcast types are child of
    /// </summary>
    private Transform parent;

    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_spellRef"></param>
    /// <param name="_parent"></param>
    public void Initialize(SpellData _spellRef, Transform _parent)
    {
        data = _spellRef;
        prefab = _spellRef.prefab;
        parent = _parent;
    }
    /// <summary>
    /// Starts the animation and Invokes Cast after the spell cd
    /// </summary>
    public void Fire()
    {
        // Security check
        if (data.animationName.Length > 0)
            data.mageRef.animator.SetTrigger(data.animationName[Random.Range(0, data.animationName.Length)]);
        Invoke("Cast", data.secondsToCast);
    }
    /// <summary>
    /// If there is any selfcast spell of the same type dstroys it before spawning the new one
    /// </summary>
    private void Cast()
    {
        // Destroys the same type selfcastTypes
        for (int i = 0; i < parent.childCount; i++)
        {
            if (parent.GetChild(i).GetComponent<Spell>() && parent.GetChild(i).GetComponent<Spell>().data.name == data.name)
            {
                Destroy(parent.GetChild(i).gameObject);
            }
        }
        // Spawns and adds effects to the prefab
        GameObject clone = Instantiate(prefab, parent);
        AddEffects(clone);
    }
}
