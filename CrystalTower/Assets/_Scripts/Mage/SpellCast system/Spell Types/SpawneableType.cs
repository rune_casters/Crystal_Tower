﻿using UnityEngine;
/// <summary>
/// Functionalities for the Spawneable spell type
/// </summary>
public class SpawneableType : Spell
{
    /// <summary>
    /// The position of the spell
    /// </summary>
    private Vector3 position;
    /// <summary>
    /// The rotation on the y axis for the spell
    /// </summary>
    private float yRotation;
    /// <summary>
    /// The transform that spawns the spell
    /// </summary>
    private Transform trans;

    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_spellRef"></param>
    /// <param name="_transform"></param>
    public void Initialize(SpellData _spellRef, Transform _transform)
    {
        data = _spellRef;
        prefab = _spellRef.prefab;
        trans = _transform;
    }
    /// <summary>
    /// Starts the animation after the spell delay and sets the spawn position
    /// </summary>
    /// <param name="pos"></param>
    public void Fire(Vector3 pos = default)
    {
        position = pos;
        if (data.animationName.Length > 0)
            data.mageRef.animator.SetTrigger(data.animationName[Random.Range(0, data.animationName.Length)]);
        Invoke("Cast", data.secondsToCast);
    }

    /// <summary>
    /// Spawns the spell on the position with the rotation and adds the effects
    /// </summary>
    private void Cast()
    {
        GameObject clone = Instantiate(prefab);
        if (trans)
        {
            position = trans.position;
            yRotation = trans.rotation.eulerAngles.y;
        }
        clone.transform.rotation = Quaternion.Euler(0, yRotation, 0);
        clone.transform.position = position;
        AddEffects(clone);
    }


}