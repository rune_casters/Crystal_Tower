﻿using UnityEngine;
/// <summary>
/// Projectile spell functionalities
/// </summary>
public class ProjectileType : Spell
{
    /// <summary>
    /// The shootpoints from where the projectile can be shot
    /// </summary>
    private Transform[] shootPoint;
    /// <summary>
    /// The force of the projectile
    /// </summary>
    private float force;
    /// <summary>
    /// the next shootpoint
    /// </summary>
    private static int nextShootPoint = 0;

    /// <summary>
    /// Initilizes all the variables
    /// </summary>
    /// <param name="_spellRef"></param>
    /// <param name="_force"></param>
    /// <param name="_shootPoint"></param>
    public void Initialize(SpellData _spellRef,float _force, Transform[] _shootPoint)
    {
        data = _spellRef;
        prefab = _spellRef.prefab;
        shootPoint = _shootPoint;
        force = _force;
    }

    /// <summary>
    /// Gets the next shootpoint, starts the animation and Invokes Cast after the spell delay
    /// </summary>
    public void Fire()
    {
        // Security check
        if (data.animationName[0].Length != 0)
        {
            nextShootPoint = ++nextShootPoint % data.animationName.Length;
            data.mageRef.animator.SetTrigger(data.animationName[nextShootPoint]);
        }
        Invoke("Cast", data.secondsToCast);
        
    }
    /// <summary>
    /// Spawns the prefab, adds the effects and lauches it
    /// </summary>
    private void Cast()
    {
        // Spawn the prefab and set it on the correct shootpoint position
        GameObject clone = Instantiate(prefab) as GameObject;
        if(shootPoint[nextShootPoint % 2] != null) clone.transform.position = shootPoint[nextShootPoint % 2].position;
        // Get the rigidbody
        Rigidbody clonerb = clone.GetComponent<Rigidbody>();
        clonerb.velocity = Vector3.zero;
        // Add the effect
        AddEffects(clone);

        // Get the direction
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f));
        RaycastHit hit;
        Vector3 lookPoint;
        if (Physics.Raycast(ray, out hit, 200, spellLayerMask))
            if (Vector3.Distance(Vector3.ProjectOnPlane(transform.position, Vector3.up), Vector3.ProjectOnPlane(hit.point, Vector3.up)) < 3)
                lookPoint = ray.origin + (ray.direction * 5);
            else
                lookPoint = hit.point;
        else
            lookPoint = ray.origin + (ray.direction * 100);

        // Impulse the projectile to the target
        clone.transform.LookAt(lookPoint);
        clonerb.AddForce(clone.transform.forward * force);
    }

}
