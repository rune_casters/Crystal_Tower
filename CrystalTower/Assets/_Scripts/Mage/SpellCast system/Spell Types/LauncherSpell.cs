﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Launches varioues projectiles with their own effects
/// </summary>
[CreateAssetMenu(menuName =("Ability/Launcher"))]
public class LauncherSpell : SpellData
{
    /// <summary>
    /// Number of projectiles to launch
    /// </summary>
    [Header("Projectile things")]
    public int proyectileCount;
    /// <summary>
    /// The max angle to spread the projectiles
    /// </summary>
    public int angle;
    /// <summary>
    /// The force of the projectiles
    /// </summary>
    public int force;
    /// <summary>
    /// The effects of the projectiles
    /// </summary>
    [Header("Projectile Effects")]
    public List<EffectData> projectileEffects;
    /// <summary>
    /// The functionality reference
    /// </summary>
    private LauncherType lt;

    /// <summary>
    /// Call launch on the functionalities
    /// </summary>
    /// <param name="pos"></param>
    public override void Cast(Vector3 pos = default)
    {
        lt.Launch();
    }

    /// <summary>
    /// Gets the reference from the prefab and initializes it
    /// </summary>
    /// <param name="shootPoint"></param>
    public override void Initialize(Transform[] shootPoint = null)
    {
        lt = prefab.GetComponent<LauncherType>();
        lt.Initialize(this, proyectileCount,angle,force, shootPoint[2], projectileEffects);
    }
}
