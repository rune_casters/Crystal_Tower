﻿using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Launcher spell functionalities
/// </summary>
public class LauncherType : Spell
{
    /// <summary>
    /// Number of projectiles to launch
    /// </summary>
    public int projectileCount;
    /// <summary>
    /// The max angle to spread the projectiles
    /// </summary>
    public int angle;
    /// <summary>
    /// The force of the projectiles
    /// </summary>
    public int force;
    /// <summary>
    /// The effects of the projectiles
    /// </summary>
    public List<EffectData> projectileEffects;
    /// <summary>
    /// The launcher shootPoint
    /// </summary>
    private Transform shootPoint;

    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_spellRef"></param>
    /// <param name="_projectileCount"></param>
    /// <param name="_angle"></param>
    /// <param name="_force"></param>
    /// <param name="_shootPoint"></param>
    /// <param name="_projectileEffects"></param>
    public void Initialize(SpellData _spellRef, int _projectileCount, int _angle, int _force, Transform _shootPoint, List<EffectData> _projectileEffects)
    {
        data = _spellRef;
        prefab = _spellRef.prefab;
        projectileCount = _projectileCount;
        angle = _angle;
        force = _force;
        shootPoint = _shootPoint;
        projectileEffects = _projectileEffects;
    }
    /// <summary>
    /// Starts the animatin and Invokes Cast after the spell delay
    /// </summary>
    public void Launch()
    {
        data.mageRef.animator.SetTrigger(data.animationName[Random.Range(0, data.animationName.Length)]);
        Invoke("Cast", data.secondsToCast);
    }

    /// <summary>
    /// Adds effects to the launcher, lauches the projectiles and adds their effects
    /// </summary>
    private void Cast()
    {
        // Instantiate the launcher and add effects
        GameObject launcher = Instantiate(prefab, shootPoint.parent);
        AddEffects(launcher);

        // Spawn all the projectiles give them their rotation and add the effects
        for (int i = 0; i < projectileCount; i++)
        {
            GameObject clone = Instantiate(prefab) as GameObject;
            Vector3 randomOffset = Random.insideUnitSphere / 2;
            clone.transform.position = new Vector3(shootPoint.position.x - (-0.005f + i * (0.01f / (projectileCount - 1))), shootPoint.position.y, shootPoint.position.z) + randomOffset;
            clone.transform.rotation = shootPoint.rotation;
            foreach (EffectData eff in projectileEffects)
            {
                eff.DoEffect(clone);
            }

            Rigidbody clonerb = clone.GetComponent<Rigidbody>();
            clonerb.velocity = Vector3.zero;

            //launch the ball
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f));
            RaycastHit hit;
            Vector3 lookPoint;
            if (Physics.Raycast(ray, out hit, 200, spellLayerMask))
                if (Vector3.Distance(Vector3.ProjectOnPlane(transform.position, Vector3.up), Vector3.ProjectOnPlane(hit.point, Vector3.up)) < 3)
                    lookPoint = ray.origin + (ray.direction * 5);
                else
                    lookPoint = hit.point;
            else
                lookPoint = ray.origin + (ray.direction * 100);

            // Impulse the projectile to the target
            clone.transform.Rotate(new Vector3(Random.Range(-angle / 2.5f, angle / 2.5f), -angle / 2 + angle / (projectileCount - 1) * i, 0));
            clonerb.AddForce(clone.transform.forward * force);
        }
    }
}
