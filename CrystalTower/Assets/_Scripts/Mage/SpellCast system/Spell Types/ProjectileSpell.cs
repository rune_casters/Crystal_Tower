﻿using UnityEngine;
/// <summary>
/// Projectile spell data
/// </summary>
[CreateAssetMenu(menuName ="Ability/Projectile")]
public class ProjectileSpell : SpellData
{
    /// <summary>
    /// The projectile force
    /// </summary>
    [Header("Projectile Settings")]
    public float force;
    /// <summary>
    /// Reference to the projectile functionalities
    /// </summary>
    private ProjectileType pt;

    /// <summary>
    /// Calls to the ProjectileType Fire
    /// </summary>
    /// <param name="pos"></param>
    public override void Cast(Vector3 pos = default)
    {
        pt.Fire();
    }

    /// <summary>
    /// Gets the reference from the prefab and initializes it
    /// </summary>
    /// <param name="shootPoint"></param>
    public override void Initialize(Transform[] shootPoint)
    {
        pt = prefab.GetComponent<ProjectileType>();
        pt.Initialize(this,force,shootPoint);
    }

}
