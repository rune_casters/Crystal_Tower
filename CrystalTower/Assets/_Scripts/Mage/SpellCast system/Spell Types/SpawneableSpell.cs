﻿using UnityEngine;
/// <summary>
/// Spawneable spell data
/// </summary>
[CreateAssetMenu(menuName = "Ability/Spawneable")]
public class SpawneableSpell : SpellData
{
    /// <summary>
    /// Reference to the functionalities
    /// </summary>
    private SpawneableType st;
    /// <summary>
    /// Calls SpawneableType Fire giving the position
    /// </summary>
    /// <param name="pos"></param>
    public override void Cast(Vector3 pos = default)
    {
        st.Fire(pos);
    }

    /// <summary>
    /// Gets the reference from the prefab and initializes it
    /// </summary>
    /// <param name="shootPoint"></param>
    public override void Initialize(Transform[] shootPoint = null)
    {
        st = prefab.GetComponent<SpawneableType>();
        st.Initialize(this,shootPoint[0]);
    }
}
