﻿using UnityEngine;
/// <summary>
/// Self Cast Spell dara
/// </summary>
[CreateAssetMenu(menuName = "Ability/SelfCast")]
public class SelfCastSpell : SpellData
{
    /// <summary>
    /// Reference to the functionality
    /// </summary>
    private SelfCastType sct;

    /// <summary>
    /// Calls Fire from SelfCastType
    /// </summary>
    /// <param name="pos"></param>
    public override void Cast(Vector3 pos = default)
    {
        sct.Fire();
    }
    /// <summary>
    /// Gets the reference from the prefab and initializes it
    /// </summary>
    /// <param name="shootPoint"></param>
    public override void Initialize(Transform[] shootPoint)
    {
        sct = prefab.GetComponent<SelfCastType>();
        sct.Initialize(this,shootPoint[2].parent);
    }
}
