﻿using UnityEngine;
/// <summary>
/// Insta cast spell functionality
/// </summary>
public class InstaCastType : Spell
{
    /// <summary>
    /// The position to spawn the spell
    /// </summary>
    private Vector3 position;
    /// <summary>
    /// initializes the variables
    /// </summary>
    /// <param name="_spellRef">the insta cast spell data</param>
    public void Initialize(SpellData _spellRef)
    {
        data = _spellRef;
        prefab = _spellRef.prefab;
    }

    /// <summary>
    /// Sets up the spawn position, starts the animation and invokes Cast afeter the spell delay
    /// </summary>
    /// <param name="_target"></param>
    public void Spawn(Vector3 _position)
    {
        position = _position;
        data.mageRef.animator.SetTrigger(data.animationName[Random.Range(0, data.animationName.Length)]);
        Invoke("Cast", data.secondsToCast);
    }

    /// <summary>
    /// Spawns the prefab on the position and adds the effects
    /// </summary>
    private void Cast()
    {
        GameObject clone = Instantiate(prefab) as GameObject;
        clone.transform.position = position;
        AddEffects(clone);
    }

    
}
