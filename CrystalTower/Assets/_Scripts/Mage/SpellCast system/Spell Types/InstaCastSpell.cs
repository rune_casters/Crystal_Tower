﻿using UnityEngine;
/// <summary>
/// Instant cast type data
/// </summary>
[CreateAssetMenu(menuName = "Ability/InstaCast")]
public class InstaCastSpell : SpellData
{
    /// <summary>
    /// The layerMask to know where can be castes the spells
    /// </summary>
    public LayerMask abilityLayerMask;
    /// <summary>
    /// Reference to the functionalities
    /// </summary>
    private InstaCastType ICT;
    /// <summary>
    /// Gets the position and calls the functionality spawn with the position
    /// </summary>
    /// <param name="pos"></param>
    public override void Cast(Vector3 pos = default)
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 200, abilityLayerMask))
        {
            if (hit.transform.CompareTag("Enemy"))
            {
                ICT.Spawn(hit.point);
            }
        }
    }
    /// <summary>
    /// Gets the functionality from the prefab and initializes it
    /// </summary>
    /// <param name="shootPoint"></param>
    public override void Initialize(Transform[] shootPoint = null)
    {
        ICT = prefab.GetComponent<InstaCastType>();
        ICT.Initialize(this);
    }
    
}
