﻿using UnityEngine;

/// <summary>
/// GrenadeSpell functionalities with a monobehaviour
/// </summary>
public class GrenadeType : Spell
{
    /// <summary>
    /// The force of the greande
    /// </summary>
    private float force;
    /// <summary>
    /// the angle of the grenade
    /// </summary>
    private float angle;
    /// <summary>
    /// The next shootPoint from where the grenade is shot
    /// </summary>
    private int nextShootPoint;
    /// <summary>
    /// The shootPoints from where the grenades are shot
    /// </summary>
    private Transform[] shootPoint;

    /// <summary>
    /// Initializes all the variables
    /// </summary>
    /// <param name="_spellRef">The reference to the spell data</param>
    /// <param name="_force">The force of the grenade</param>
    /// <param name="_angle"> The angle of the grenade</param>
    /// <param name="_shootPoint"> The shootPoints from where the grenades are shot </param>
    public void Initialize(SpellData _spellRef, float _force, float _angle, Transform[] _shootPoint)
    {
        data = _spellRef;
        prefab = _spellRef.prefab;
        force = _force;
        angle = _angle;
        shootPoint = _shootPoint;
        nextShootPoint = 0;
    }
    /// <summary>
    /// Plays the spell animation sets the correct shootpoint and invokes the spawn after the delay of the spell
    /// </summary>
    public void Fire()
    {
        nextShootPoint = ++nextShootPoint % 2;
        data.mageRef.animator.SetTrigger(data.animationName[nextShootPoint]);
        Invoke("Cast", data.secondsToCast);
    }

    /// <summary>
    /// Spawns the grenade and adds the effects
    /// </summary>
    private void Cast()
    {
        // Spawn the grenade and set the position on the correct shootpoint
        GameObject clone = Instantiate(prefab) as GameObject;
        clone.transform.position = shootPoint[nextShootPoint].position;
        // Get the grenade rigidbody and reset its velocity
        Rigidbody clonerb = clone.GetComponent<Rigidbody>();
        clonerb.velocity = Vector3.zero;
        // Add the effects to the grenade
        AddEffects(clone);

        // Get the impact position and launch the grenade
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f));
        RaycastHit hit;
        Vector3 lookPoint;
        if (Physics.Raycast(ray, out hit, 200, spellLayerMask))
            if (Vector3.Distance(Vector3.ProjectOnPlane(transform.position, Vector3.up), Vector3.ProjectOnPlane(hit.point, Vector3.up)) < 3)
                lookPoint = ray.origin + (ray.direction * 5);
            else
                lookPoint = hit.point;
        else
            lookPoint = ray.origin + (ray.direction * 100);

        // Impulse the greande to the target
        clone.transform.LookAt(lookPoint);
        clone.transform.Rotate(angle, 0, 0);
        clonerb.AddForce(clone.transform.forward * force);
    }
}
