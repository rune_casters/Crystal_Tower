﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Custom game event container used for a dictionary
/// </summary>
[System.Serializable]
public class EventStringDictionary
{
    public string name;
    public GameEvent value;
}

/// <summary>
/// Stores Player stats and events
/// </summary>
[CreateAssetMenu(menuName = "Mage/Stats")]
public class Stats : ScriptableObject
{
    /// <summary>
    /// Stat class container used to ease manipulation
    /// </summary>
    [System.Serializable]
    public class Stat
    {
        /// <summary>
        /// Current stat value
        /// </summary>
        [SerializeField]
        private float currentValue;
        /// <summary>
        /// Stat value setter and getter
        /// </summary>
        public float CurrentValue
        {
            get { return currentValue; }
            set
            {
                // Clamp if clampable stat
                if (clamped) currentValue = Mathf.Clamp(value, 0.0f, maxValue);
                else currentValue = value;

                // Raise change if event exists
                if (onChangedEvent)
                    onChangedEvent.Raise();
            }
        }

        /// <summary>
        /// Resets stat to base value
        /// </summary>
        public void Reset()
        {
            CurrentValue = maxValue = baseValue;
            multiplicator = 1;
        }

        /// <summary>
        /// Base value 
        /// </summary>
        public float baseValue;
        /// <summary>
        /// Max value
        /// </summary>
        public float maxValue;
        /// <summary>
        /// Multiplicator used for stats purposes
        /// </summary>
        public float multiplicator = 1;
        /// <summary>
        /// If the stat is clamped
        /// </summary>
        public bool clamped;
        /// <summary>
        /// Event raised when stat has been changed
        /// </summary>
        public GameEvent onChangedEvent;
    }

    /// <summary>
    /// Player health stat
    /// </summary>
    [Header("Stats")]
    public Stat health;

    /// <summary>
    /// Player damage stat
    /// </summary>
    public Stat damage;

    /// <summary>
    /// Player speed stat
    /// </summary>
    public Stat speed;

    /// <summary>
    /// Player shield stat
    /// </summary>
    public Stat shield;

    /// <summary>
    /// Player damage received stat
    /// </summary>
    public Stat damageReceivedMultiplicator;

    /// <summary>
    /// Player max jumps stat
    /// </summary>
    public Stat maxJumps;

    /// <summary>
    /// Player animator reference
    /// </summary>
    [HideInInspector]
    public Animator animator;

    /// <summary>
    /// Transform reference
    /// </summary>
    [HideInInspector]
    public Transform playerTransform;

    /// <summary>
    /// Serialized player events dictionary
    /// </summary>
    [Header("Events")]
    public EventStringDictionary[] playerEventsDictionary;

    /// <summary>
    /// Current player events dictionary
    /// </summary>
    [HideInInspector]public Dictionary<string,GameEvent> playerEvents;

    #region PUBLIC ACCESORS / USE THESE

    /// <summary>
    /// Heal player with given value and raise heal event
    /// </summary>
    public void Heal(float value)
    {
        health.CurrentValue += value;
        playerEvents["Heal"].Raise();
    }

    /// <summary>
    /// Heal player with given percent value and raise heal event
    /// </summary>
    public void HealPercentage(float value)
    {
        health.CurrentValue += health.maxValue * value / 100f;
        playerEvents["Heal"].Raise();
    }

    /// <summary>
    /// Hit player with given value, raise hit event, check shield and death
    /// </summary>
    public void Hit(float value)
    {
        // Calculate total damage and raise hit event
        float totalDamage = value * damageReceivedMultiplicator.CurrentValue;
        playerEvents["Hit"].Raise();

        // Check shield
        if (shield.CurrentValue > totalDamage)
        {
            shield.CurrentValue -= totalDamage;
            //raise shield hit
        }
        // If no shield 
        else
        {
            // Remove health
            totalDamage -= shield.CurrentValue;
            shield.CurrentValue = 0;
            health.CurrentValue -= totalDamage;
            // Check if dead
            if (health.CurrentValue == 0)
                Kill();
        }
    }

    /// <summary>
    /// Adds Shield with given value 
    /// </summary>
    /// <param name="value"></param>
    /// <param name="time"></param>
    public void GainShield(float value, float time)
    {
        shield.CurrentValue += value;
        // RAISE SHIELD GAIN EVENT
        // ADD DECAY OR REMOVE SHIELD AFTER TIME
    }

    /// <summary>
    /// Destroys player's shield
    /// </summary>
    public void DestroyShield()
    {
        shield.CurrentValue = 0;
    }

    /// <summary>
    /// Kill player
    /// </summary>
    public void Kill()
    {
        // Raises death check event (some trinkets revive)
        playerEvents["DeathCheck"].Raise();

        // Check real death
        if (health.CurrentValue <= 0)
            playerEvents["Death"].Raise();
    }

    /// <summary>
    /// Reset every stat to the base value
    /// </summary>
    public void Reset()
    {
        health.Reset();
        shield.Reset();
        damage.Reset();
        speed.Reset();
        maxJumps.Reset();
        damageReceivedMultiplicator.Reset();
    }

    /// <summary>
    /// Add maximum health to the player with the given value and raise event
    /// </summary>
    public void AddMaximumHealth(float value)
    {
        health.maxValue += value;
        health.onChangedEvent.Raise();
    }

    /// <summary>
    /// Add damage to the player with the given value
    /// </summary>
    public void AddDamage(float value)
    {
        damage.CurrentValue += value;
    }

    /// <summary>
    /// Add speed to the player with the given value
    /// </summary>
    public void AddMaxSpeed(float value)
    {
        speed.maxValue += value;
        speed.CurrentValue = speed.maxValue;
    }

    /// <summary>
    /// Add maximum health to the player with the given value and raise event
    /// </summary>
    public void SetMaximumHealthPercentage(float value)
    {
        health.maxValue *= value / 100f;
        health.CurrentValue *= value / 100f;
    }

    /// <summary>
    /// Sets speed multiplicator of the player with the given value
    /// </summary>
    public void SetSpeedMultiplicator(float value)
    {
        speed.multiplicator = value;
    }

    /// <summary>
    /// Sets jump force multiplicator of the player with the given value
    /// </summary>
    public void SetJumpForceMultiplicator(float value)
    {
        maxJumps.multiplicator = value;
    }

    /// <summary>
    /// Sets damage multiplicator of the player with the given value
    /// </summary>
    public void SetDamageMultiplicator(float value)
    {
        damage.multiplicator = value;
    }

    /// <summary>
    /// Sets damage received multiplicator of the player with the given value
    /// </summary>
    public void SetDamageReceivedMultiplicator(float value)
    {
        damageReceivedMultiplicator.CurrentValue = value;
    }

    /// <summary>
    /// Adds jump stacks of the player with the given value
    /// </summary>
    public void AddJumpStacks(int value)
    {
        maxJumps.CurrentValue += value;
    }

    /// <summary>
    /// Copies event dictionary from serialized to real data
    /// </summary>
    public void CreateDictionary()
    {
        playerEvents = new Dictionary<string, GameEvent>();
        for (int i = 1; i < playerEventsDictionary.Length; i++)
            playerEvents.Add(playerEventsDictionary[i].name, playerEventsDictionary[i].value);
    }

    #endregion
}