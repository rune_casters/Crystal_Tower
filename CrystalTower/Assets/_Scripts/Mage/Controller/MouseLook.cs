using System;
using UnityEngine;

/// <summary>
/// Handles the player camera movement and mouse lock
/// </summary>
[Serializable]
public class MouseLook
{
    /// <summary>
    /// mouse sensitivity on X axis
    /// </summary>
    public float XSensitivity = 2f;
    /// <summary>
    /// mouse sensitivity on Y axis
    /// </summary>
    public float YSensitivity = 2f;
    /// <summary>
    /// Enables the vertical rotation
    /// </summary>
    public bool clampVerticalRotation = true;
    /// <summary>
    /// The minimum X rotationn for the camera
    /// </summary>
    public float MinimumX = -90F;
    /// <summary>
    /// The maximum X rotationn for the camera
    /// </summary>
    public float MaximumX = 90F;
    /// <summary>
    /// Enables the rotation with Slerp
    /// </summary>
    public bool smooth;
    /// <summary>
    /// The speed of the Slerp
    /// </summary>
    public float smoothTime = 5f;
    /// <summary>
    /// if the cursor is locked or not
    /// </summary>
    public bool lockCursor = true;

    /// <summary>
    /// The player rotation
    /// </summary>
    private Quaternion playerRotation;
    /// <summary>
    /// The player camera rotation
    /// </summary>
    private Quaternion cameraRoattion;

    /// <summary>
    /// Initializes both rotations
    /// </summary>
    /// <param name="character">The player</param>
    /// <param name="camera">the player camera</param>
    public void Init(Transform character, Transform camera)
    {
        playerRotation = character.localRotation;
        cameraRoattion = camera.localRotation;
    }

    /// <summary>
    /// Rotates the player and the player camera
    /// </summary>
    /// <param name="character"> The player</param>
    /// <param name="camera"> The player camera</param>
    public void LookRotation(Transform character, Transform camera)
    {
        // get the mouse Y rotation for the player
        float yRot = Input.GetAxis("Mouse X") + Input.GetAxis("Mouse X Joystick") * XSensitivity;
        // get the mouse X rotation for the camera
        float xRot = Input.GetAxis("Mouse Y") + Input.GetAxis("Mouse Y Joystick") * YSensitivity;

        // set the desired rotations
        playerRotation *= Quaternion.Euler (0f, yRot, 0f);
        cameraRoattion *= Quaternion.Euler (-xRot, 0f, 0f);

        // if the vertical rotation is clamped clamp it
        if(clampVerticalRotation)
            cameraRoattion = ClampRotationAroundXAxis (cameraRoattion);

        // if the user want a smooth movement 
        if(smooth)
        {
            // lerp both rotations to the desired rotation by the smooth speed
            character.localRotation = Quaternion.Slerp (character.localRotation, playerRotation,
                smoothTime * Time.deltaTime);
            camera.localRotation = Quaternion.Slerp (camera.localRotation, cameraRoattion,
                smoothTime * Time.deltaTime);
        }
        else
        {
            // stablish the new rotation
            character.localRotation = playerRotation;
            camera.localRotation = cameraRoattion;
        }
        
    }

    
    /// <summary>
    /// Sets the cursos lock
    /// </summary>
    /// <param name="value">-true locks the cursor -false unlocks the cursor</param>
    public void SetCursorLock(bool value)
    {
        lockCursor = value;

        if(!lockCursor)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
    
    /// <summary>
    /// Clamps the vertical rotation
    /// </summary>
    /// <param name="q"> the rotation to clamp</param>
    /// <returns> the clamped rotation</returns>
    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x);

        angleX = Mathf.Clamp (angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }

}

