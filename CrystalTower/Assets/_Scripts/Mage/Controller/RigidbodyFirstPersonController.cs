using UnityEngine;

/// <summary>
/// Player movement controller
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class RigidbodyFirstPersonController : MonoBehaviour
{
    /// <summary>
    /// Reference to the player stats
    /// </summary>
    [SerializeField] private Stats mageRef;
    /// <summary>
    /// The force of the jump
    /// </summary>
    [SerializeField] private float jumpForce;
    /// <summary>
    /// manages the maximum slope the player can go through
    /// </summary>
    [SerializeField] private AnimationCurve slopeModifier;
    /// <summary>
    /// distance for checking if the controller is grounded
    /// </summary>
    [SerializeField] private float groundCheckDistance; 
    
    /// <summary>
    /// layers to ignore
    /// </summary>
    [SerializeField] private LayerMask groundCheckLayerMask; 
    /// <summary>
    /// The player camera
    /// </summary>
    [SerializeField] private Camera cam;
    /// <summary>
    /// MouseLook for the camera rotation
    /// </summary>
    public MouseLook mouseLook = new MouseLook();

    /// <summary>
    /// The player rigidbody
    /// </summary>
    private Rigidbody rb;
    /// <summary>
    /// The player capsule collider
    /// </summary>
    private CapsuleCollider capsule;
    /// <summary>
    /// The normal of the ground
    /// </summary>
    private Vector3 groundContactNormal;
    /// <summary>
    /// helper booleans to know if the player just landed
    /// </summary>
    private bool jumpPressed, previouslyGrounded;
    /// <summary>
    /// if the player is allowed to move
    /// </summary>
    private bool movementEnabled = true;
    /// <summary>
    /// the number of jumps done by the player
    /// </summary>
    private int jumpsDone = 0;
    /// <summary>
    /// if the player is dashing
    /// </summary>
    private bool dashing;
    /// <summary>
    /// the last direction the player had
    /// </summary>
    private Vector3 lastForward;
    /// <summary>
    /// the position where the player starts on the run
    /// </summary>
    public Vector3 startingDungeonPos;
    /// <summary>
    /// lets the player rotate the view
    /// </summary>
    [HideInInspector] public bool rotateView = true;
    /// <summary>
    /// Raises when the player jumps
    /// </summary>
    public GameEvent jumpEvent; 
    /// <summary>
    /// if the player is grounded or not
    /// </summary>
    public bool Grounded { get; set; }
    /// <summary>
    /// if the player is jumping or not
    /// </summary>
    public bool Jumping { get; private set; }

    /// <summary>
    /// Initialize all the variables
    /// </summary>
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        capsule = GetComponent<CapsuleCollider>();
        mouseLook.Init(transform, cam.transform);
        slopeModifier = new AnimationCurve(new Keyframe(-90.0f, 1.0f), // from -90 grades
        new Keyframe(45.0f, 1.0f), // to 45 grades there is no speed penalty for slopes
        new Keyframe(46.0f, 0.0f), // if the angle is >= 46 u cant go through that slope
        new Keyframe(90.0f, 0.0f));
    }

    /// <summary>
    /// Checks the mouse and jump imput
    /// </summary>
    private void Update()
    {
        if (!GameState.instance.GamePaused && movementEnabled)
        {
            if (mouseLook.lockCursor && rotateView)
                RotateView();
            if (Input.GetButtonDown("Jump") && jumpsDone < mageRef.maxJumps.CurrentValue)
            {
                jumpEvent.Raise();
                ++jumpsDone;
                jumpPressed = true;
            }
        }
    }

    /// <summary>
    /// Handles the player movement
    /// </summary>
    private void FixedUpdate()
    {
        // if the player can move
        if (movementEnabled)
        {
            // check if the player is grounded
            GroundCheck();
            // get the inputs
            Vector2 input = GetInput();
            // reset where the player wants to go
            Vector3 desiredDirection = Vector3.zero;
            // if the input isnt 0 or if the player is dashing
            if ((Mathf.Abs(input.x) > float.Epsilon || Mathf.Abs(input.y) > float.Epsilon) || dashing)
            {
                // Set the walking animations
                mageRef.animator.SetBool("Walking", true);
                // if isnt dashing 
                if (!dashing) 
                {
                    // get the desired move from the inputs
                    desiredDirection = transform.forward * input.y + transform.right * input.x;
                    lastForward = desiredDirection;
                }
                // if its dashing
                else 
                {
                    // the desired move was the last direction the player had before pressing the dash button
                    desiredDirection = lastForward;
                }
                // Project on the ground plane the desired move and normalize it
                desiredDirection = Vector3.ProjectOnPlane(desiredDirection, groundContactNormal).normalized;
                // Add the speed on the desired move direction
                desiredDirection *= mageRef.speed.CurrentValue * mageRef.speed.multiplicator;
                // If the rb speed is less than the player max speed or the player is dashig add force to the player rigidbody to the desired direction
                if (rb.velocity.magnitude * rb.velocity.magnitude < mageRef.speed.maxValue * mageRef.speed.maxValue || dashing) rb.AddForce(desiredDirection * SlopeMultiplier(), ForceMode.Impulse);
            }
            // If the player is on the air and isnt dashing apply the gravity
            if (!Grounded && !dashing) rb.AddForce(Physics.gravity * rb.mass * rb.mass * Time.deltaTime * 5); 
            // if the player is grounded 
            if (Grounded)
            {
                // if there is no input the speed is low and the player isnt jumping neither dashing stop the player
                if (Mathf.Abs(input.x) < float.Epsilon && Mathf.Abs(input.y) < float.Epsilon &&                            
                                                                   rb.velocity.magnitude < 5 &&                            
                                                                                    !Jumping &&                            
                                                                                    !dashing)
                {
                    rb.Sleep();                  
                    mageRef.animator.SetBool("Walking", false);
                }
            }
            // if there are jumps left and the jump button has been pressed
            if (jumpsDone <= mageRef.maxJumps.CurrentValue && jumpPressed)
            {
                // change the player mass to create a more realistic jump
                rb.mass = 25.0f;
                // slow the target to create the wind friction on the first jump
                if (!dashing && jumpsDone == 1) mageRef.speed.CurrentValue *= 0.8f; 
                // get the direction of the jump to be able to change the jump direction in mid air
                if(desiredDirection != Vector3.zero)
                {
                    // Get the rb.velocity normalized on plane
                    Vector3 rb2D = Vector3.ProjectOnPlane(rb.velocity, groundContactNormal);
                    // Get radians between both vector by calculating its angle with x axis
                    float angle = Mathf.Atan2(desiredDirection.x, desiredDirection.z) - Mathf.Atan2(rb2D.x, rb2D.z);
                    // Convert from radians to grades
                    angle *= 180 / Mathf.PI;
                    // Get the rotation on Quaternion
                    Quaternion velRotation = Quaternion.AngleAxis(angle, groundContactNormal);    
                    // change the direction of the velocity 
                    rb.velocity = velRotation * rb.velocity;
                }
                // Get the velocity on the xz plane
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
                // Add force on the y axis
                rb.AddForce(new Vector3(0f, jumpForce * mageRef.maxJumps.multiplicator, 0f), ForceMode.Impulse);
                // set jumping to true
                Jumping = true;
            }
            // set jump buttom pressed to false
            jumpPressed = false;
        }
    }

    /// <summary>
    /// Allows or disables player movement
    /// </summary>
    /// <param name="state">true to allow the player movement and false to diable it</param>
    public void SetMovementState(bool state)
    {
        movementEnabled = state;
    }

    /// <summary>
    /// Get the slope multiplier from the animation curve
    /// </summary>
    /// <returns></returns>
    private float SlopeMultiplier()
    {
        // get the angle between the ground and the y axis
        float angle = Vector3.Angle(groundContactNormal, Vector3.up);
        // return the value of the angle on the animation curve
        return slopeModifier.Evaluate(angle);
    }

    /// <summary>
    /// Sets the player on the starting position on the game
    /// </summary>
    public void ResetToInitialPosition()
    {
        transform.position = startingDungeonPos;
    }

    /// <summary>
    /// Get the horizontal and vertical input values
    /// </summary>
    /// <returns> The horizontal and veritcal input values on a vector2</returns>
    private Vector2 GetInput()
    {

        Vector2 input = new Vector2
        {
            x = Input.GetAxis("Horizontal") + Input.GetAxis("Horizontal Joystick"),
            y = Input.GetAxis("Vertical") + Input.GetAxis("Vertical Joystick")
        };
        return input;
    }

    /// <summary>
    /// Initializes the mouse look
    /// </summary>
    public void ReInitMouseLook()
    {
        mouseLook.Init(transform, cam.transform);
    }
    /// <summary>
    /// Rotates the rigidbody velocity to match the mouse look
    /// </summary>
    private void RotateView()
    {
        // avoids the mouse looking if the game is paused
        if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;

        // get the rotation before it's changed
        float oldYRotation = transform.eulerAngles.y;

        // update the camera rotation
        mouseLook.LookRotation(transform, cam.transform);

        // if its grounded and isnt dashing rotate the rigidbody velocity to match the new direction that the character is looking
        if (Grounded && !dashing)
        {
            Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
            rb.velocity = velRotation * rb.velocity;
        }
    }

    /// <summary>
    /// Sphere cast down to see if the player is touching the ground
    /// </summary>
    private void GroundCheck()
    {
        // cache the player last state
        previouslyGrounded = Grounded;
        RaycastHit hitInfo;
        // sphere cast down with the capsule radius
        if (Physics.SphereCast(transform.position, capsule.radius, Vector3.down, out hitInfo,
        ((capsule.height / 2f) - capsule.radius) + groundCheckDistance, groundCheckLayerMask, QueryTriggerInteraction.Ignore))
        {
            // if the sphere hit the ground and the ground has less than 45� get the ground contact normal and set grounded to true
            if (hitInfo.transform.CompareTag("Ground") && Vector3.Angle(hitInfo.normal,Vector3.up) <= 45)
            {
                    Grounded = true;
                    groundContactNormal = hitInfo.normal;
            }
        }
        // if the sphere cast doesnt hit the player is in the air
        else
        {
            // set the rigidbody values for when the player isnt grounded
            rb.drag = 0f;
            rb.mass = 25.0f;
            Grounded = false;
            groundContactNormal = Vector3.up;
        }
        // if the player wasnt grounded and now it is
        if (!previouslyGrounded && Grounded)
        {
            // set the rigidbody values for when the player is grounded
            rb.drag = 5f;
            rb.mass = 10f;
            jumpsDone = 0;
            Jumping = false;
            rb.velocity = Vector3.ProjectOnPlane(rb.velocity, groundContactNormal);
            mageRef.speed.CurrentValue = mageRef.speed.maxValue;
        }
    }

    /// <summary>
    /// starts the dash of the player
    /// </summary>
    /// <param name="_speed"> The speed of the dash</param>
    public void Dash(float _speed)
    {
        // reset the speed
        rb.velocity = Vector3.zero;
        // set the dash bool
        dashing = true;
        // set the speed to the dashing speed
        mageRef.speed.CurrentValue = _speed;
        // reset the values after .5s
        Invoke("DisableDash", 0.5f);
    }
    /// <summary>
    /// Resets the values after the dash
    /// </summary>
    private void DisableDash()
    {
        mageRef.speed.CurrentValue = mageRef.speed.maxValue;
        dashing = false;
        rb.velocity *= 0.2f;
    }

    /// <summary>
    /// sets the mouseLook cursor lock
    /// </summary>
    /// <param name="value">-true locks the cursos -false unlocks it</param>
    public void SetCursorLock(bool value)
    {
        mouseLook.SetCursorLock(value);
    }
    /// <summary>
    /// Adds force to the player for knockbacks
    /// </summary>
    /// <param name="force"></param>
    public void AddForce(Vector3 force)
    {
        rb.velocity = Vector3.zero;
        if (Grounded && cam.transform.localRotation.eulerAngles.x > 10 && cam.transform.localRotation.eulerAngles.x < 315)
            rb.mass = 25; 
        rb.AddForce(force);
    }

}
