﻿using UnityEngine;

/// <summary>
/// Handles Flask that regenerates health (removed from the game so I removed comments)
/// </summary>
[CreateAssetMenu(menuName = "Mage/Flask")]
public class Flask : ScriptableObject
{
    public Stats mageStats;

    [Header("Charges")]
    [Range(0, 10)]
    public int      maxCharges = 3;
    [Range(0, 10)]
    public int      currentCharges = 3;
    public GameEvent onChargeChanged;

    [Space(5)]
    [Header("Healing")]
    [Range(0, 1000)]
    public float    baseHealingValue = 50;
    [Range(0, 1000)]
    public float    currentHealingValue = 50;
    [Range(20, 300)]
    public float    percentageMultiplicator = 100;
    public GameEvent onFlaskUsed;

    [Space(5)]
    [Header("Orbs")]
    [Range(1, 100)]
    public float    chargePerOrb = 3;
    [Range(0, 30)]
    public float    randomInterval = 2;
    public float    currentOrbCharge = 0;
    [Range(1, 100)]
    public float    maxOrbCharge = 100;
    public GameEvent onOrbChargeChanged;

    public void UseFlask()
    {
        if (currentCharges-- > 0)
        {
            if (currentOrbCharge >= maxOrbCharge)
            {
                currentOrbCharge = 0;
                currentCharges++;
                onOrbChargeChanged.Raise();
            }

            mageStats.Heal(currentHealingValue * (percentageMultiplicator / 100));
            onChargeChanged.Raise();
            onFlaskUsed.Raise();
        }
    }

    public void AddOrbs(int quantity)
    {
        currentOrbCharge = quantity + Mathf.Clamp(currentOrbCharge + Random.Range(chargePerOrb - randomInterval, chargePerOrb + randomInterval), 0, maxOrbCharge);

        if (currentOrbCharge >= maxOrbCharge && currentCharges < maxCharges)
        {
            currentOrbCharge = 0;
            currentCharges++;
            onChargeChanged.Raise();
        }

        onOrbChargeChanged.Raise();
    }

    public void Reset()
    {
        currentCharges = maxCharges;
        currentOrbCharge = 10;
        onChargeChanged.Raise();
        onOrbChargeChanged.Raise();
    }

    public void AddFlaskCharge(int value)
    {
        currentCharges = maxCharges += value;
        onChargeChanged.Raise();
    }

    public void AddOrbCharge(int value)
    {
        chargePerOrb += value;
    }

}
