﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles player's loot collection
/// </summary>
public class LootCollector : MonoBehaviour
{
    /// <summary>
    /// Player stats
    /// </summary>
    public Stats mageStats;

    /// <summary>
    /// Player Currencies
    /// </summary>
    public Currencies currencies;

    /// <summary>
    /// Speed in which loot we be collected
    /// </summary>
    [Range(0.1f, 20)] public float lootSpeed;

    /// <summary>
    /// List that stores every item inside collection trigger
    /// </summary>
    private List<GameObject> toCollectList = new List<GameObject>();

    /// <summary>
    /// Add loot to the list
    /// </summary>
    public void OnTriggerEnter(Collider other)
    {
        if (!toCollectList.Contains(other.gameObject))
            toCollectList.Add(other.gameObject);
    }

    /// <summary>
    /// Remove loot from the list
    /// </summary>
    public void OnTriggerExit(Collider other)
    {
        if (toCollectList.Contains(other.gameObject))
            toCollectList.Remove(other.gameObject);
    }

    /// <summary>
    /// Loot collector checker
    /// </summary>
    public void Update()
    {
        // Loop objects inside trigger
        foreach (GameObject objsToCollect in toCollectList)
        {
            // Move loot towards player
            objsToCollect.transform.position = Vector3.MoveTowards(objsToCollect.transform.position, transform.position, lootSpeed * Time.deltaTime);
            if (Vector3.Distance(objsToCollect.transform.position, transform.position) < 0.05f)
            {
                // Collect it when near
                Collectible collectible = objsToCollect.GetComponent<Collectible>();

                // Check type and give reward
                switch (collectible.type)
                {
                    case Collectible.Type.Skull:
                        currencies.AddSkulls(collectible.Quantity);
                        break;
                    case Collectible.Type.Orb:
                        mageStats.HealPercentage(5);
                        break;
                    default:
                        break;
                }

                // Remove and destroy
                toCollectList.Remove(objsToCollect);
                Destroy(objsToCollect);

                return;
            }
        }
    }
}
