﻿using TMPro;
using UnityEngine;

/// <summary>
/// Handles new shop logic 
/// (not the best optimization since I had to go from a 2D shop with interface to a 3D shop and adapt more elements in a couple of days)
/// </summary>
public class Shop : MonoBehaviour
{
    /// <summary>
    /// Reference to all the used selling spots
    /// </summary>
    public Transform[] sellingSpots;

    /// <summary>
    /// Reference to all the available trinkets to sell
    /// </summary>
    public TrinketData[] trinketDatas;

    /// <summary>
    /// Reference to all the available spells to sell
    /// </summary>
    public SpellData[] spellDatas;

    /// <summary>
    /// Reference to the trinket prefab
    /// </summary>
    public GameObject trinketPrefab;

    /// <summary>
    /// Reference to the spell prefab
    /// </summary>
    public GameObject spellPrefab;

    /// <summary>
    /// Reference to the spell upgrade prefab
    /// </summary>
    public GameObject spellUpgradePrefab;

    /// <summary>
    /// Reference to player's spell system
    /// </summary>
    public SpellSystem spellSystem;

    /// <summary>
    /// Reference to the player's currencies 
    /// </summary>
    public Currencies currencies;

    private void Start()
    {
        // Create it some time later due to execution order issues 
        Invoke("CreateShopItems", 0.1f);
    }

    /// <summary>
    /// Creates the shop's stock
    /// </summary>
    public void CreateShopItems()
    {
        for (int i = 0; i < 5; i++)
        {
            CreateTrinket();
        }

        // Only creates trinkets since we removed temporarily spell drops and upgrades.
    }

    /// <summary>
    /// Creates random trinket
    /// </summary>
    private void CreateTrinket()
    {
        // Create physical trinket and set data
        Trinket trinket = Instantiate(trinketPrefab).GetComponent<Trinket>();
        trinket.trinketData = trinketDatas[Random.Range(0, trinketDatas.Length)];
        trinket.DisplayInfo = true;
        trinket.transform.parent = GetRandomFreeSpot();
        trinket.transform.localPosition = Vector3.up;
        SetCost(trinket.transform.parent.GetChild(0).GetChild(0).GetComponent<TextMeshPro>(), trinket.trinketData.cost);
    }

    /// <summary>
    /// Creates random spell
    /// </summary>
    private void CreateSpellDrop()
    {
        // Create physical spell and set data
        SpellDrop spellDrop = Instantiate(spellPrefab).GetComponent<SpellDrop>();
        spellDrop.sold = true;
        spellDrop.spellData = spellDatas[Random.Range(0, spellDatas.Length)];
        spellDrop.transform.parent = GetRandomFreeSpot();
        spellDrop.transform.localPosition = Vector3.up;
        SetCost(spellDrop.transform.parent.GetChild(0).GetChild(0).GetComponent<TextMeshPro>(), spellDrop.spellData.cost);
    }

    /// <summary>
    /// Creates a random spell upgrade from the player's spell system
    /// </summary>
    private void CreateSpellUpgrade()
    {
        SpellData spellUpgradeCheck = GetRandomSpellUpgrade();
        // If we found an available spell upgrade
        if (spellUpgradeCheck)
        {
            // Create physical upgrade and set data
            SpellUpgrade spellUpgrade = Instantiate(spellUpgradePrefab).GetComponent<SpellUpgrade>();
            spellUpgrade.spellSlot = spellUpgradeCheck.slot;
            spellUpgrade.sold = true;
            spellUpgrade.upgradeSpell = spellUpgradeCheck;
            spellUpgrade.transform.parent = GetRandomFreeSpot();
            spellUpgrade.transform.localPosition = Vector3.up;
            SetCost(spellUpgrade.transform.parent.GetChild(0).GetChild(0).GetComponent<TextMeshPro>(), spellUpgradeCheck.cost);
        }
    }

    /// <summary>
    /// Return a random free selling spot
    /// </summary>
    /// <returns></returns>
    private Transform GetRandomFreeSpot()
    {
        // Get random spot 
        Transform freeSpot = sellingSpots[Random.Range(0, sellingSpots.Length)];

        // Check if it's available
        while (freeSpot.gameObject.activeSelf)
        {
            // Loop until you get an available spot
            freeSpot = sellingSpots[Random.Range(0, sellingSpots.Length)];
        }
        // Set it as available and return it
        freeSpot.gameObject.SetActive(true);

        return freeSpot;
    }

    /// <summary>
    /// Return a random spell upgrade from the player's spell system
    /// </summary>
    private SpellData GetRandomSpellUpgrade()
    {
        int iterations = 0;
        SpellData upgradedSpell = null;
        SpellData checkSpell = null;
        while (iterations++ < 10 || !upgradedSpell)
        {
            // Get random player's spell
            checkSpell = spellSystem.spells[Random.Range(0, spellSystem.spells.Length)].spell;
            // Check if it's upgradable
            if (checkSpell.upgradeSpell)
                upgradedSpell = checkSpell.upgradeSpell;
        }
        // Return upgraded spell
        return upgradedSpell;
    }

    /// <summary>
    /// Sets the cost in the given text reference
    /// </summary>
    private void SetCost(TextMeshPro textMesh, int cost)
    {
        // Check if the player can buy it
        if (currencies.Skulls >= cost)
            textMesh.SetText(cost + " <sprite=0>");
        else
            // Red if the player has no money
            textMesh.SetText("<color=red> "+ cost + "</color> <sprite=0>");
    }

    /// <summary>
    /// Update shop costs 
    /// </summary>
    public void UpdateCosts()
    {
        // Loop every selling point
        for (int i = 0; i < transform.GetChild(0).childCount; i++)
        {
            // Cache it and check it it's available
            GameObject sellingPoint = transform.GetChild(0).GetChild(i).gameObject;
            if (sellingPoint.activeSelf)
            {
                // Get selling item type checking tag and update cost
                GameObject sellingItem = sellingPoint.transform.GetChild(1).gameObject;
                switch (sellingItem.tag)
                {
                    case "Trinket":
                        SetCost(sellingPoint.transform.GetChild(0).GetChild(0).GetComponent<TextMeshPro>(), sellingItem.GetComponent<Trinket>().trinketData.cost);
                        break;
                    case "SpellDrop":
                        SetCost(sellingPoint.transform.GetChild(0).GetChild(0).GetComponent<TextMeshPro>(), sellingItem.GetComponent<SpellDrop>().spellData.cost);
                        break;
                    case "SpellUpgrade":
                        SetCost(sellingPoint.transform.GetChild(0).GetChild(0).GetComponent<TextMeshPro>(), sellingItem.GetComponent<SpellUpgrade>().upgradeSpell.cost);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
