﻿using UnityEngine;

/// <summary>
/// Handles body decomposition (sounds weird)
/// </summary>
public class Corpse : MonoBehaviour
{
    /// <summary>
    /// Mesh reference
    /// </summary>
    [SerializeField] private SkinnedMeshRenderer mesh;

    /// <summary>
    /// Speed in which it dissolves
    /// </summary>
    [SerializeField] private float dissolveSpeed = 0.2f;

    /// <summary>
    /// Dissolve material used (shader)
    /// </summary>
    private Material dissolveMat;

    /// <summary>
    /// Current disolve value
    /// </summary>
    private float dissolveValue;

    /// <summary>
    /// Timer used
    /// </summary>
    private float timer;

    private void Awake()
    {
        // Cache current material
        dissolveMat = mesh.material;
    }

    /// <summary>
    /// Dissolve slowly corpse
    /// </summary>
    private void Update()
    {
        // Lerp and change shader value depending on timer
        dissolveValue = Mathf.Lerp(0, 1, timer);
        dissolveMat.SetFloat("_DissolveAmount", dissolveValue);
        timer += dissolveSpeed * Time.deltaTime;

        // Destroy body when it's over
        if (dissolveValue >= 1) Destroy(gameObject);
    }


}
