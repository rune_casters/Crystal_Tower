﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Stores loot data such as percentage and type along with general data such as spells and trinkets
/// </summary>
[CreateAssetMenu(fileName = "LootTable", menuName = "LootTable")]
public class LootTable : ScriptableObject
{
    /// <summary>
    /// Loot container that stores percentage, type and more
    /// </summary>
    [System.Serializable]
    public struct LootData
    {
        /// <summary>
        /// Current loot type
        /// </summary>
        public LootType type;

        /// <summary>
        /// Percentage of being looted
        /// </summary>
        public int percentage;

        /// <summary>
        /// Minimal value that could be created
        /// </summary>
        public int minValue;

        /// <summary>
        /// Maximal value that could be created
        /// </summary>
        public int maxValue;

        /// <summary>
        /// Loot prefab (mesh + effects)
        /// </summary>
        public GameObject prefab;
    }

    /// <summary>
    /// Type of loot
    /// </summary>
    public enum LootType
    {   
        Nothing,
        Skull,
        HealthOrb,
        Trinket,
        Spell,
        SpellUpgrade
    }

    /// <summary>
    /// Stores every possible loot data (filled through editor)
    /// </summary>
    public LootData[] lootDatas;

    /// <summary>
    /// Total percentage (100)
    /// </summary>
    public int totalPercentage;

    /// <summary>
    /// Stores every trinket data reference
    /// </summary>
    public TrinketData[] trinketDatas;

    /// <summary>
    /// Stores every spell data reference
    /// </summary>
    public SpellData[] spellDatas;

    /// <summary>
    /// Spell system reference 
    /// </summary>
    public SpellSystem spellSystem;
}

