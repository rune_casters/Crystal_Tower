﻿using UnityEngine;

/// <summary>
/// Simple menu button container that handles hover and button data
/// </summary>
public class MenuButton : MonoBehaviour
{
    /// <summary>
    /// Button name
    /// </summary>
    public string buttonName;

    /// <summary>
    /// If the button is on hover
    /// </summary>
    public bool onHover;

    /// <summary>
    /// Called when button is being hovered by player
    /// </summary>
    public void Hover(bool hovered)
    {
        // If it's being hovered
        if (hovered)
        {
            // Scale up
            onHover = true;
            transform.localScale = Vector3.one * 1.3f;
        }
        // Hover has been stopped
        else
        {
            // Scale down
            onHover = false;
            transform.localScale = Vector3.one;
        }
    }
}
