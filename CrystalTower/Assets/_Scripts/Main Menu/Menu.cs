﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Handles Menus logic
/// </summary>
public class Menu : MonoBehaviour
{
    /// <summary>
    /// Animator reference
    /// </summary>
    public Animator anim;

    /// <summary>
    /// Camera used reference
    /// </summary>
    public Camera cam;

    /// <summary>
    /// Cached hovered button
    /// </summary>
    private MenuButton currentButtonHovered;

    /// <summary>
    /// 
    /// </summary>
    private bool loadingScene;

    /// <summary>
    /// Menu different states
    /// </summary>
    public enum menuState
    {
        MAINSCREEN,
        PLAYSCREEN
    }

    /// <summary>
    /// Current menu state
    /// </summary>
    public menuState currentMenuState;

    /// <summary>
    /// Check what the player is doing depending on the state of the menu
    /// </summary>
    public void Update()
    {
        // Depending on the state
        switch (currentMenuState)
        {
            // If the user presses any key it goes to the play screen
            case menuState.MAINSCREEN:
                if (Input.anyKeyDown)
                {
                    ChangeMenuState(menuState.PLAYSCREEN);
                }
                break;
            // Use menu raycast to check what the player is selecting
            case menuState.PLAYSCREEN:
                MenuRaycast();
                break;
            default:
                break;
        }
        
    }

    /// <summary>
    /// Menu user mouse raycast to check what he's clicking
    /// </summary>
    private void MenuRaycast()
    {
        // Create raycast data to mouse position
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // If it hits something
        if (Physics.Raycast(ray, out hit, 100))
        {
            // If it's not the same button then disable the hover state on that button
            if (currentButtonHovered && currentButtonHovered != hit.transform.GetComponent<MenuButton>())
                currentButtonHovered.Hover(false);

            // Get button component and set it on hover
            currentButtonHovered = hit.transform.GetComponent<MenuButton>();
            if (!currentButtonHovered.onHover)
                currentButtonHovered.Hover(true);

            // Get mouse axis (if the user clicked)
            if (Input.GetAxis("Basic 1") > 0)
            {
                // Depending on the button name 
                switch (currentButtonHovered.buttonName)
                {
                    // Load game scene
                    case "Play":
                        if (!loadingScene)
                        {
                            loadingScene = true;
                            StartCoroutine(LoadYourAsyncScene("Scene_Game"));
                        }
                        break;
                    // Go back to main menu
                    case "Back":
                        ChangeMenuState(menuState.MAINSCREEN);
                        break;
                    default:
                        break;
                }
            }
            
        }
        // Clear hover if user isnt hovering anything
        else if(currentButtonHovered)
        {
            currentButtonHovered.Hover(false);
            currentButtonHovered = null;
        }
    }

    /// <summary>
    /// Change to given state triggering a camera animation
    /// </summary>
    private void ChangeMenuState(menuState newMenuState)
    {
        currentMenuState = newMenuState;
        switch (newMenuState)
        {
            case menuState.MAINSCREEN:
                anim.SetTrigger("ToMainMenu");
                break;
            case menuState.PLAYSCREEN:
                anim.SetTrigger("ToPlayMenu");
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Loads given scene name 
    /// </summary>
    public static IEnumerator LoadYourAsyncScene(string scene)
    {
        // Load scene asyncronously
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);

        asyncLoad.allowSceneActivation = false;

        // Check if loading is done
        while (!asyncLoad.isDone)
        {
            // When it's near loaded then activate scene
            if (asyncLoad.progress >= 0.9f)
            {
                asyncLoad.allowSceneActivation = true;
            }
            yield return null;
        }
    }
}
