﻿using UnityEditor;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;

/// <summary>
/// Editor Window to create a searching area for the enemies.
/// </summary>

public class WalkableWindow : EditorWindow
{
    /// <summary>
    /// Struct used by the dll.
    /// </summary>
    public struct Cube
    {
        public Cube(float x, float y, float z, float width, float height, float length)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.width = width;
            this.height = height;
            this.length = length;
        }
        float x, y, z;
        float width, height, length;

        float GetVolume() { return width * height * length; }
        void SetPos(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    };

    /// <summary>
    /// Imports the main function of the dll.
    /// </summary>
    /// <param name="xOffset"> The offset where the collision check begins on the X axis. </param>
    /// <param name="zOffset"> The offset where the collision check begins on the Y axis. </param>
    /// <param name="yOffset"> The offset where the collision check begins on the Z axis. </param>
    /// <param name="walkableAreaWidth"> The number of nodes on the X axis. </param>
    /// <param name="walkableAreaHeight"> The number of nodes on the Y axis. </param>
    /// <param name="walkableAreaLength"> The number of nodes on the Z axis. </param>
    /// <param name="checker"> The cube that checks collisions with the walkables. </param>
    /// <param name="walkables"> Array of cubes created by the user on the scene. </param>
    /// <param name="walkableCount"> The length of the walkables array. </param>
    /// <returns></returns>
    [DllImport("CollisionCheck")]
    public static extern IntPtr GetWalkableArray(float xOffset, float zOffset, float yOffset, int walkableAreaWidth, int walkableAreaHeight, int walkableAreaLength, Cube checker, Cube[] walkables, int walkableCount);
    /// <summary>
    /// The root of the cubes created by the user.
    /// </summary>
    Transform walkablesRoot;
    /// <summary>
    /// The scriptable object where the final data is saved at.
    /// </summary>
    RoomData scriptable;

    /// <summary>
    /// bool to open or close the foldout.
    /// </summary>
    bool nodeFoldout;
    /// <summary>
    /// Variables to save from the window.
    /// </summary>
    float xSize, ySize, zSize;
    float xOffset, yOffset, zOffset;
    int areaWidth, areaHeight, areaLength;
    /// <summary>
    /// List of the cubes drawn in the scene to preview the searching area.
    /// </summary>
    static List<GameObject> nodes;

    /// <summary>
    /// Creates or focuses the WalkableTool window.
    /// </summary>
    [MenuItem("| LevelDesign |/Walkable Tool")]
    public static void ShowWindow()
    {
        GetWindow<WalkableWindow>("Walkable Tool");
        // We need to create the nodes list each time because unity deletes editor variables on play.
        if (nodes == null) nodes = new List<GameObject>();
    }
    /// <summary>
    /// Handles the window.
    /// </summary>
    private void OnGUI()
    {
        // Label to give information
        GUILayout.Label("Walkable Area", EditorStyles.boldLabel);
        // Saves the root of the walkables.
        walkablesRoot = EditorGUILayout.ObjectField("Walkables Root", walkablesRoot, typeof(Transform), true) as Transform;
        // Saves the scriptable.
        scriptable = EditorGUILayout.ObjectField("Scriptable Object", scriptable, typeof(RoomData), true) as RoomData;

        GUILayout.Space(10);

        // Saves the node position offset and the number of nodes on each axis.
        GUILayout.Label("3d size of the walkable area");
        areaWidth = EditorGUILayout.IntField("Area Width", areaWidth);
        areaHeight = EditorGUILayout.IntField("Area Height", areaHeight);
        areaLength = EditorGUILayout.IntField("Area Length", areaLength);

        GUILayout.Space(10);

        GUILayout.Label("First node position relative to the transform");
        xOffset = EditorGUILayout.FloatField("Node Start X", xOffset);
        yOffset = EditorGUILayout.FloatField("Node Start Y", yOffset);
        zOffset = EditorGUILayout.FloatField("Node Start Z", zOffset);

        GUILayout.Space(10);
        // Saves the node size.
        nodeFoldout = EditorGUILayout.Foldout(nodeFoldout, "Node Stuff");
        if (nodeFoldout)
        {
            xSize = EditorGUILayout.FloatField("X Size", xSize);
            ySize = EditorGUILayout.FloatField("Y Size", ySize);
            zSize = EditorGUILayout.FloatField("Z Size", zSize);
        }
        // Button to save the data on the scriptable.
        GUILayout.Space(20);
        if (GUILayout.Button("Bake") && xSize > 0 && ySize > 0 && zSize > 0 && scriptable != null && walkablesRoot != null && walkablesRoot.childCount > 0)
        {
            // It needs to be Dirty so unity knows its gonna change on editor mode.
            EditorUtility.SetDirty(scriptable);
            scriptable.SetUpWalkable(Bake(), areaWidth, areaHeight, areaLength, xSize,new Vector3(xOffset,yOffset,zOffset));
        }
        // Preview button.
        GUILayout.Label("Unity slows down after 25k nodes and this button won't work for more than 100k nodes");
        if (GUILayout.Button("Preview") && xSize > 0 && ySize > 0 && zSize > 0 && scriptable != null && walkablesRoot != null && walkablesRoot.childCount > 0 && areaHeight * areaWidth * areaLength < 100000)
        {
            PreviewClicked();
        }
        // Delete nodes button.
        if (GUILayout.Button("Delte Nodes"))
        {
            DeleteNodes();
        }

    }

    /// <summary>
    /// Calls the dll and transforms the c++ int* to c# int[].
    /// </summary>
    /// <returns> Int[] with 1 and 0. </returns>
    private int[] Bake()
    {
        // Create the Cube that will collide with the walkables with the given size.
        Cube node = new Cube(0, 0, 0, xSize, ySize, zSize);

        // Create the array with all the walkables.
        int arraySize = walkablesRoot.childCount;
        Cube[] cubes = new Cube[arraySize];
        for (int i = 0; i < arraySize; i++)
        {
            // Convert each BoxCollider to a Cube and save it.
            BoxCollider box = walkablesRoot.GetChild(i).GetComponent<BoxCollider>();
            if (box)
                cubes[i] = new Cube(box.center.x + box.transform.position.x, box.center.y + box.transform.position.y, box.center.z + box.transform.position.z, box.size.x, box.size.y, box.size.z);
        }

        // Get the resulting int* from the dll.
        IntPtr result = GetWalkableArray(walkablesRoot.position.x + xOffset, walkablesRoot.position.y + yOffset, walkablesRoot.position.z + zOffset, areaWidth, areaHeight, areaLength, node, cubes, arraySize);

        // The first value of the array is the size of the array with the values we want.
        int arrayLength = Marshal.ReadInt32(result);
        // Copy all the array to convert it from IntPtr to Int[].
        int[] conversion = new int[arrayLength];
        Marshal.Copy(result, conversion, 0, arrayLength);
        // Remove the first element of the array.
        int[] clearedArray = new int[arrayLength - 1];
        for (int i = 1; i < arrayLength; i++)
        {
            clearedArray[i - 1] = conversion[i];
        }
        return clearedArray;
    }
    /// <summary>
    /// Draws cubes on each position of the searching area where the enemy can walk.
    /// </summary>
    private void PreviewClicked()
    {
        // Deletes the previews cubes if there was any.
        DeleteNodes();
        // The offset of the 1st position of the array.
        Vector3 nodeStartPos = new Vector3(walkablesRoot.position.x + xOffset, walkablesRoot.position.y + yOffset, walkablesRoot.position.z + zOffset);
        // Gets the searching area.
        int[] previewArray = Bake();
        // Draws a cube on each position where the array is 1.
        for (int i = 0; i < areaWidth; i++)
        {
            for (int j = 0; j < areaLength; j++)
            {
                for (int k = 0; k < areaHeight; k++)
                {
                    if (previewArray[i * areaLength * areaHeight + j * areaHeight + k] == 1)
                    {
                        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        cube.transform.position = nodeStartPos + new Vector3(i * xSize + xSize / 2, k * ySize + ySize / 2, j * zSize + zSize / 2);
                        nodes.Add(cube);
                    }
                }
            }
        }
    }

    /// <summary>
    /// If there is any GameObject in the array delete them and clear the array.
    /// </summary>
    private void DeleteNodes()
    {
        if (nodes.Count > 0)
        {
            foreach (GameObject cube in nodes)
            {
                DestroyImmediate(cube);
            }
            nodes.Clear();
        }
    }
}
